<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" isELIgnored="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="<c:url value="/resources/css/bootstrap.min.css" />">
<script type="text/javascript" src="<c:url value="/resources/js/jquery.1.10.2.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/bootstrap.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/books.js" />"></script>
	
<style type="text/css">
table tr, th, td {
	text-align: center;
}
img {
	width: 200px;
	height: 200px;
}
</style>
<title><spring:message code="book.label" /></title>
</head>
<body>
	<%@ include file="header.jsp" %>
	<div class="panel panel-default">
		<div id="title" class="panel-heading h3 text-center">
			List book
		</div>
		<div class="col-sm-3">
			<div class="list-group">
				<a href="./genre?idgenre=0" class="list-group-item">Tat ca sach</a>
				<c:forEach items="${genres}" var="genre">
					<a href="./genre?idgenre=${genre.id }" class="list-group-item">${genre.description }</a>
				</c:forEach>

			</div>
		</div>
		<div class="col-sm-9">
			<div class="pull-left">
				<p>${messsearch }</p>
			</div>
			<div class="pull-right">
				<button class="btn btn-info" onclick="addBook();">add book</button>
			</div>
			<table class="table table-condensed">
				<thead>
					<tr>
						<th>id</th>
						<th>name</th>
						<th>price</th>
						<th>hinh anh</th>
						<th>action</th>
					</tr>
				</thead>
				<tbody>
					<c:choose>
						<c:when test="${!empty books}">
							<c:forEach items="${books }" var="book">
								<tr>
									<td>${book.id}</td>
									<td>${book.name}</td>
									<td>${book.price}</td>
									<td>${book.imgage}</td>
									<td>
										<button class="btn btn-default" onclick="getBook(${book.id}, 'VIEW');">view</button>
										<button class="btn btn-info" onclick="getBook(${book.id}, 'EDIT');">edit</button>
										<button class="btn btn-danger" onclick="deleteBook(${book.id})">delete</button>
									</td>
								</tr>
							</c:forEach>
						</c:when>
					</c:choose>
				</tbody>
				<tfoot>
					<tr>
						<c:if test="${slpagebook>1}">
							<th colspan="5">
								<c:if test="${pagebook>1 }">
									<a href="./pagebook?page=${pagebook-1 }" class="btn btn-link">Back</a>
								</c:if>
								<c:if test="${pagebook<slpagebook }">
									<a href="./pagebook?page=${pagebook+1 }" class="btn btn-link"> Next</a>
								</c:if>
							</th>
						</c:if>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>
</body>
</html>