<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="<c:url value="/resources/css/bootstrap.min.css" />">
<script type="text/javascript" src="<c:url value="/resources/js/jquery.1.10.2.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/bootstrap.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/order.js" />"></script>

<title>Order</title>
</head>
<body>
	<%@ include file="header.jsp" %>
	
	<div class="panel panel-default">
		<div id="title" class="panel-heading h3 text-center">
			Order
		</div>
		<div class="panel-body">
			<div class="col-md-6">
				<h4>order chua xac nhan</h4>
				<table class="table table-striped">
					<thead>
						<tr>
							<th>id</th>
							<th>Name User</th>
							<th>Date</th>
							<th>view</th>
							<th>confirm</th>
							<th>delete</th>
						</tr>
					</thead>
					<tbody>
						<c:choose>
							<c:when test="${!empty orders}">
								<c:forEach var="order" items="${orders}" >
									<c:if test="${order.status ==0}">
										<tr>
											<td>${order.id_order}</td>
											<td>${order.user.name}</td>
											<td>${order.date_purchse }</td>
											<td>
												<button class="btn btn-info" onclick=" getOrder(${order.id_order}, 'VIEW')">
													View
												</button>
											</td>
											<td>
												<button class="btn btn-primary" onclick=" getOrder(${order.id_order}, 'CONFIRM');">
													confirm
												</button>
											</td>
											<td>
												<button class="btn btn-danger" onclick="deleteOrder(${order.id_order });">
													Delete
												</button>
											</td>
										</tr>
									</c:if>
								</c:forEach>
								</c:when> 
		 						<c:otherwise> 
	 							<tr> 
	 								<th colspan="5" class="text-center">
	 									No Order
	 								</th> 
	 							</tr> 
	 						</c:otherwise>
	 					</c:choose> 
					</tbody>
				</table>
			</div>
			<div class="col-md-6">
				<h4>order da xac nhan</h4>
				<table class="table table-striped">
					<thead>
						<tr>
							<th>id</th>
							<th>Name User</th>
							<th>Date</th>
							<th>view</th>
							<th>delete
						</tr>
					</thead>
					<tbody>
						<c:choose>
							<c:when test="${!empty orders}">
								<c:forEach var="order" items="${orders}" >
									<c:if test="${order.status ==1}">
										<tr>
											<td>${order.id_order}</td>
											<td>${order.user.name}</td>
											<td>${order.date_purchse }</td>
											<td>
												<button class="btn btn-info" onclick=" getOrder(${order.id_order}, 'VIEW');">
													View
												</button>
											</td> 
											<td>
												<button class="btn btn-danger" onclick="deleteOrder(${order.id_order });">
													Delete
												</button>
											</td>
										</tr>
									</c:if>
								</c:forEach>
								</c:when> 
		 						<c:otherwise> 
	 							<tr> 
	 								<th colspan="5" class="text-center">
	 									No Order
	 								</th> 
	 							</tr> 
	 						</c:otherwise>
	 					</c:choose> 
					</tbody>
				</table>
			</div>
			<div class="col-md-6">
				<h4>order da bi huy</h4>
				<table class="table table-striped">
					<thead>
						<tr>
							<th>id</th>
							<th>Name User</th>
							<th>Date</th>
							<th>view</th>
							
							<th>delete</th>
						</tr>
					</thead>
					<tbody>
						<c:choose>
							<c:when test="${!empty orders}">
								<c:forEach var="order" items="${orders}" >
									<c:if test="${order.status ==3}">
										<tr>
											<td>${order.id_order}</td>
											<td>${order.user.name}</td>
											<td>${order.date_purchse }</td>
											<td>
												<button class="btn btn-info" onclick=" getOrder(${order.id_order}, 'VIEW')">
													View
												</button>
											</td>
											
											<td>
												<button class="btn btn-danger" onclick="deleteOrder(${order.id_order });">
													Delete
												</button>
											</td>
										</tr>
									</c:if>
								</c:forEach>
								</c:when> 
		 						<c:otherwise> 
	 							<tr> 
	 								<th colspan="5" class="text-center">
	 									No Order
	 								</th> 
	 							</tr> 
	 						</c:otherwise>
	 					</c:choose> 
					</tbody>
				</table>
			</div>
		</div>
	</div>
</body>
</html>