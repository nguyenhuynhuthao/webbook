<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" isELIgnored="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<%-- <link rel="stylesheet" href="<c:url value="/resources/css/bootstrap.min.css" />"> --%>
<%-- <script type="text/javascript" src="<c:url value="/resources/js/jquery.1.10.2.min.js" />"></script> --%>
<%-- <script type="text/javascript" src="<c:url value="/resources/js/bootstrap.min.js" />"></script> --%>

<title>Insert title here</title>
</head>
<body>
	<div class="navbar navbar-inverse">
		<div class="container-fluid">
			<a href="../admin/view" class="navbar-brand">
				logo web
			</a>
			<form class="navbar-form navbar-left" action="../admin/get">
			     <div class="form-group">
			       <input type="text" name="search" id="search" class="form-control" placeholder="Search" >
			       
			     </div>
			     <button type="submit" class="btn btn-info">Search</button>
		    </form>
		    <ul class="nav navbar-nav navbar-right">
		    	<li class="active"><a href="../admin/view">Home</a></li>
		    	<li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" >Product<span class="caret"></span></a>
			        <ul class="dropdown-menu">
			          <li><a href="../bookcontroller/books">Book</a></li>
			          <li><a href="../publishercontroller/publishers">Publisher</a></li>
			          <li><a href="../authorcontroller/authors">Author</a></li>
			          <li><a href="../genrecontroller/genres">Genre</a></li>
			        </ul>
			    </li>
			    <li>
			    	<a href="../usercontroller/users">User</a>
			    </li>
			     <li>
			    	<a href="../commentcontroller/comments">Comment
			    	<c:if test="${checkmessages >0}"><span class="badge">${checkmessages}</span></c:if>
			    	</a>
			    </li>
			    <li>
			    	<a href="../ordercontroller/orders">Order
			    		<c:if test="${checkorder >0}"><span class="badge">${checkorder}</span></c:if>
			    	</a>
			    </li>
	    		<li class="dropdown">
	    			<a class="dropdown-toggle" data-toggle="dropdown" href="">
	    				<span class="glyphicon glyphicon-pencil	"></span>
	    				${manager.name }
	    				<span class="caret"></span>
	    			</a>
			        <ul class="dropdown-menu">
			          <li><a href="../admin/logout">logout</a></li>
			          
			        </ul>
			     </li>
		    	
		    </ul>
	   </div>

	</div>
</body>
</html>