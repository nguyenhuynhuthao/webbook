<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" isELIgnored="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="<c:url value="/resources/css/bootstrap.min.css" />">
<script type="text/javascript" src="<c:url value="/resources/js/jquery.1.10.2.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/bootstrap.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/customers.js" />"></script>
<title><spring:message code="customer.detail.label"></spring:message></title>

<style type="text/css">
table tr, th, td {
	text-align: center;
}

table th {
	font-size: 20px;
}

table td {
	font-size: 15px;
}
</style>

<script type="text/javascript">
	$(document).ready(function() {
		$("tr.rows").click(function() {
			alert("Click!");
		});
	});
</script>
<title>More Detail</title>

</head>
<body>
	<%@ include file="header.jsp" %>
	<div class="panel panel-default">
		<div class="panel-heading h3 text-center"><spring:message code="customer.detail.label" /></div>
		<div class="panel-body">
			<form:form class="form-horizontal" action="./customer" method="post" modelAttribute="customer">
				<form:input path="id" type="hidden"/>
				<div class="form-group">
					<label class="control-label col-sm-4" for="NameCu">
						<spring:message code="customer.detail.name" />
					</label>
					<div class="col-sm-6">
						<form:input path="name_customer" type="text" class="form-control" id="NameCu" 
								name="NameCu" placeholder="Customer name" readonly="${mode == 'VIEW'}"/>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-offset-4 col-sm-6">
						<c:if test = "${mode == 'VIEW'}">
							<button disabled="disabled" type="submit" class="btn btn-primary">
								<spring:message code="customer.detail.btn.save" />
							</button>
						</c:if>
						<c:if test = "${mode == 'EDIT'}">
							<button type="submit" class="btn btn-primary">
								<spring:message code="customer.detail.btn.save" />
							</button>
						</c:if>
						<button type="button" onclick="getCustomer()" class="btn btn-default">
							<spring:message code="customer.detail.btn.cancel" />
						</button>
					</div>
				</div>
			</form:form>
		</div>
	</div>
</body>
</html>