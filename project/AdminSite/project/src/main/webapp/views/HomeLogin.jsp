<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="<c:url value="/resources/css/bootstrap.min.css" />">
<script type="text/javascript" src="<c:url value="/resources/js/jquery.1.10.2.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/bootstrap.min.js" />"></script>
<style type="text/css">
	#login {
	text-align: center;
	font-size: 20px;
	border: green;
	background-color: aqua;
	
	}
	
</style>
<title>LOGIN</title>
</head>
<body>
	<div class="center-container">

			<div id="login">
				<div id="ct">
					<h2>Login</h2>
				</div>
				<form action="../admin/login" method="get">
					<p>${error}</p>
					<input placeholder="Username" name="name" class="user" type="text">
<!-- 						<span class="icon1"><i class="fa fa-user" aria-hidden="true"></i></span> -->
					<br><br>
					<input  placeholder="Password" name="pass" class="pass" type="password">
<!-- 						<span class="icon2"><i class="fa fa-unlock" aria-hidden="true"></i></span> -->
					<br>
					<div class="sub-w3l">
<!-- 							<h6><a href="#"></a></h6> -->
						<div class="right-w3l">
							<input type="submit" value="Login">
						</div>
					</div>
				</form>
			</div>
		</div>

</body>
</html>