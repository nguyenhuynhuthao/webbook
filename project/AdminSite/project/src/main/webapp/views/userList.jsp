<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet"
	href="<c:url value="/resources/css/bootstrap.min.css" />">
<script type="text/javascript"
	src="<c:url value="/resources/js/jquery.1.10.2.min.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/bootstrap.min.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/users.js" />"></script>
	
<style type="text/css">
table tr, th, td {
	text-align: center;
}
</style>
<title><spring:message code="user.label" /></title>
</head>
<body>
	<%@ include file="header.jsp" %>
	<div class="panel panel-default">
		<div id="title" class="panel-heading h3 text-center">
			<spring:message code="user.header"></spring:message>
		</div>
		<div class="panel-body">
			<table class="table table-striped" id="userHome">
				<thead>
					<tr>
						<th><spring:message code="user.table.id" /></th>
						<th><spring:message code="user.table.name" /></th>
						<th>Status</th>
						<th><spring:message code="user.table.action" /></th>
					</tr>
				</thead>
				<tbody>
					<c:choose>
						<c:when test="${!empty users}">
							<c:forEach items="${users}" var="user">
								<c:if test="${user.role==2||user.role==1 }">
									<tr>
										<td>${user.id}</td>
										<td>${user.name}</td>
										<td>
											<c:if test="${user.role==2 }">
												Normal
											</c:if>
											<c:if test="${user.role==1 }">
												Lock
											</c:if>
										</td>
										<td>
											<button class="btn btn-default"
												onclick="getUser(${user.id}, 'VIEW');">
												<spring:message code="user.btn.view" />
											</button>
											<button class="btn btn-primary"
												onclick="lock(${user.id});">
												Lock
											</button>
											<button class="btn btn-danger"
												onclick="deleteUser(${user.id}, this);">
												<spring:message code="user.btn.delete" />
											</button>
										</td>
									</tr>
								</c:if>
							</c:forEach>
						</c:when>
						<c:otherwise>
							<tr>
								<th colspan="5" class="text-center"><spring:message
										code="user.table.empty" /></th>
							</tr>
						</c:otherwise>
					</c:choose>
					
				</tbody>
			</table>
		</div>
	</div>
</body>
</html>