<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" isELIgnored="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="<c:url value="/resources/css/bootstrap.min.css" />">
<script type="text/javascript" src="<c:url value="/resources/js/jquery.1.10.2.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/bootstrap.min.js" />"></script>

<link rel="stylesheet" href="<c:url value="/resources/css/fontadmin.css" />">
<script type="text/javascript" src="<c:url value="/resources/js/adminpage.js" />"></script>

<style type="text/css">
	div {
		text-align: center;
	}
	#div_dt,#div_hd,#div_tn
	{
	
	}
}
</style>
<title>Insert title here</title>
</head>
<body>
	<%@ include file="header.jsp" %>
	<div id="txtHint"></div>
	<div class="container" > 
		<div class="row" >
			<div class="col-sm-4 well well-sm" id="div_dt">
				<p class="lead"><strong>Tong Doanh thu: </strong></p>
				
				 <h4>${price } <small>VND</small></h4> 
			</div>
			<div class="col-sm-4 well well-sm" id="div_dh">
				<p class="lead"><strong>Tong Don Hang: </strong></p>
				<h4> ${sizeorder } </h4>
			</div>
			<div class="col-sm-4 well well-sm" id="div_tn">
				<p class="lead"><strong>Tong Tin Nhan: </strong></p>
				<h4>${sizemessages }</h4>
			</div>	
		</div>
		<div class="row">
			<div class="col-sm-6 well">
				<div style="font-size: 30px;">
					<strong>Doanh thu :</strong> ${pri }
				</div>
				<div>
				
					<form action="./getprice" method="get" onsubmit="return checkdate()">
						<input type="date" name="date1" id="date1" class="form-horizontal">
						<input type="date" name="date2" id="date2" class="form-horizontal">
						<input type="submit" value="Get" >
					</form>
				</div>
				<c:if test="${!empty bieudo}">
				<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">xem Bieu do</button>
				
				</c:if>
				<div class="modal fade" id="myModal" role="dialog">
				    <div class="modal-dialog modal-lg">
				    
				      <!-- Modal content-->
				      <div class="modal-content">
				        <div class="modal-body">
				          <%@ include file="chart.jsp" %>
				        </div>
				        <div class="modal-footer">
				          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				        </div>
				      </div>
				      
				    </div>
				  </div>
			</div>
			<div class="col-sm-6 well" style="font-size: 20px;">
				<div style="text-align: center;">
					<strong>Message</strong>
				</div>
				<div>
				<c:choose>
					<c:when test="${!empty messages }">
						<c:forEach items="${messages }" var="message">
							<div class="alert alert-info">
								${message.user.name } :
								<c:forEach items="${messagedt}" var="messdt">
									<c:if test="${messdt.comment.id_customer== message.id_customer}">
										<c:if test="${messdt.function==1 }">
											${messdt.tinnhan }
										</c:if>
									
									</c:if>
								</c:forEach>
								<a class="btn btn-default" href="../commentcontroller/comments">   Chi Tiet</a>
							</div>
							
						</c:forEach>
					</c:when>
					<c:otherwise>
						<div>
							No message 
						</div>
					</c:otherwise>
				</c:choose>
				
			</div>
			
		</div>
	</div>

</body>
</html>