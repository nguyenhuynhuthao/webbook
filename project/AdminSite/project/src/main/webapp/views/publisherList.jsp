<%@ page language="java" contentType="text/html; charset=ISO-8859-1" 
			pageEncoding="ISO-8859-1" isELIgnored="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet"
	href="<c:url value="/resources/css/bootstrap.min.css" />">
<script type="text/javascript"
	src="<c:url value="/resources/js/jquery.1.10.2.min.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/bootstrap.min.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/publishers.js" />"></script>
<title><spring:message code="publisher.label" /></title>
</head>
<body>
	<%@ include file="header.jsp" %>
	<div class="panel panel-default">
		<div id="title" class="panel-heading h3 text-center">
			<spring:message code="publisher.header"></spring:message>
		</div>
		<div class="panel-body">
			<table class="table table-striped">
				<thead>
					<tr>
						<th><spring:message code="publisher.table.id" /></th>
						<th><spring:message code="publisher.table.publisher_name" /></th>
						<th><spring:message code="publisher.table.action" /></th>
					</tr>
				</thead>
				<tbody>
					<c:choose>
						<c:when test="${!empty publishers}">
							<c:forEach items="${publishers}" var="publisher">
								<tr>
									<td>${publisher.id}</td>
									<td>${publisher.publisher_name}</td>
									<td>
										<button class="btn btn-info"
											onclick="getPublisher(${publisher.id}, 'VIEW');">
											<spring:message code="publisher.btn.view" />
										</button>
										<button class="btn btn-primary"
											onclick="getPublisher(${publisher.id}, 'EDIT');">
											<spring:message code="publisher.btn.edit" />
										</button>
										<button class="btn btn-danger"
											onclick="deletePublisher(${publisher.id});">
											<spring:message code="publisher.btn.delete" />
										</button>
									</td>
								</tr>
							</c:forEach>
						</c:when>
						<c:otherwise>
							<tr>
								<th colspan="5" class="text-center"><spring:message
										code="publisher.table.empty" /></th>
							</tr>
						</c:otherwise>
					</c:choose>
					<tr>
						<th colspan="5">
							<button onclick="location.href='addPublisher'"
								class="btn btn-primary">
								<spring:message code="publisher.btn.add" />
							</button>
						</th>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</body>
</html>