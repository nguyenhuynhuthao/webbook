<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<link rel="stylesheet" href="<c:url value="/resources/css/bootstrap.min.css" />">
<script type="text/javascript" src="<c:url value="/resources/js/jquery.1.10.2.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/bootstrap.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/author.js" />"></script>

<title><spring:message code="author.label" /></title>
</head>
<body>
	<%@ include file="header.jsp" %>
	
	<div class="panel panel-default">
		<div id="title" class="panel-heading h3 text-center">
			<spring:message code="author.header"/>
		</div>
		<div class="panel-body">
			<table class="table table-striped">
				<thead>
					<tr>
						<th><spring:message code="author.table.id"/></th>
						<th><spring:message code="author.table.name"/></th>
						<th><spring:message code="author.table.action"/></th>
					</tr>
				</thead>
				<tbody>
					<c:choose>
						<c:when test="${!empty authors}">
							<c:forEach var="author" items="${authors}" >
								<tr>
									<td>${author.id}</td>
									<td>${author.name}</td>
									<td>
										<button class="btn btn-info" onclick=" getAuthor(${author.id}, 'VIEW');">
											<spring:message code="author.btn.view"/>
										</button>
										<button class="btn btn-primary" onclick=" getAuthor(${author.id }, 'EDIT');">
											<spring:message code="author.btn.edit"/>
										</button>
										<button class="btn btn-danger" onclick="deleteAuthor(${author.id });">
											<spring:message code="author.btn.delete"/>
										</button>
									</td>
								</tr>
							</c:forEach>
							</c:when> 
	 						<c:otherwise> 
 							<tr> 
 								<th colspan="5" class="text-center">
 								<spring:message code="author.table.empty"/> 
 								</th> 
 							</tr> 
 						</c:otherwise>
 					</c:choose> 
 					<ul class="pagination">
 						<c:if test="${pageauthor>1}">
							<li><a href="./pageauthor?page=${pageauthor-1}">	&lt;</a></li>
						</c:if>
						<c:if test="${pageauthor<slpageauthor}">
								<li><a href="./pageauthor?page=${pageauthor+1}">	&gt;</a></li>
						</c:if>
					</ul>
					<tr>
						<th colspan="5">
							<button class="btn btn-primary" onclick="addAuthor();">
								<spring:message code="author.btn.add"/>
							</button>
						</th>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</body>
</html>