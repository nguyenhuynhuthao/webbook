<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" isELIgnored="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="<c:url value="/resources/css/bootstrap.min.css" />">
<script type="text/javascript" src="<c:url value="/resources/js/jquery.1.10.2.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/bootstrap.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/order.js" />"></script>

<style type="text/css">
	label {
		font-size: 20px;
}
</style>
<title>Order Detail</title>
</head>
<body>
	<%@ include file="header.jsp" %>

	<div class="panel panel-default">
		<div class="panel-heading h3 text-center">OrderDetail</div>
		<div class="panel-body">
			<%-- <form:form action="" modelAttribute="order"> --%>
				<div class="col-sm-6">
					<div class="form-group">
						<label id="Name User">Full Name:
							${order.user.userDetails.fullname } 
						</label>
					</div>
					<div class="form-group">
						<label id="date">Phone:
						${order.user.userDetails.phones}</label>
					</div>
					<div class="form-group">
						<label id="date">Street:
						${order.user.userDetails.address.street} </label>
					</div>
					<div class="form-group">
						<label id="date">District:
					${order.user.userDetails.address.district} </label>
					</div>
					<div class="form-group">
						<label id="date">City:
						${order.user.userDetails.address.city}</label>
					</div>
					<div class="form-group">
						<label id="date">Date Pruchse:
						${order.date_purchse }</label>
					</div>
					<div class="form-group">
						<c:if test="${order.status==0 }">
							<label id="date">Status:
							 Not Delivery</label>
						</c:if>
						<c:if test="${order.status==1 }">
							<label id="date">Status: Delivered
							</label>
						</c:if>
						<c:if test="${order.status==1 }">
							<label id="date">Status: Cancel
							</label>
						</c:if>
					</div>
				</div>
				<div class="col-sm-6">
					<table class="table table-striped">
						<thead>
							<tr>
								<th>Name book</th>
								<th>Price</th>
								<th>Quantity</th>
								<th>Total price</th>
							</tr>
						</thead>
						<tbody>
							<c:choose>
								<c:when test="${!empty listorderdetail }">
									<c:forEach items="${listorderdetail }" var="orderdetail">
										<tr>
											<td>${orderdetail.book.name}</td>
											<td>${orderdetail.book.price}</td>
											<td>${orderdetail.quantity}</td>
											<td id="price">${orderdetail.book.price*orderdetail.quantity}</td>
										</tr>
									</c:forEach>
								</c:when>
								<c:otherwise>
									<tr>
										<th>NO orderdetail</th>
									</tr>
								</c:otherwise>
							</c:choose>
							<tr >
								<th colspan="3">Tam Tinh</th>
								<th id="total_price" colspan="5">${price }</th>
							</tr>
							<tr>
								<th colspan="3">Phi Van Chuyen</th>
								<th id="total_price" colspan="5">15000</th>
							</tr>
							<tr>
								<th colspan="3">VAT</th>
								<th id="total_price" colspan="5">${price*0.1 }</th>
							</tr>
							<tr>
								<th colspan="3">tong cong</th>
								<th id="total_price" colspan="5">${price+15000+price*0.1 }</th>
							</tr>
						</tbody>
					</table>
				</div>
<%-- 			</form:form> --%>
		</div>
	</div>
</body>
</html>