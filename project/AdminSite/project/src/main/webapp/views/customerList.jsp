<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet"
	href="<c:url value="/resources/css/bootstrap.min.css" />">
<script type="text/javascript"
	src="<c:url value="/resources/js/jquery.1.10.2.min.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/bootstrap.min.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/customers.js" />"></script>
	
<style type="text/css">
table tr, th, td {
	text-align: center;
}
</style>
<title><spring:message code="customer.label" /></title>
</head>
<body>
	<%@ include file="header.jsp" %>
	<div class="panel panel-default">
		<div id="title" class="panel-heading h3 text-center">
			<spring:message code="customer.header"></spring:message>
		</div>
		<div class="panel-body">
		<table class="table table-striped" id="customerHome">
			
				<thead>
				<tr>
					<th  colspan="3" style="font-size: 30px;">Comment Chua Rep</th>
				</tr>
					<tr>
						<th>ID</th>
						<th>Name</th>
						<th>Comment</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<c:choose>
						<c:when test="${!empty comment1}">
							<c:forEach items="${comment1}" var="com">
								<tr>
									<td>${com.id_customer}</td>
									<td>${com.user.name}</td>
									
									<c:forEach items="${commentdt }" var="comd">
									<c:if test="${comd.comment.id_customer== com.id_customer}">
										
										<c:if test="${ comd.function==1}">
											<td>
												${comd.tinnhan }
											</td>
										</c:if>
										
									</c:if>
									</c:forEach>
									<td>
										
<!-- 										rep --> 
										<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Rep</button>
										<div id="myModal" class="modal fade" role="dialog">
											 <div class="modal-dialog">
											
											    <!-- Modal content-->
											   <div class="modal-content">
											     <div class="modal-header">
											       <button type="button" class="close" data-dismiss="modal">&times;</button>
											        <h4 class="modal-title">Rep</h4>
											      </div>
											      <form action="./repcomment" onsubmit="return check()">
											      <div class="modal-body">
											      		<input type="hidden" name="idcom" value="${com.id_customer}">
											        	<input type="text" name="mesrep" id="repcom">
											      </div>
											      <div class="modal-footer">
											      	<button type="submit" class="btn btn-default" >Rep</button>
											        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
											      </div>
											      </form>
											    </div>
											
											  </div>
										</div>
									</td>
								</tr>
							</c:forEach>
						</c:when>
						<c:otherwise>
							<tr>
								<th colspan="5" class="text-center">No Comment</th>
							</tr>
						</c:otherwise>
					</c:choose>
				</tbody>
			</table>
			<table class="table table-striped" id="customerHome">
				<thead>
				<tr>
					<th colspan="3" style="font-size: 30px;">Comment da Rep</th>
				</tr>
					<tr>
						<th>ID</th>
						<th>Name</th>
						<th>Comment</th>
						<th>Rep</th>
						
					</tr>
				</thead>
				<tbody>
					<c:choose>
						<c:when test="${!empty comment2}">
							<c:forEach items="${comment2}" var="com">
								<tr>
									<td>${com.id_customer}</td>
									<td>${com.user.name}</td>
									
									<c:forEach items="${commentdt }" var="comd">
									<c:if test="${comd.comment.id_customer== com.id_customer}">
										
										<c:if test="${ comd.function==1}">
											<td>
												${comd.tinnhan }
											</td>
										</c:if>
										
										
										<c:if test="${comd.function==2}">
											<td>
												${comd.tinnhan }
											</td>
										</c:if>
										
									</c:if>
									</c:forEach>
									<td>
										
<!-- 										rep -->
										
									</td>
								</tr>
							</c:forEach>
						</c:when>
						<c:otherwise>
							<tr>
								<th colspan="5" class="text-center">No Comment</th>
							</tr>
						</c:otherwise>
					</c:choose>
				</tbody>
			</table>
		</div>
	</div>
</body>
</html>