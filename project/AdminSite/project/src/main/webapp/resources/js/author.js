function sendRequest(url, data, method, callback) {
	$.ajax({
		url : url,
		type : method,
		data : data,
		contentType : 'application/json',
		success : callback,
		error : function(request, msg, error) {
			// handle failure
		}
	});
};

function sendGetRequest(url, callback) {
	sendRequest(url, "", 'GET', callback);
};

function sendPostRequest(url, data, callback) {
	sendRequest(url, data, 'POST', callback);
};

function sendPutRequest(url, data, callback) {
	sendRequest(url, data, 'PUT', callback);
};

function sendDeleteRequest(url, callback) {
	sendRequest(url, "", 'DELETE', callback);
};

function addAuthor() {
	location.href='addauthor';
}

function getAuthor(id, mode) {
	location.href='author?id=' + id + "&mode=" + mode;
};

function getAuthors() {
	var rootPath = getContextRootPath();
	location.href = rootPath + "authorcontroller/authors";
};

function deleteAuthor(id) {
	var result = confirm("Do you want to delete this author ?");
	if (result == true) {
		sendDeleteRequest('./author/' + id, function(){location.href='./authors';});
	}	
};

function getContextRootPath() {
	
	 var pathName = location.pathname.substring(1);
	 var token = pathName.split("/");	 
	 var rootContextPath = "/" + token[0] + "/";
	 
	 return rootContextPath;
}