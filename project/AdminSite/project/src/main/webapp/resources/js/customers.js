function sendRequest(url, data, method, callback) {
	$.ajax({
		url : url,
		type : method,
		data : data,
		contentType : 'application/json',
		success : callback,
		error : function(request, msg, error) {
			// handle failure
		}
	});
};

function sendGetRequest(url, callback) {
	sendRequest(url, "", 'GET', callback);
};

function sendPostRequest(url, data, callback) {
	sendRequest(url, data, 'POST', callback);
};

function sendPutRequest(url, data, callback) {
	sendRequest(url, data, 'PUT', callback);
};

function sendDeleteRequest(url, callback) {
	sendRequest(url, "", 'DELETE', callback);
};

function addCustomer() {
	location.href='addCustomer';
}

function getCustomer(id, mode) {
	location.href='customer?id=' + id + "&mode=" + mode;
};

function getCustomers() {
	var rootPath = getContextRootPath();
	location.href = rootPath + "controller/customers";
};

function check()
{
	var str=document.getElementById("repcom").value;
	if (str=="") 
	{
		return false;
	}
}

function getContextRootPath() {
	
	 var pathName = location.pathname.substring(1);
	 var token = pathName.split("/");	 
	 var rootContextPath = "/" + token[0] + "/";
	 
	 return rootContextPath;
}