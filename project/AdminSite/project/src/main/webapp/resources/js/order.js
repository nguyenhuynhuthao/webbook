function sendRequest(url, data, method, callback) {
	$.ajax({
		url : url,
		type : method,
		data : data,
		contentType : 'application/json',
		success : callback,
		error : function(request, msg, error) {
			// handle failure
		}
	});
};

function sendGetRequest(url, callback) {
	sendRequest(url, "", 'GET', callback);
};

function sendPostRequest(url, data, callback) {
	sendRequest(url, data, 'POST', callback);
};

function sendPutRequest(url, data, callback) {
	sendRequest(url, data, 'PUT', callback);
};

function sendDeleteRequest(url, callback) {
	sendRequest(url, "", 'DELETE', callback);
};


function getOrder(id, mode) {
	location.href='order?id=' + id + "&mode=" + mode;
};

function getorder() {
	var rootPath = getContextRootPath();
	location.href = rootPath + "ordercontroller/orders";
};

function deleteOrder(id) {
	var result = confirm("Do you want to delete this Order ?");
	if (result == true) {
		sendDeleteRequest('./order/' + id, function(){location.href='./orders';});
	}	
};

function getContextRootPath() {
	
	 var pathName = location.pathname.substring(1);
	 var token = pathName.split("/");	 
	 var rootContextPath = "/" + token[0] + "/";
	 
	 return rootContextPath;
}