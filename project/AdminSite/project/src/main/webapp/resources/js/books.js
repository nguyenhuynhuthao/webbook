function sendRequest(url, data, method, callback) {
	$.ajax({
		url : url,
		type : method,
		data : data,
		contentType : 'application/json',
		success : callback,
		error : function(request, msg, error) {
			// handle failure
		}
	});
};

function sendGetRequest(url, callback) {
	sendRequest(url, "", 'GET', callback);
};

function sendPostRequest(url, data, callback) {
	sendRequest(url, data, 'POST', callback);
};

function sendPutRequest(url, data, callback) {
	sendRequest(url, data, 'PUT', callback);
};

function sendDeleteRequest(url, callback) {
	sendRequest(url, "", 'DELETE', callback);
};

function addBook() {
	location.href='addBook';
}

function getBook(id, mode) {
	location.href='../bookcontroller/book?id=' + id + "&mode=" + mode;
};

function getBooks() {
	location.href = '../bookcontroller/books';
};

function deleteBook(id) {
	var result = confirm("Do you want to delete this book ?");
	if (result == true) {
		sendDeleteRequest('../bookcontroller/book/' + id, function(){location.href='./books';});
	}	
};

function getContextRootPath() {
	
	 var pathName = location.pathname.substring(1);
	 var token = pathName.split("/");	 
	 var rootContextPath = "/" + token[0] + "/";
	 
	 return rootContextPath;
}