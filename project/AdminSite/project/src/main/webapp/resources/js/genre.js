function sendRequest(url, data, method, callback) {
	$.ajax({
		url : url,
		type : method,
		data : data,
		contentType : 'application/json',
		success : callback,
		error : function(request, msg, error) {
			// handle failure
		}
	});
};

function sendGetRequest(url, callback) {
	sendRequest(url, "", 'GET', callback);
};

function sendPostRequest(url, data, callback) {
	sendRequest(url, data, 'POST', callback);
};

function sendPutRequest(url, data, callback) {
	sendRequest(url, data, 'PUT', callback);
};

function sendDeleteRequest(url, callback) {
	sendRequest(url, "", 'DELETE', callback);
};

function addGenre() {
	location.href='addGenre';
}

function getGenre(id, mode) {
	location.href='genre?id=' + id + "&mode=" + mode;
};

function getGenres() {
	var rootPath = getContextRootPath();
	location.href = rootPath + "genrecontroller/genres";
};

function deleteGenre(id) {
	var result = confirm("Do you want to delete this genre ?");
	if (result == true) {
		sendDeleteRequest('./genre/' + id, function(){location.href='./genres';});
	}	
};

function getContextRootPath() {
	
	 var pathName = location.pathname.substring(1);
	 var token = pathName.split("/");	 
	 var rootContextPath = "/" + token[0] + "/";
	 
	 return rootContextPath;
}