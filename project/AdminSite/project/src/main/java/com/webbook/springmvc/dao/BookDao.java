package com.webbook.springmvc.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.webbook.springmvc.entities.Book;


@Repository
public class BookDao extends Dao<Book>{

		@Autowired
		private SessionFactory sessionFactory;
		
		@Override
		public List<Book> getAll() {
			Session session = this.sessionFactory.getCurrentSession();
			List<Book> Books = session.createQuery("from Book").list();
			return Books;
//			return session.createQuery("from Book").list();
		}
		@Override
		public Book get(Long id) {
			Session session = this.sessionFactory.getCurrentSession();
			return (Book) session.get(Book.class, new Long(id));
		}
		@Override
		public Book add(Book book) {
			Session session = this.sessionFactory.getCurrentSession();
			session.save(book);
			return book;
		}
		@Override
		public Boolean update(Book book) {
			Session session = this.sessionFactory.getCurrentSession();
			try {
				session.update(book);
				return Boolean.TRUE;
			} catch (Exception e) {
				return Boolean.FALSE;
			}
		}
		
		@Override
		public Boolean delete(Book book) {
			Session session = this.sessionFactory.getCurrentSession();
			if (null != book) {
				try {
					session.delete(book);
					return Boolean.TRUE;
				} catch (Exception e) {
					return Boolean.FALSE;
				}
			}
			return Boolean.FALSE;
		}
		@Override
		public Boolean delete(Long id) {
			Session session = this.sessionFactory.getCurrentSession();
			Book book = (Book) session.load(Book.class, new Long(id));
			if (null != book) {
				session.delete(book);
				return Boolean.TRUE;
			}
			return Boolean.FALSE;
		}
		public List<Book> search(String chuoi)
		{
			Session session = this.sessionFactory.getCurrentSession();
			Query query = session.createQuery("from Book");
			List<Book>list1=query.list();
			List<Book> list2=new ArrayList<Book>();
			for (Book book : list1) {
				String regex = chuoi;
				Pattern pattern = Pattern.compile(regex,Pattern.CASE_INSENSITIVE);
				Matcher matcher = pattern.matcher(book.name);
				if (matcher.find()) {
					System.out.println("davo");
					list2.add(book);
				}
			}
			System.out.println(list2);
			return list2;
		}
		public List<Book> getByGenre(Long id) 
		{
			System.out.println(id);
			Session session = this.sessionFactory.getCurrentSession();
			String hql="from Book WHERE genre_id.id = :keyword";
			Query query = session.createQuery(hql);
			query.setParameter("keyword",id);
			List<Book>list=query.list();
			System.out.println(list);
			return list;
		}
}
