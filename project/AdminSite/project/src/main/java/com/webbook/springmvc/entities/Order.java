package com.webbook.springmvc.entities;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "order" , catalog = "webbook")
public class Order implements java.io.Serializable {
	private static final long serialVersionUID = 1L;
	
	private Long id_order;
	private User user;
	private Date date_purchse;
	private Integer status;
	public Order() {
	}
	
	public Order(Date date_purchse) {
		this.date_purchse = date_purchse;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	public Long getId_order() {
		return id_order;
	}

	public void setId_order(Long id_order) {
		this.id_order = id_order;
	}

	@ManyToOne(fetch=FetchType.EAGER,cascade=CascadeType.PERSIST)
	@JoinColumn(name="ID_USER",referencedColumnName="ID_USER")
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Column(name = "DATE_PURCHSE" , length = 50)
	public Date getDate_purchse() {
		return date_purchse;
	}

	public void setDate_purchse(Date date_purchse) {
		this.date_purchse = date_purchse;
	}
	
	public int getStatus() {
		return status;
	}
	
	@Override
	public String toString() {
		return "Order [id_order=" + id_order + ", user=" + user + ", date_purchse=" + date_purchse + "]";
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
	
	
	
	
}
