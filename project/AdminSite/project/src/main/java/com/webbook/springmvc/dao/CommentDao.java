package com.webbook.springmvc.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.webbook.springmvc.entities.Comment;

@Repository
public class CommentDao extends Dao<Comment> {

	@Autowired
	private SessionFactory sessionFactory;
	
	
	@Override
	public List<Comment> getAll() {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		return session.createQuery("from Comment").list();
	}

	@Override
	public Comment get(Long id) {
		Session session = this.sessionFactory.getCurrentSession();
		return (Comment) session.get(Comment.class, id);
	}

	@Override
	public Comment add(Comment t) {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(t);
		return t;
	}

	@Override
	public Boolean update(Comment t) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			session.update(t);
			return Boolean.TRUE;
		} catch (Exception e) {
			return Boolean.FALSE;				
		}
	}

	@Override
	public Boolean delete(Comment t) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Boolean delete(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

}
