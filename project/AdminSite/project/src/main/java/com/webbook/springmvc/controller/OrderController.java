package com.webbook.springmvc.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.webbook.springmvc.entities.Order;
import com.webbook.springmvc.entities.OrderDetails;
import com.webbook.springmvc.entities.User;
import com.webbook.springmvc.service.OrderDetailService;
import com.webbook.springmvc.service.OrderService;
import com.webbook.springmvc.service.UserService;

@Controller
@RequestMapping(value="/ordercontroller")
public class OrderController {

	@Autowired
	private OrderDetailService orderDetailService;
	
	@Autowired
	private OrderService orderService;
	
	@RequestMapping(value="/orders",method=RequestMethod.GET)
	public ModelAndView getAllOrder()
	{
		ModelAndView model =new ModelAndView();
		model.setViewName("orderList");//them view name
		model.addObject("orders", orderService.getAll());
		return model;
	}
	
	@RequestMapping(value="/order",method=RequestMethod.GET)
	public ModelAndView getOrderByID(@RequestParam(name="id") Long id,@RequestParam(name="mode")String mode,HttpSession session) 
	{
		ModelAndView model=new ModelAndView();
		if (mode.equalsIgnoreCase("VIEW")) {
			model.setViewName("orderDetail");//them viewname
			model.addObject("order", orderService.get(id));
			model.addObject("listorderdetail", orderDetailService.getOrderDetailByOrder(id));
			model.addObject("price", Tong(orderDetailService.getOrderDetailByOrder(id)));
		}
		if (mode.equalsIgnoreCase("CONFIRM")) 
		{
			model.setViewName("orderList");
			Order order=orderService.get(id);
			order.setStatus(1);
			orderService.update(order);
			model.addObject("orders", orderService.getAll());
			session.setAttribute("checkorder", checkorder());
		}
		return model;
	}
	
	@RequestMapping(value="/order/{id}")
	public @ResponseBody void deleteOrder(@PathVariable("id") Long id)
	{
		List<OrderDetails> orderDetails=orderDetailService.getOrderDetailByOrder(id);
		for (OrderDetails od : orderDetails) {
			orderDetailService.delete(od.getId());
			
		}
		orderService.delete(id);
	}
	
	private int Tong(List<OrderDetails> list) 
	{
		int price=0;
		for (OrderDetails od : list) {
			price+=od.quantity*od.total_price;
		}
		return price;
	}
	private int checkorder() {
		List<Order>list=orderService.getAll();
		int d=0;
		for (Order order : list) {
			if (order.getStatus()==0) {
				d++;
			}
		}
		return d;
	}
}
