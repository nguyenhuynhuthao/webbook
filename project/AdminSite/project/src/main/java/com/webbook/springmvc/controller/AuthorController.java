package com.webbook.springmvc.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.webbook.springmvc.entities.Author;
import com.webbook.springmvc.entities.Book;
import com.webbook.springmvc.service.AuthorService;


@Controller
@RequestMapping(value="/authorcontroller")
public class AuthorController {

	@Autowired
	private AuthorService authorService;
	
	@RequestMapping(value = "/authors", method = RequestMethod.GET)
	public ModelAndView getAllAuthor(HttpSession session) {
		ModelAndView model = new ModelAndView();
		if (session.getAttribute("manager")!=null) {
			List<Author> authors = authorService.getAll();
			model.setViewName("authorList");
			if (authors.size()>10) {
				if (authors.size()%10!=0) {
					model.addObject("authors", phantrang(authors, 10, 0));
					session.setAttribute("slpageauthor", authors.size()/10+1);
				}
				else {
					model.addObject("authors", phantrang(authors, 10, 0));
					session.setAttribute("slpageauthor", authors.size()/10);
				}
			}
			else {
				model.addObject("authors", authors);
			}
			
			session.setAttribute("pageauthor", 1);
			System.out.println(authors);
		}
		else {
			model.setViewName("HomeLogin");
			model.addObject("error", "Ban Chua Dang Nhap");
		}
		return model;
	}
	@RequestMapping(value = "/pageauthor", method = RequestMethod.GET)
	public ModelAndView getAllAuthorpage(HttpSession session,@RequestParam(value="page")Integer page) {
		ModelAndView model = new ModelAndView();
		if (session.getAttribute("manager")!=null) {
			List<Author> authors = authorService.getAll();
			model.setViewName("authorList");
			if (authors.size()>10) {
				if (authors.size()%10!=0) {
					model.addObject("authors", phantrang(authors, 10, (page-1)*10));
					session.setAttribute("slpageauthor", authors.size()/10+1);
				}
				else {
					model.addObject("authors", phantrang(authors, 10, 0));
					session.setAttribute("slpageauthor", authors.size()/10);
				}
			}
			else {
				model.addObject("authors", authors);
			}
			
			session.setAttribute("pageauthor", page);
			System.out.println(authors);
		}
		else {
			model.setViewName("HomeLogin");
			model.addObject("error", "Ban Chua Dang Nhap");
		}
		return model;
	}

	@RequestMapping(value = "/author", method = RequestMethod.GET)
	public ModelAndView getAuthorById(@RequestParam(name = "id") Long id, @RequestParam(name = "mode") String mode) {
		ModelAndView model = new ModelAndView();

		model.setViewName("authorDetail");
		model.addObject("mode", mode);
		model.addObject("author", authorService.get(id));

		return model;
	}

	@RequestMapping(value = "/author", method = RequestMethod.POST)
	public String saveAuthor(@ModelAttribute("author") Author author) {
		if (author.getId() == null) {
			authorService.add(author);
		} else {
			authorService.update(author);
		}

		return "redirect:/authorcontroller/authors";
	}

	@RequestMapping(value = "/addauthor", method = RequestMethod.GET)
	public ModelAndView addAuthor() {
		ModelAndView model = new ModelAndView();

		model.setViewName("authorDetail");
		model.addObject("author", new Author());
		model.addObject("mode", "EDIT");
		System.out.println(model.getModel());
		return model;
	}
	
	@RequestMapping(value = "/author/{ID_AUTHOR}", method = RequestMethod.DELETE)
	public @ResponseBody void delete(@PathVariable("ID_AUTHOR") Long id) {		
		authorService.delete(id);
	}
	private List<Author> phantrang(List<Author> list,Integer slsp,Integer vt)
	{
		Integer pointsl=0;
		Integer pointvt=0;
		int check=0;
		List<Author> books=new ArrayList<Author>();
		for (Author b : list) {
		//	System.out.println(pointsl+" "+pointvt);
			if (pointvt!=vt) {
				pointvt++;
			}
			else {
				check=1;
			}
			if (check==1) {
				if (pointsl!=slsp) {
				//	System.out.println("da cao day");
					books.add(b);
					pointsl++;
				}
				else {
					break;
				}
			}
		}
		return books;
	}
}
