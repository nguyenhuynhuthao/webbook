package com.webbook.springmvc.controller;


import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.webbook.springmvc.entities.Author;
import com.webbook.springmvc.entities.Book;
import com.webbook.springmvc.entities.Publisher;
import com.webbook.springmvc.service.AuthorService;
import com.webbook.springmvc.service.BookService;
import com.webbook.springmvc.service.GenreService;
import com.webbook.springmvc.service.PublisherService;



@Controller
@RequestMapping(value="/bookcontroller")
public class BookController {

	@Autowired
	private BookService bookService;
	
	@Autowired
	private PublisherService publisherService;
	
	@Autowired
	private AuthorService authorService;
	
	@Autowired
	private GenreService genreService;

	@RequestMapping(value="/books",method=RequestMethod.GET)
	public ModelAndView getAll(HttpSession session)
	{
		ModelAndView model=new ModelAndView();
		if (session.getAttribute("manager")!=null) {
			model.setViewName("bookList");
			model.addObject("genres", genreService.getAll());
			List<Book> listbook=new ArrayList<Book>();
			if (session.getAttribute("booksearch")!=null) {
				listbook=(List<Book>) session.getAttribute("booksearch");
				model.addObject("messsearch", session.getAttribute("messsearch"));
				session.removeAttribute("booksearch");
				session.removeAttribute("messsearch");
				model.addObject("books", listbook);
				session.setAttribute("slpagebook", 1);
			}
			else {
				listbook=getByGenre(session);
			
				if (listbook.size()>5) {
					if (listbook.size()%5!=0) {
						model.addObject("books", phantrang(listbook, 5, 0));
						session.setAttribute("slpagebook", listbook.size()/5+1);
					}
					else {
						model.addObject("books", phantrang(listbook, 5, 0));
						session.setAttribute("slpagebook", listbook.size()/5);
					}
				}
				else {
					model.addObject("books", listbook);
					session.setAttribute("slpagebook", 1);
				}
				session.setAttribute("pagebook", 1);
			}
		}
		else
		{
			model.setViewName("HomeLogin");
			model.addObject("error", "Ban Chua Dang Nhap");
		}
		return model;
	}
	
	@RequestMapping(value="/pagebook",method=RequestMethod.GET)
	public ModelAndView getAll(HttpSession session,@RequestParam(value="page")Integer page)
	{
		ModelAndView model=new ModelAndView();
		if (session.getAttribute("manager")!=null) {
			model.addObject("genres", genreService.getAll());
			model.setViewName("bookList");
			List<Book> listbook=getByGenre(session);
			if (listbook.size()>5) {
				if (bookService.getAll().size()%5!=0) {
					model.addObject("books", phantrang(listbook, 5, (page-1)*5));
					System.out.println(page);
					System.out.println(phantrang(listbook, 5, (page-1)*5));
					session.setAttribute("slpagebook", listbook.size()/5+1);
				}
				else {
					model.addObject("books", phantrang(listbook, 5, (page-1)*5));
					session.setAttribute("slpagebook", listbook.size()/5);
				}
			}
			else {
				session.setAttribute("slpagebook", 0);
				model.addObject("books", listbook);
			}
			session.setAttribute("pagebook", page);
			System.out.println("dau "+ bookService.getAll()+"duoi");
		}
		else
		{
			model.setViewName("HomeLogin");
			model.addObject("error", "Ban Chua Dang Nhap");
		}
		return model;
	}
	
	@RequestMapping(value="/genre",method=RequestMethod.GET)
	public String getGenre(@RequestParam(value="idgenre")Long idgenre,HttpSession session)
	{
		session.setAttribute("genreid", idgenre);
		return "redirect:/bookcontroller/books";
	}
	
	@RequestMapping(value="/addBook",method=RequestMethod.GET)
	public ModelAndView addBook() 
	{
		ModelAndView model=new ModelAndView();
		model.setViewName("bookDetail");
		model.addObject("book", new Book());
		model.addObject("authorlist", authorService.getAll());
		model.addObject("genres", genreService.getAll());
		model.addObject("publishers", publisherService.getAll());
		model.addObject("mode", "EDIT");
		return model;
	}
	
	@RequestMapping(value="/book", method=RequestMethod.POST)
	public String saveBook(@ModelAttribute("book")Book book) 
	{
		book.author_id=authorService.get(book.author_id.getId());
		book.genre_id=genreService.get(book.genre_id.getId());
		book.publisher=publisherService.get(book.publisher.getId());
		
//		System.out.println("xxxxxxxxxx: "+book);
		if(book.id==null)
		{
			bookService.add(book);
		}
		else
		{
			String img=bookService.get(book.id).imgage;
			System.out.println(img);
			if (book.imgage=="")
			{
				if (img!=null) {
					book.imgage=img;
				}
			}
			bookService.update(book);
		}
		return "redirect:/bookcontroller/books";
	}
	@RequestMapping(value="/book",method=RequestMethod.GET)
	public ModelAndView getBookbyId(@RequestParam Long id,@RequestParam String mode) 
	{
		ModelAndView model=new ModelAndView();
		model.setViewName("bookDetail");
		model.addObject("book", bookService.get(id));
		model.addObject("imgage", bookService.get(id).getImgage());
		if (mode.equalsIgnoreCase("EDIT")) {
			model.addObject("authorlist", authorService.getAll());
			model.addObject("authors", publisherService.getAll());
			model.addObject("publishers", publisherService.getAll());
			model.addObject("genres", genreService.getAll());
		}
		model.addObject("mode", mode);
		System.out.println(model.getModel());
		return model;
	}
	@RequestMapping(value = "/book/{id}", method = RequestMethod.DELETE)
	public @ResponseBody void btnDelete(@PathVariable(value = "id") Long id,HttpSession session) {
		System.out.println(id);
		bookService.delete(id);
		if (bookService.getAll().size()>5) {
			if (bookService.getAll().size()%5!=0) {
				session.setAttribute("slpagebook", bookService.getAll().size()/5+1);
			}
			else {
				
				session.setAttribute("slpagebook", bookService.getAll().size()/5);
			}
		}		
	}
	private List<Book> getByGenre(HttpSession session) 
	{
		Long id=(Long) session.getAttribute("genreid");
		if (id==null) {
			id=(long) 0;
		}
		if (id== (long)0) 
		{
			return bookService.getAll();
		}
		else {
			return bookService.getByGenre(id);
		}
	}
	private List<Book> phantrang(List<Book> list,Integer slsp,Integer vt)
	{
		Integer pointsl=0;
		Integer pointvt=0;
		int check=0;
		List<Book> books=new ArrayList<Book>();
		for (Book b : list) {
		//	System.out.println(pointsl+" "+pointvt);
			if (pointvt!=vt) {
				pointvt++;
			}
			else {
				check=1;
			}
			if (check==1) {
				if (pointsl!=slsp) {
				//	System.out.println("da cao day");
					books.add(b);
					pointsl++;
				}
				else {
					break;
				}
			}
		}
		return books;
	}
}
