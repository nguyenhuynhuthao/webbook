package com.webbook.springmvc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.webbook.springmvc.dao.BookDao;
import com.webbook.springmvc.dao.Dao;
import com.webbook.springmvc.entities.Author;
import com.webbook.springmvc.entities.Book;

@Transactional
@Service
public class BookService {
	
	@Autowired
	BookDao bookDAO;
	
	public List<Book> getAll(){
		return bookDAO.getAll();
	}
	
	public Book get(Long id){
		return bookDAO.get(id);
	}
	
	public Book add(Book book){
		return bookDAO.add(book);
	}
	
	public Boolean update(Book book){
		return bookDAO.update(book);
	}
	
	public Boolean delete(Book book){
		return bookDAO.delete(book);
	}
	
	public Boolean delete(Long id){
		return bookDAO.delete(id);
	}
	public List<Book> search(String book) 
	{
		return bookDAO.search(book);
	}
	public List<Book> getByGenre(Long id)
	{
		return bookDAO.getByGenre(id);
	}
}