package com.webbook.springmvc.controller;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.w3c.dom.ls.LSException;

import com.webbook.springmvc.entities.Book;
import com.webbook.springmvc.entities.Comment;
import com.webbook.springmvc.entities.ConmemntDetail;
import com.webbook.springmvc.entities.Order;
import com.webbook.springmvc.entities.OrderDetails;
import com.webbook.springmvc.entities.User;
import com.webbook.springmvc.service.AuthorService;
import com.webbook.springmvc.service.BookService;
import com.webbook.springmvc.service.CommentDetailService;
import com.webbook.springmvc.service.CommentService;
import com.webbook.springmvc.service.GenreService;
import com.webbook.springmvc.service.OrderDetailService;
import com.webbook.springmvc.service.OrderService;
import com.webbook.springmvc.service.PublisherService;
import com.webbook.springmvc.service.UserService;

@Controller
@RequestMapping(value="/admin")
public class AdminController {
	
	@Autowired
	private CommentService commentService;
	@Autowired
	private OrderDetailService orderDetailService;
	@Autowired 
	private OrderService orderService;
	@Autowired
	private AuthorService authorService;
	@Autowired
	private UserService userService;
	@Autowired
	private BookService bookService;
	@Autowired
	private PublisherService publisherService;
	@Autowired
	private GenreService genreService;
	@Autowired
	private CommentDetailService commentDetailService;
	
	@RequestMapping(value="/homelogin",method=RequestMethod.GET)
	public ModelAndView HomeAdmin()
	{
		ModelAndView model=new ModelAndView();
		model.setViewName("HomeLogin");
		model.addObject("error", "");
//		System.out.println("da vao day");
		return model;
		
	}
	
	@RequestMapping(value="/homelogin1",method=RequestMethod.GET)
	public ModelAndView HomeAdmin1()
	{
		ModelAndView model=new ModelAndView();
		model.setViewName("HomeLogin");
		model.addObject("error", "Sai UserName Hoac Mat Khau");
//		System.out.println("da vao day");
		return model;
		
	}
	
	@RequestMapping(value="/login",method=RequestMethod.GET)
	public String Login(@RequestParam(value="name")String username,@RequestParam(value="pass")String password,HttpSession session) 
	{
		User user=CheckUser(username, password);
		if (user!=null) 
		{
			if (user.getRole()==0) {
				session.setAttribute("manager", user);
				return "redirect:/admin/view";
			}
			
		}
		return "redirect:/admin/homelogin1";
	}
	
	@RequestMapping(value="/view",method=RequestMethod.GET)
	public ModelAndView ViewAdmin(HttpSession session) 
	{
		ModelAndView model =new ModelAndView();
		if (session.getAttribute("manager")!=null) {
			model.setViewName("AdminPage");
			model.addObject("messages", getCommentDT(commentService.getAll(), commentDetailService.getAll()));
			model.addObject("messagedt",  commentDetailService.getAll());
			model.addObject("price", GetPrice());
			model.addObject("sizeorder", orderService.getAll().size());
			model.addObject("sizemessages",  commentService.getAll().size());
			model.addObject("pri",0);
			session.setAttribute("checkorder", checkorder());
			session.setAttribute("checkmessages", getCommentDT(commentService.getAll(), commentDetailService.getAll()).size());

		}
		else {
			model.setViewName("HomeLogin");
			model.addObject("error", "Ban Chua Dang Nhap");
		}
		return model;
	}
	private int checkorder() {
		List<Order>list=orderService.getAll();
		int d=0;
		for (Order order : list) {
			if (order.getStatus()==0) {
				d++;
			}
		}
		return d;
	}
	@RequestMapping(value="/getprice",method=RequestMethod.GET)
	public ModelAndView GetPriceByDate(@RequestParam(value="date1")String date1,@RequestParam(value="date2")String date2)
	{
		ModelAndView model=new ModelAndView();
		model.setViewName("AdminPage");
		model.addObject("messages", getCommentDT(commentService.getAll(), commentDetailService.getAll()));
		model.addObject("messagedt",  commentDetailService.getAll());
		model.addObject("price", GetPrice());
		model.addObject("sizeorder", orderService.getAll().size());
		model.addObject("sizemessages", getCommentDT(commentService.getAll(), commentDetailService.getAll()).size());
		model.addObject("pri",0);
		model.addObject("pri", getOrderByDate(date1, date2));
		model.addObject("bieudo", getPriceByDateOrMonth(date1, date2));
		model.addObject("datebd", getDate1(date1, date2));
		return model;
	}
	
	@RequestMapping(value="/get",method=RequestMethod.GET)
	public String GetByName(@RequestParam(value="search")String search,HttpSession session)
	{
		session.setAttribute("booksearch", bookService.search(search));
		session.setAttribute("messsearch", "Ket Qua Tim kiem Cho: "+search);
		return "redirect:/bookcontroller/books";
		
	}
//	@RequestMapping(value="/search",method=RequestMethod.GET)
//	public List<Book> GetBookByName(@RequestParam(value="search")String search)
//	{
//
//		return bookService.search(search);
//		
//	}
	@RequestMapping(value="/logout",method=RequestMethod.GET)
	public ModelAndView logout(HttpSession session) 
	{
		ModelAndView model=new ModelAndView();
		model.setViewName("HomeLogin");
		session.removeAttribute("manager");
		return model;
	}
	
	private List<Comment> getCommentDT(List<Comment> set,List<ConmemntDetail> cDetails) {
		List<Comment> set2=new ArrayList<Comment>();
		
		for (Comment comment : set) {
			int n=0;
			for (ConmemntDetail Detail : cDetails) {
				if (comment.getId_customer()==Detail.getComment().getId_customer()) {
					n++;
				}
			}
			if (n==1) {
				set2.add(comment);
			}
		}
		return set2;
	}
	private List<Integer> getDate1(String date1,String date2) 
	{
		List<Integer> list=new ArrayList<Integer>();
		try {
			SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
			Date beginDate = (Date) dateFormatter.parse(date1);
			Date enDate =(Date)dateFormatter.parse(date2);
			if ((enDate.getYear()-beginDate.getYear())==0) {
				if ((enDate.getMonth()-beginDate.getMonth())==0) {
					for (int i = beginDate.getDate(); i <= enDate.getDate(); i++) {
						list.add(i);
					}
				}
				else {
					for (int i = beginDate.getMonth(); i <= enDate.getMonth(); i++) {
						list.add(i+1);
					}
				}
			}
			else {
				for (int i = beginDate.getMonth(); i <= enDate.getMonth(); i++) {
					list.add(i);
				}
			}
			
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
		}
		return list;
	}
	private int getOrderByDate(String date1,String date2) 
	{
		List<Order> orderlist=orderService.getorderbydate(date1, date2);
		
		List<OrderDetails> orderDetaillist=orderDetailService.getAll();
		System.out.println(orderlist+" "+orderDetaillist);
		int pri=0;
		for (Order order : orderlist) {
			for (OrderDetails orderDetails : orderDetaillist) {
				if (orderDetails.order_id.getId_order()==order.getId_order()) {
					pri+=orderDetails.total_price;
				}
			}
		}
		return pri;
	}
	private User CheckUser(String username,String password) 
	{
		List<User> list=userService.getAll();
		for (User user : list) {
			if (username.equalsIgnoreCase(user.getName())) {
				if(password.equalsIgnoreCase(user.getPassword()))
				{
					return user;
				}
			}
		}
		return null;
	}
	
	public List<Integer> getPriceByDateOrMonth(String date1,String date2) 
	{
		List<Integer> list=new ArrayList<Integer>();
		List<Order> orderlist=orderService.getorderbydate(date1, date2);
		List<OrderDetails> orderDetaillist=orderDetailService.getAll();
		try {
			SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
			Date beginDate = (Date) dateFormatter.parse(date1);
			Date enDate =(Date)dateFormatter.parse(date2);
			System.out.println("year="+(enDate.getYear()));
			if ((enDate.getYear()-beginDate.getYear())==0) {
				if ((enDate.getMonth()-beginDate.getMonth())==0) {
					list=getpircebydate(beginDate, enDate, orderlist, orderDetaillist);
				}
				else {
					System.out.println("da vao day");
					list=getPriceByMonth(beginDate, enDate, orderlist, orderDetaillist);
				}
			}
			else {
				list=getPriceByYear(beginDate, enDate, orderlist, orderDetaillist);
			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;
	}
	private List<Integer> getPriceByMonth(Date beginDate,Date enDate,List<Order> orderlist,List<OrderDetails> orderDetaillist) 
	{
		List<Integer> list=new ArrayList<Integer>();
		for (int i = (beginDate.getMonth()); i <= (enDate.getMonth()); i++) {
			for (Order order : orderlist) {
				if (i==order.getDate_purchse().getMonth()&&order.getStatus()==1) {
					for (OrderDetails oDetails : orderDetaillist) {
						if (oDetails.getOrder_id().getId_order()==order.getId_order()) {
							if ((i-beginDate.getMonth())>=list.size()) {
							
								list.add(oDetails.getTotal_price());
							}
							else {
								
								list.set(i-beginDate.getMonth(), list.get(i-beginDate.getMonth())+oDetails.getTotal_price());
							}
						}
					}
				}
				else {
					if ((i-beginDate.getMonth())>=list.size()) {
						list.add(0);
					}
				}
			}
		}
		return list;
	}
	private List<Integer> getPriceByYear(Date beginDate,Date enDate,List<Order> orderlist,List<OrderDetails> orderDetaillist) 
	{
		List<Integer> list=new ArrayList<Integer>();
		for (int i = (beginDate.getYear()); i <= (enDate.getYear()); i++) {
			for (Order order : orderlist) {
				if (i==order.getDate_purchse().getYear()&&order.getStatus()==1) {
					for (OrderDetails oDetails : orderDetaillist) {
						if (oDetails.getOrder_id().getId_order()==order.getId_order()) {
							if ((i-beginDate.getYear())>=list.size()) {
							
								list.add(oDetails.getTotal_price());
							}
							else {
								
								list.set(i-beginDate.getYear(), list.get(i-beginDate.getYear())+oDetails.getTotal_price());
							}
						}
					}
				}
				else {
					if ((i-beginDate.getYear())>=list.size()) {
						list.add(0);
					}
				}
			}
		}
		return list;
	}
	private List getpircebydate(Date beginDate,Date enDate,List<Order> orderlist,List<OrderDetails> orderDetaillist) 
	{
		List<Integer> list=new ArrayList<Integer>();
		for (int i = (beginDate.getDate()); i <= (enDate.getDate()); i++) {
			for (Order order : orderlist) {
				if (i==order.getDate_purchse().getDate()&&order.getStatus()==1) {
					System.out.println(order.getStatus());
					for (OrderDetails oDetails : orderDetaillist) {
						if (oDetails.getOrder_id().getId_order()==order.getId_order()) {
							if ((i-beginDate.getDate())>=list.size()) {
							
								list.add(oDetails.getTotal_price());
							}
							else {
								
								list.set(i-beginDate.getDate(), list.get(i-beginDate.getDate())+oDetails.getTotal_price());
							}
						}
					}
				}
				else {
					if ((i-beginDate.getDate())>=list.size()) {
						list.add(0);
					}
				}
			}
		}
		return list;
	}
	
	private int GetPrice() {
		List<OrderDetails>list=orderDetailService.getAll();
		int price=0;
		for (OrderDetails od : list) {
			price+=od.total_price;
		}
		return price;
	}
}
