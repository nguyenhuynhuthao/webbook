package com.webbook.springmvc.entities;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "comment" , catalog = "webbook")
public class Comment implements java.io.Serializable {
	private static final long serialVersionUID = 1L;
	
	private Long id_customer;
	private User user;
	private Book book;
	
	
	public Comment() {
	}
	
	
	public Comment(Long id_customer, User user, Book book) {
		this.id_customer = id_customer;
		this.user = user;
		this.book = book;
	}


	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ID_CUSTOMER", unique = true, nullable = false)
	public Long getId_customer() {
		return id_customer;
	}

	public void setId_customer(Long id_customer) {
		this.id_customer = id_customer;
	}

	

	@ManyToOne(fetch=FetchType.EAGER,cascade=CascadeType.ALL)
	@JoinColumn(name="user_id",nullable = false)
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	

	@ManyToOne(fetch=FetchType.EAGER,cascade=CascadeType.ALL)
	@JoinColumn(name="book_id",nullable = false)
	public Book getBook() {
		return book;
	}

	public void setBook(Book book) {
		this.book = book;
	}

	@Override
	public String toString() {
		return "Comment [id_customer=" + id_customer + ", user=" + user ;
	}

	
	

	
	
}
