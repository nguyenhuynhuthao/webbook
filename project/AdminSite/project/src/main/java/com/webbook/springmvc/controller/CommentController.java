package com.webbook.springmvc.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;


import com.webbook.springmvc.entities.Comment;
import com.webbook.springmvc.entities.ConmemntDetail;
import com.webbook.springmvc.service.CommentDetailService;
import com.webbook.springmvc.service.CommentService;

@Controller
@RequestMapping(value="/commentcontroller")
public class CommentController {

	@Autowired
	private CommentDetailService commentDetailService;
	
	@Autowired
	private CommentService commentService;
	
	@RequestMapping(value = "/comments", method = RequestMethod.GET)
	public ModelAndView getAllCustomer(HttpSession session) {
		ModelAndView model = new ModelAndView();
		if (session.getAttribute("manager")!=null)
		{
			model.setViewName("customerList");
			model.addObject("commentdt", commentDetailService.getAll());
			model.addObject("comment2", getCommentDT2(commentService.getAll(), commentDetailService.getAll()));
			model.addObject("comment1", getCommentDT1(commentService.getAll(), commentDetailService.getAll()));
		}
		else {
			model.setViewName("HomeLogin");
			model.addObject("error", "Ban Chua Dang Nhap");
		}
		return model;
	}

	@RequestMapping(value="/repcomment",method=RequestMethod.GET)
	public String RepComment(@RequestParam(value="mesrep")String mesrep,@RequestParam(value="idcom")Long id,HttpSession session) 
	{
		ConmemntDetail detail=new ConmemntDetail();
		detail.setComment(commentService.get(id));
		detail.setFunction(2);
		detail.setTinnhan(mesrep);
		commentDetailService.add(detail);
		session.removeAttribute("checkmessages");
		return "redirect:/commentcontroller/comments";
	}
	
	private List<Comment> getCommentDT1(List<Comment> set,List<ConmemntDetail> cDetails) {
		List<Comment> set2=new ArrayList<Comment>();
		
		for (Comment comment : set) {
			int n=0;
			for (ConmemntDetail Detail : cDetails) {
				if (comment.getId_customer()==Detail.getComment().getId_customer()) {
					n++;
				}
			}
			if (n==1) {
				set2.add(comment);
			}
		}
		return set2;
	}
	private List<Comment> getCommentDT2(List<Comment> set,List<ConmemntDetail> cDetails) {
		List<Comment> set2=new ArrayList<Comment>();
		
		for (Comment comment : set) {
			int n=0;
			for (ConmemntDetail Detail : cDetails) {
				if (comment.getId_customer()==Detail.getComment().getId_customer()) {
					n++;
				}
			}
			if (n==2) {
				set2.add(comment);
			}
		}
		return set2;
	}
}
