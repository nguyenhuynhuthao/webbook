<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>privacypolicy</title>
       <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="<c:url value="/resources/page/images/ico/favicon.ico"/>">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<c:url value="/resources/page/images/ico/apple-touch-icon-144-precomposed.png"/>">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<c:url value="/resources/page/images/ico/apple-touch-icon-114-precomposed.png"/>">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<c:url value="/resources/page/images/ico/apple-touch-icon-72-precomposed.png"/>">
    <link rel="apple-touch-icon-precomposed" href="<c:url value="/resources/page/images/ico/apple-touch-icon-57-precomposed.png"/>">
<link href="<c:url value="../resources/manager/css/style.css"/>" rel='stylesheet' type='text/css'/>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Bonfire Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!--fonts-->
<link href="<c:url value='http://fonts.googleapis.com/css?family=Exo:100,200,300,400,500,600,700,800,900'/>" rel='stylesheet' type='text/css'>
<!--//fonts-->
<link rel="stylesheet"
	href="<c:url value="/resources/page/css/bootstrap.min.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/page/css/font-awesome.min.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/page/css/prettyPhoto.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/page/css/price-range.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/page/css/animate.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/page/css/main.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/page/css/responsive.css" />">
</head>
<body>
	<jsp:include page="header.jsp"></jsp:include>

	<div class="container ">
		<div class="ty-wysiwyg-content" data-ca-live-editor-object-id="0"
			data-ca-live-editor-object-type="">
			<h1 align="center" dir="ltr" rel="line-height:2.1;margin-top:0pt;margin-bottom:0pt;"
				style="margin-top: 0pt; margin-bottom: 0pt; font-family: Arial, Helvetica, Verdana, Tahoma, sans-serif; line-height: 2.1;">Chính
				Sách Bảo Mật</h1>
			<p dir="ltr"
				style="margin-top: 5pt; margin-bottom: 0pt; line-height: 1.38; color: rgb(51, 51, 51);">1.
				Mục đích và phạm vi thu thập</p>
			<p dir="ltr"
				style="margin-top: 5pt; margin-bottom: 0pt; line-height: 1.38; color: rgb(51, 51, 51);">Việc
				thu thập dữ liệu chủ yếu trên Sàn TMĐT bao gồm: email,
				điện thoại, tên đăng nhập, mật khẩu đăng nhập, địa chỉ Khách hàng.
				Đây là các thông tin mà chúng tôi cần Khách hàng cung cấp bắt buộc
				khi đăng ký sử dụng dịch vụ và chúng tôi sử dụng nhằm liên hệ xác
				nhận khi Khách hàng đăng ký sử dụng dịch vụ trên website, đảm bảo
				quyền lợi cho Khách hàng.</p>
			<p dir="ltr"
				style="margin-top: 5pt; margin-bottom: 0pt; line-height: 1.38; color: rgb(51, 51, 51);">Các
				Khách hàng sẽ tự chịu trách nhiệm về bảo mật và lưu giữ mọi hoạt
				động sử dụng dịch vụ dưới tên đăng ký, mật khẩu và hộp thư điện tử
				của mình. Ngoài ra, Khách hàng có trách nhiệm thông báo kịp thời cho
				Sàn TMĐT về những hành vi sử dụng trái phép, lạm dụng, vi
				phạm bảo mật, lưu giữ tên đăng ký và mật khẩu của bên thứ ba để có
				biện pháp giải quyết phù hợp.</p>
			<p dir="ltr"
				style="margin-top: 5pt; margin-bottom: 0pt; line-height: 1.38; color: rgb(51, 51, 51);">2.
				Phạm vi sử dụng thông tin</p>
			<p dir="ltr"
				style="margin-top: 5pt; margin-bottom: 0pt; line-height: 1.38; color: rgb(51, 51, 51);">Sàn
				TMĐT sử dụng thông tin Khách hàng cung cấp để:</p>
			<ul>
				<li dir="ltr">Cung cấp các dịch vụ đến Khách hàng.</li>
				<li dir="ltr">Gửi các thông báo về các hoạt động trao đổi thông
					tin giữa Khách hàng và Sàn TMĐT vn.</li>
				<li dir="ltr">Ngăn ngừa các hoạt động phá hủy tài khoản người
					dùng của Khách hàng hoặc các hoạt động giả mạo Khách hàng.</li>
				<li dir="ltr">Liên lạc và giải quyết với khách hàng trong những
					trường hợp đặc biệt.</li>
				<li dir="ltr">Không sử dụng thông tin cá nhân của Khách hàng
					ngoài mục đích xác nhận và liên hệ có liên quan đến giao dịch</li>
				<li dir="ltr">Sàn TMĐT vn có trách nhiệm hợp tác cung cấp thông
					tin cá nhân Khách hàng khi có yêu cầu từ cơ quan nhà nước có thẩm
					quyền.</li>
			</ul>
			<p dir="ltr"
				style="margin-top: 5pt; margin-bottom: 0pt; line-height: 1.38; color: rgb(51, 51, 51);">3.
				Thời gian lưu trữ thông tin</p>
			<p dir="ltr"
				style="margin-top: 5pt; margin-bottom: 0pt; line-height: 1.38; color: rgb(51, 51, 51);">Dữ
				liệu cá nhân của Khách hàng sẽ được lưu trữ cho đến khi có yêu cầu
				hủy bỏ hoặc tự Khách hàng đăng nhập và thực hiện hủy bỏ. Còn lại
				trong mọi trường hợp thông tin cá nhân Khách hàng sẽ được bảo mật
				trên máy chủ của website</p>
			<p dir="ltr"
				style="margin-top: 5pt; margin-bottom: 0pt; line-height: 1.38; color: rgb(51, 51, 51);">4.
				Địa chỉ của đơn vị thu thập, quản lý thông tin và hỗ trợ Khách hàng</p>
			<ul>
				<li dir="ltr">Công ty MTV chưa có tên</li>
				<li dir="ltr">Hotline: 0703132898</li>
				<li dir="ltr">Email: abcxyz@gmail.com</li>
			</ul>
			<p dir="ltr"
				style="margin-top: 5pt; margin-bottom: 0pt; line-height: 1.38; color: rgb(51, 51, 51);">5.
				Phương tiện và công cụ để Khách hàng tiếp cận và chỉnh sửa dữ liệu
				của mình</p>
			<p dir="ltr"
				style="margin-top: 5pt; margin-bottom: 0pt; line-height: 1.38; color: rgb(51, 51, 51);">Khách
				hàng có quyền tự kiểm tra, cập nhật, điều chỉnh thông tin cá nhân
				của mình bằng cách đăng nhập vào tài khoản và chỉnh sửa thông tin cá
				nhân hoặc yêu cầu Nhân Văn thực hiện việc này.</p>
			<p dir="ltr"
				style="margin-top: 5pt; margin-bottom: 0pt; line-height: 1.38; color: rgb(51, 51, 51);">Khách
				hàng có quyền gửi khiếu nại về việc lộ thông tin các nhân cho bên
				thứ ba đến Ban quản trị của Sàn TMĐT. Khi tiếp nhận những
				phản hồi này, Nhân viên sẽ xác nhận lại thông tin, phải có trách
				nhiệm trả lời lý do và hướng dẫn Khách hàng khôi phục và bảo mật lại
				thông tin.</p>
			<p dir="ltr"
				style="margin-top: 5pt; margin-bottom: 0pt; line-height: 1.38; color: rgb(51, 51, 51);">6.
				Cam kết bảo mật thông tin cá nhân Khách hàng</p>
			<p dir="ltr"
				style="margin-top: 5pt; margin-bottom: 0pt; line-height: 1.38; color: rgb(51, 51, 51);">Thông
				tin cá nhân của Khách hàng trên website được chúng tôi cam kết
				bảo mật tuyệt đối theo chính sách bảo vệ thông tin cá nhân của
				website. Việc thu thập và sử dụng thông tin của mỗi Khách hàng
				chỉ được thực hiện khi có sự đồng ý của Khách hàng đó, trừ những
				trường hợp pháp luật có quy định khác. Chúng tôi cam kết:</p>
			<ul>
				<li dir="ltr">Không sử dụng, không chuyển giao, cung cấp hay
					tiết lộ cho bên thứ ba nào về thông tin cá nhân của Khách hàng khi
					không có sự cho phép hoặc đồng ý từ Khách hàng, trừ những trường
					hợp pháp luật có quy định khác.</li>
				<li dir="ltr">Trong trường hợp máy chủ lưu trữ thông tin bị
					hacker tấn công dẫn đến mất mát dữ liệu cá nhân Khách hàng,Chúng 
					tôi sẽ có trách nhiệm thông báo vụ việc cho cơ quan chức năng điều
					tra xử lý kịp thời và thông báo cho Khách hàng được biết.</li>
				<li dir="ltr">Bảo mật tuyệt đối mọi thông tin giao dịch trực
					tuyến của Khách hàng bao gồm thông tin hóa đơn, chứng từ kế toán số
					hóa tại khu vực dữ liệu trung tâm an toàn cấp 1 của Chúng tôi.</li>
			</ul>
		</div>
	</div>

	<jsp:include page="footer.jsp"></jsp:include>
	 <script src="../resources/page/js/jquery.js"></script>
	<script src="../resources/page/js/price-range.js"></script>
    <script src="../resources/page/js/jquery.scrollUp.min.js"></script>
	<script src="../resources/page/js/bootstrap.min.js"></script>
    <script src="../resources/page/js/jquery.prettyPhoto.js"></script>
    <script src="../resources/page/js/main.js"></script>
</body>
</html>