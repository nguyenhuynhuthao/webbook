<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Product</title>
 <link rel="shortcut icon" href="<c:url value="/resources/page/images/ico/favicon.ico"/>">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<c:url value="/resources/page/images/ico/apple-touch-icon-144-precomposed.png"/>">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<c:url value="/resources/page/images/ico/apple-touch-icon-114-precomposed.png"/>">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<c:url value="/resources/page/images/ico/apple-touch-icon-72-precomposed.png"/>">
    <link rel="apple-touch-icon-precomposed" href="<c:url value="/resources/page/images/ico/apple-touch-icon-57-precomposed.png"/>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Bonfire Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!--fonts-->
<link href="<c:url value='http://fonts.googleapis.com/css?family=Exo:100,200,300,400,500,600,700,800,900'/>" rel='stylesheet' type='text/css'>
<!--//fonts-->
<link rel="stylesheet"
	href="<c:url value="/resources/page/css/bootstrap.min.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/page/css/font-awesome.min.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/page/css/prettyPhoto.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/page/css/price-range.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/page/css/animate.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/page/css/main.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/page/css/responsive.css" />">
<link href="<c:url value="../resources/manager/css/style.css"/>" rel='stylesheet' type='text/css'/>
				<script type="text/javascript">
					jQuery(document).ready(function($) {
						$(".scroll").click(function(event){		
							event.preventDefault();
							$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
						});
					});
				</script>
<!--slider-script-->
			<script>
				$(function () {
				  $("#slider1").responsiveSlides({
					auto: true,
					speed: 500,
					namespace: "callbacks",
					pager: true,
				  });
				});
			</script>
<!--//slider-script-->
<script>$(document).ready(function(c) {
	$('.alert-close').on('click', function(c){
		$('.message').fadeOut('slow', function(c){
	  		$('.message').remove();
		});
	});	  
});
</script>
<script>$(document).ready(function(c) {
	$('.alert-close1').on('click', function(c){
		$('.message1').fadeOut('slow', function(c){
	  		$('.message1').remove();
		});
	});	  
});
</script>
<style type="text/css">
	#img{
		width: 400px;
		height: 400px;
	}
	span {
	text-align: right;
}
</style>
</head>
<body>
	<jsp:include page="header.jsp"></jsp:include>
	<div class="container">
		<div class="row">
			<div class="rowtop">
				<h2>${product.name }</h2>
			</div>
			<div class="productinfo text-center">
				 <div class="col-sm-6">
				 	<img id="img" alt="" src="../resources/page/images/home/${product.imgage}" >
				 </div>
				 <div class="col-sm-6">
				 	<div class="alert alert-success">
					 	<span>
					 		<h3>Book Detail</h3>
					 	</span>
					 	<span>
					 	<h4>Giá: ${product.price }</h4>
					 	</span>
					 	<span>
					 	<h4>Tác Giả: ${product.author_id.name }</h4>
					 	
					 	
					 	<h4>Nhà Xuất Bản: ${product.publisher.publisher_name }</h4>
					 	
					 	
					 	<h4>Thể Loại: ${product.genre_id.description }</h4>
					 	</span>
				 	</div>
				 	<span>
					 	<c:if test="${mode == 'LOGIN' }">
								<a href="../cartcontroller/addcart?id=${product.id}" class="btn btn-default add-to-cart"><i
									class="fa fa-shopping-cart"></i>Add to cart</a>
						</c:if>
						<c:if test="${mode == null || mode == 'LOGOUT' }">
								<a href="../controller/logins" class="btn btn-default add-to-cart"><i
									class="fa fa-shopping-cart"></i>Add to cart</a>
						</c:if>
					</span>
				 </div>
				 
			</div>
		</div>
		<div class="row">
			<h2><div class="rowtop">San pham cung the loai</div> </h2>
		<c:forEach items="${bookli}" var="book">
			<div class="col-sm-3">
				<div class="product-image-wrapper">
					<div class="single-products">
						<div class="productinfo text-center" style="width: 210px;height: 350px">
							<a href="../controller/product?id=${book.id }">
							<img src="../resources/page/images/home/${book.imgage}" alt="" style="width: 207px;height: 200px"/>
							<h2>${book.price }</h2>
							<p>${book.name }</p>
							</a>
							<a href="../cartcontroller/addcart?id=${book.id}" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Thêm vào giỏ hàng</a>
						</div>
						<div class="product-overlay">
							<div class="overlay-content">
								<a href="../controller/product?id=${book.id }">
								<h2>${book.price }</h2>
								<p>${book.name }</p>
								</a>
								<a href="../cartcontroller/addcart?id=${book.id}" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Thêm vào giỏ hàng</a>
							</div>
						</div>
					</div>
				</div>
			
		
			</div>
		</c:forEach>
		</div>
		<div	class="row">
			<div class="col-sm-6">
				<div class="message">
				<c:if test="${!empty username.id }">
					<form action="../controller/comment">
						<input type="hidden" name="user" value="${username.id } ">
						<input type="hidden" name="bookid" value="${product.id } ">
						<textarea name="message" style="width:400px; height:100px;" placeholder="Mời bạn để lại bình luận..."></textarea>
						<button type="submit" class="btn btn-info">Gửi</button>
					</form>
				</c:if>
				<div>Có: ${tongbl } bình luận trong sản phẩm</div>
				<hr>
				
				<div>
					<c:if test="${tongbl >0}">
						<ul class="comment_list">
						<c:forEach items="${comments}" var="comt">
							<li class="comment_ask">
								<div class="well">
								<div class="rowuser">Ten Khach hang: ${comt.user.name}</div>
								
								<c:forEach items="${comdt }" var="comd">
									
									<c:if test="${comd.comment.id_customer== comt.id_customer}">
										
										<c:if test="${ comd.function==1}">
											<div class="alert alert-info pull-left">
												${comd.tinnhan }
											</div>
										</c:if>
										
										<br>
										
										<c:if test="${comd.function==2}">
											<div class="alert alert-info pull-right">
												Admin: 	${comd.tinnhan }
											</div>
										</c:if>
										
									</c:if>
									
									
								</c:forEach>
								</div>
								<hr>
							</li>
						</c:forEach>
						</ul>
					</c:if>
				</div>
			</div>
		</div>
	</div>
	
	<jsp:include page="footer.jsp"></jsp:include>

	    <script src="../resources/page/js/jquery.js"></script>
	<script src="../resources/page/js/price-range.js"></script>
    <script src="../resources/page/js/jquery.scrollUp.min.js"></script>
	<script src="../resources/page/js/bootstrap.min.js"></script>
    <script src="../resources/page/js/jquery.prettyPhoto.js"></script>
    <script src="../resources/page/js/main.js"></script>
</body>
</html>