<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>banner</title>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
<style type="text/css">
	li {
		font-size: 17px;
}
</style>
</head>
<body>

	<section id="slider"><!--slider-->
		<div class="container">
			<div class="row">
				<div class="col-sm-3">
				<div class="title h3 text-center">
					Thể Loại
				</div>
				<div class="padding-left">
				<ul class="list-group">
						<c:forEach items="${genres}" var="genre">
							<li><a href="../controller/getgenres?id=${genre.id }">${genre.description }</a></li>
						</c:forEach>
					</ul>
				</div>
			</div>
				<div class="col-sm-9">
					<div id="MyCarousel" class="carousel slide" data-ride="carousel">
						<ol class="carousel-indicators">
							<li data-target="#MyCarousel" data-slide-to="0" class="active"></li>
							<li data-target="#MyCarousel" data-slide-to="1"></li>
							<li data-target="#MyCarousel" data-slide-to="2"></li>
						</ol>
						
						<div class="carousel-inner" role="listbox">
							<div class="item active" >
								
									<img src=" <c:url value="https://nhanvan.vn/images/promo/63/banner-1920-500.jpg"/>" class="img-responsive" alt="" s/>
								
							</div>
							<div class="item">
								
									<img src="<c:url value="https://nhanvan.vn/images/promo/59/home_khosachtre.jpg"/>" class="img-responsive" alt="" />
								
							</div>
							<div class="item">
								
									<img src="<c:url value="https://nhanvan.vn/images/promo/66/165banner_h%E1%BB%99i_s%C3%A1ch_1920-726.jpg"/>" class="img-responsive" alt="" />
								
							</div>
							
						</div>
						
						<a href="#MyCarousel" class="left control-carousel hidden-xs" data-slide="prev">
							<i class="fa fa-angle-left"></i>
						</a>
						<a href="#MyCarousel" class="right control-carousel hidden-xs" data-slide="next">
							<i class="fa fa-angle-right"></i>
						</a>
					</div>
					
				</div>
			</div>
		</div>
	</section><!--/slider-->

</body>
</html>