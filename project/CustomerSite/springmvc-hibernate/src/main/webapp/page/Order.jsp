<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Giỏ hàng</title>
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="<c:url value="/resources/page/images/ico/favicon.ico"/>">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<c:url value="/resources/page/images/ico/apple-touch-icon-144-precomposed.png"/>">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<c:url value="/resources/page/images/ico/apple-touch-icon-114-precomposed.png"/>">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<c:url value="/resources/page/images/ico/apple-touch-icon-72-precomposed.png"/>">
    <link rel="apple-touch-icon-precomposed" href="<c:url value="/resources/page/images/ico/apple-touch-icon-57-precomposed.png"/>">
<link href="<c:url value="../resources/manager/css/style.css"/>" rel='stylesheet' type='text/css'/>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Bonfire Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!--fonts-->
<link href="<c:url value='http://fonts.googleapis.com/css?family=Exo:100,200,300,400,500,600,700,800,900'/>" rel='stylesheet' type='text/css'>
<!--//fonts-->
<link rel="stylesheet"
	href="<c:url value="/resources/page/css/bootstrap.min.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/page/css/font-awesome.min.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/page/css/prettyPhoto.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/page/css/price-range.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/page/css/animate.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/page/css/main.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/page/css/responsive.css" />">
</head>
<body>

	<jsp:include page="header.jsp"></jsp:include>
	
	<section id="cart_items">
		<div class="container">
			<div class="breadcrumbs">
				<ol class="breadcrumb">
				  <li><a href="index.jsp">Trang chủ</a></li>
				  <li class="active">Đơn Hàng</li>
				</ol>
			</div>
			<p style="color:red">${tb }</p>
			<div class="table-responsive cart_info">
				<c:if test="${mode=='VIEW' }">
					<table class="table table-condensed">
						<thead>
							<tr class="cart_menu">
								<td class="id">ID</td>
								<td class="dateorder">Ngày </td>
								<td class="status">Tình Trạng</td>
								<td class="total">chi tiet</td>
								<td></td>
							</tr>
						</thead>
						<tbody>
						<c:forEach items="${orders}" var="order" >
							<tr>
								<td class="cart_product">
									${order.id_order }
								</td>
								<td class="cart_description">
									${order.date_purchse }
								</td>
								<td class="cart_price">
									<c:if test="${order.status==0 }">Chưa Xác Nhận</c:if>
									<c:if test="${order.status==1 }">Đã Xác Nhận</c:if>
									<c:if test="${order.status==3 }">Đã huy</c:if>
								</td>
								<td>
									<a href="./viewdt?id=${order.id_order}" class="btn btn-primary">Chi Tiet</a>
								</td>
							</tr>
						</c:forEach>
						</tbody>
					</table>
				</c:if>
				<c:if test="${mode=='VIEWDT' }">
				<div class="pull-right">
					<c:if test="${orders.status==0 }">
						<a class="btn btn-default" href="../cartcontroller/cancel?id=${orders.id_order }" onclick="return kt();">Huy don hang</a>
						<script type="text/javascript">
							function kt() {
								return confirm("ban co chac muon huy!");
							}
						</script>
					</c:if>
				</div>
				<div >
					<table class="table table-condensed" >
						<thead>
							<tr class="cart_menu">
								<td class="id">ID</td>
								<td class="dateorder">Ngày </td>
								<td class="status">Tình Trạng</td>
							</tr>
						</thead>
						<tbody>
						
							<tr>
								<td class="cart_product">
									${orders.id_order }
								</td>
								<td class="cart_description">
									${orders.date_purchse }
								</td>
								<td class="cart_price">
									<c:if test="${orders.status==0 }">Chưa Xác Nhận</c:if>
									<c:if test="${orders.status==1 }">Đã Xác Nhận</c:if>
									<c:if test="${orders.status==3 }">Đã huy</c:if>
								</td>
							</tr>
						
						</tbody>
					</table>
					</div>
					<div class="alert alert-success" >
						<p >Dia Chi Giao hang</p>
						<p>Ten: ${username.userDetails.fullname }</p>
						<p>Dia Chi: ${username.userDetails.address.street}, ${username.userDetails.address.district}, ${username.userDetails.address.city}
						
					</div>
					<div >
					<table class="table table-condensed" >
						<thead>
							<tr class="cart_menu">
								
								<td class="dateorder">Tên Sách </td>
								<td class="status">Giá</td>
								<td>Số Lượng</td>
								<td>Tổng Giá</td>
							</tr>
						</thead>
						<tbody>
						<c:forEach items="${orderdetail}" var="o" >
							<tr>
								<td class="cart_description">
									${o.book.name }
								</td>
								<td class="cart_description">
									${o.book.price }
								</td>
								
								<td class="cart_description">
									${o.quantity }
								</td>
								<td class="cart_description">
									${o.total_price }
								</td>
							</tr>
						</c:forEach>
						<tr>
							<th colspan="5" style="text-align: center;">Tam Tinh:${price } <small>VND</small></th>
							<th colspan="5" style="text-align: center;">VAT :${price*0.1 } <small>VND</small></th>
							<th colspan="5" style="text-align: center;">Ship:15000  <small>VND</small></th>
							<th colspan="5" style="text-align: center;">Tong Cong:${price+price*0.1+15000 } <small>VND</small></th>
						</tr>
						</tbody>
						
					</table>
					</div>
				</c:if>
			</div>
		</div>
			
	</section> <!--/#cart_items-->


	
	<jsp:include page="footer.jsp"></jsp:include>
	<script src="../resources/page/js/jquery.js"></script>
	<script src="../resources/page/js/price-range.js"></script>
    <script src="../resources/page/js/jquery.scrollUp.min.js"></script>
	<script src="../resources/page/js/bootstrap.min.js"></script>
    <script src="../resources/page/js/jquery.prettyPhoto.js"></script>
    <script src="../resources/page/js/main.js"></script>

</body>
</html>