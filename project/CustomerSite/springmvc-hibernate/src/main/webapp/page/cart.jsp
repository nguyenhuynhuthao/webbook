<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Giỏ hàng</title>
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="<c:url value="/resources/page/images/ico/favicon.ico"/>">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<c:url value="/resources/page/images/ico/apple-touch-icon-144-precomposed.png"/>">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<c:url value="/resources/page/images/ico/apple-touch-icon-114-precomposed.png"/>">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<c:url value="/resources/page/images/ico/apple-touch-icon-72-precomposed.png"/>">
    <link rel="apple-touch-icon-precomposed" href="<c:url value="/resources/page/images/ico/apple-touch-icon-57-precomposed.png"/>">
<link href="<c:url value="../resources/manager/css/style.css"/>" rel='stylesheet' type='text/css'/>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Bonfire Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!--fonts-->
<link href="<c:url value='http://fonts.googleapis.com/css?family=Exo:100,200,300,400,500,600,700,800,900'/>" rel='stylesheet' type='text/css'>
<!--//fonts-->
<link rel="stylesheet"
	href="<c:url value="/resources/page/css/bootstrap.min.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/page/css/font-awesome.min.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/page/css/prettyPhoto.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/page/css/price-range.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/page/css/animate.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/page/css/main.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/page/css/responsive.css" />">
	
	<script type="text/javascript" src="<c:url value="/resources/page/js/cart.js" />"></script>
</head>
<body>

	<jsp:include page="header.jsp"></jsp:include>
	
	<section id="cart_items">
		<div class="container">
			<div class="breadcrumbs">
				<ol class="breadcrumb">
				  <li><a href="index.jsp">Trang chủ</a></li>
				  <li class="active">Giỏ hàng</li>
				</ol>
			</div>
			<p style="color:red">${tb }</p>
			<div class="table-responsive cart_info">
				<table class="table table-condensed">
					<thead>
						<tr class="cart_menu">
							<td class="image">Tên Sản Phẩm</td>
							<td class="description"></td>
							<td class="price">Giá</td>
							<td class="quantity">Số lượng</td>
							<td class="total">Tổng</td>
							<td></td>
						</tr>
					</thead>
					<tbody>
					<c:choose>
						<c:when test="${!empty carts}">
							<c:forEach items="${carts}" var="c" >
								<tr>
									<td class="cart_product">
										<a href=""><img src="../resources/page/images/home/${c.book.imgage}" alt="" style="width: 110px;height: 110px"></a>
									</td>
									<td class="cart_description">
										<h4>${c.book.name }</a></h4>
										<p>ID sản phẩm:${c.book.id }</p>
									</td>
									<td class="cart_price">
										<p>${c.book.price }</p>
									</td>
									
									<td class="cart_quantity">
										<div class="cart_quantity_button">
											<a class="cart_quantity_up" href="../cartcontroller/addcart?id=${c.book.id}"> + </a>
											<input class="cart_quantity_input" type="text" name="quantity" value="${c.num}" autocomplete="off" size="2">
											<a class="cart_quantity_down" href="../cartcontroller/deleted/${c.book.id}"> - </a>
										</div>
									</td>
									<td class="cart_total" >
										<p class="cart_total_price">${c.book.price * c.num }</p>
									</td>
		
									<td class="cart_delete">
										<a class="cart_quantity_delete" href="../cartcontroller/removecart?id=${c.id}"><i class="fa fa-times"></i></a>
									</td>
								</tr>
							</c:forEach>
						</c:when>
						<c:otherwise>
							<tr>
								<th colspan="3" >Ban chua co san pham trong gio hang</th>
							</tr>
						</c:otherwise>
					</c:choose>
					</tbody>
				</table>
			</div>
		</div>
				</table>
			</div>
		</div>
	</section> <!--/#cart_items-->
	<c:if test="${!empty carts}">
		<section id="do_action">
			<div class="container">
					<div class="col-sm-6">
						<div class="total_area">
							<ul>
								<li>Tạm tính: <span>${price}</span></li>
								<li>Phí vận chuyển: <span>15000</span></li>
								<li>VAT 10%:  <span>${price*0.1 }</span></li>
								<li>Tổng cộng: <span>${price + 15000 +(price*0.1)}</span></li>
							</ul>
								<a class="btn btn-default update" href="../controller/genres">Chọn tiếp</a>
								<c:if test="${checkuser==0 }">
									<a class="btn btn-default check_out" href="../cartcontroller/vieworder" >Thanh toán</a>
								</c:if >
						
								<c:if test="${checkuser>0 }">
									
									<a class="btn btn-default check_out" href="../controller/userdet?id=${username.id } " onclick="chuyen();">Thanh toán</a>
									<script type="text/javascript">
										function chuyen() {
											alert("ban chua dien day du thong tin ");
										}
										</script>
								</c:if>
						</div>
					</div>
			</div>
		</section><!--/#do_action-->	
	</c:if>
	
	<jsp:include page="footer.jsp"></jsp:include>
	<script src="../resources/page/js/jquery.js"></script>
	<script src="../resources/page/js/price-range.js"></script>
    <script src="../resources/page/js/jquery.scrollUp.min.js"></script>
	<script src="../resources/page/js/bootstrap.min.js"></script>
    <script src="../resources/page/js/jquery.prettyPhoto.js"></script>
    <script src="../resources/page/js/main.js"></script>

</body>
</html>