<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>terms</title>
     <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="<c:url value="/resources/page/images/ico/favicon.ico"/>">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<c:url value="/resources/page/images/ico/apple-touch-icon-144-precomposed.png"/>">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<c:url value="/resources/page/images/ico/apple-touch-icon-114-precomposed.png"/>">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<c:url value="/resources/page/images/ico/apple-touch-icon-72-precomposed.png"/>">
    <link rel="apple-touch-icon-precomposed" href="<c:url value="/resources/page/images/ico/apple-touch-icon-57-precomposed.png"/>">
<link href="<c:url value="../resources/manager/css/style.css"/>" rel='stylesheet' type='text/css'/>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Bonfire Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!--fonts-->
<link href="<c:url value='http://fonts.googleapis.com/css?family=Exo:100,200,300,400,500,600,700,800,900'/>" rel='stylesheet' type='text/css'>
<!--//fonts-->
<link rel="stylesheet"
	href="<c:url value="/resources/page/css/bootstrap.min.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/page/css/font-awesome.min.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/page/css/prettyPhoto.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/page/css/price-range.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/page/css/animate.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/page/css/main.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/page/css/responsive.css" />">
</head>
<body>
	<jsp:include page="header.jsp"></jsp:include>

	<div class="container ">
		<div class="ty-wysiwyg-content" data-ca-live-editor-object-id="0"
			data-ca-live-editor-object-type="">
			<h1 align="center" dir="ltr" rel="line-height:2.1;margin-top:0pt;margin-bottom:0pt;"
				style="margin-top: 0pt; margin-bottom: 0pt; font-family: Arial, Helvetica, Verdana, Tahoma, sans-serif; line-height: 2.1;">Điều
				Khoản Sử Dụng</h1>
			<p dir="ltr"
				style="margin-top: 5pt; margin-bottom: 0pt; line-height: 1.38; color: rgb(51, 51, 51);">1.
				Giới thiệu</p>
			<p dir="ltr"
				style="margin-top: 5pt; margin-bottom: 0pt; line-height: 1.38; color: rgb(51, 51, 51);">Chào
				mừng quý khách hàng</p>
			<p dir="ltr"
				style="margin-top: 5pt; margin-bottom: 0pt; line-height: 1.38; color: rgb(51, 51, 51);">Khi
				quý khách hàng truy cập vào trang website của chúng tôi có nghĩa là
				quý khách đồng ý với các điều khoản này. Trang web có quyền thay
				đổi, chỉnh sửa, thêm hoặc lược bỏ bất kỳ phần nào trong Điều khoản
				mua bán hàng hóa này, vào bất cứ lúc nào. Các thay đổi có hiệu lực
				ngay khi được đăng trên trang web mà không cần thông báo trước. Và
				khi quý khách tiếp tục sử dụng trang web, sau khi các thay đổi về
				Điều khoản này được đăng tải, có nghĩa là quý khách chấp nhận với
				những thay đổi đó.</p>
			<p dir="ltr"
				style="margin-top: 5pt; margin-bottom: 0pt; line-height: 1.38; color: rgb(51, 51, 51);">Quý
				khách hàng vui lòng kiểm tra thường xuyên để cập nhật những thay đổi
				của chúng tôi.</p>
			<p dir="ltr"
				style="margin-top: 5pt; margin-bottom: 0pt; line-height: 1.38; color: rgb(51, 51, 51);">2.
				Hướng dẫn sử dụng website</p>
			<p dir="ltr"
				style="margin-top: 5pt; margin-bottom: 0pt; line-height: 1.38; color: rgb(51, 51, 51);">Khi
				vào web của chúng tôi, khách hàng phải đảm bảo đủ 18 tuổi, hoặc truy
				cập dưới sự giám sát của cha mẹ hay người giám hộ hợp pháp. Khách
				hàng đảm bảo có đầy đủ hành vi dân sự để thực hiện các giao dịch mua
				bán hàng hóa theo quy định hiện hành của pháp luật Việt Nam.</p>
			<p dir="ltr"
				style="margin-top: 5pt; margin-bottom: 0pt; line-height: 1.38; color: rgb(51, 51, 51);">Chúng
				tôi sẽ cấp một tài khoản (Account) sử dụng để khách hàng có thể mua
				sắm trên website trong khuôn khổ Điều khoản và Điều kiện
				sử dụng đã đề ra.</p>
			<p dir="ltr"
				style="margin-top: 5pt; margin-bottom: 0pt; line-height: 1.38; color: rgb(51, 51, 51);">Quý
				khách hàng sẽ phải đăng ký tài khoản với thông tin xác thực về bản
				thân và phải cập nhật nếu có bất kỳ thay đổi nào. Mỗi người truy cập
				phải có trách nhiệm với mật khẩu, tài khoản và hoạt động của mình
				trên web. Hơn nữa, quý khách hàng phải thông báo cho chúng tôi biết
				khi tài khoản bị truy cập trái phép. Chúng tôi không chịu bất kỳ
				trách nhiệm nào, dù trực tiếp hay gián tiếp, đối với những thiệt hại
				hoặc mất mát gây ra do quý khách không tuân thủ quy định.</p>
			<p dir="ltr"
				style="margin-top: 5pt; margin-bottom: 0pt; line-height: 1.38; color: rgb(51, 51, 51);">Nghiêm
				cấm sử dụng bất kỳ phần nào của trang web này với mục đích thương
				mại hoặc nhân danh bất kỳ đối tác thứ ba nào nếu không được chúng
				tôi cho phép bằng văn bản. Nếu vi phạm bất cứ điều nào trong đây,
				chúng tôi sẽ hủy tài khoản của khách mà không cần báo trước.</p>
			<p dir="ltr"
				style="margin-top: 5pt; margin-bottom: 0pt; line-height: 1.38; color: rgb(51, 51, 51);">Trong
				suốt quá trình đăng ký, quý khách đồng ý nhận email quảng cáo từ
				website. Nếu không muốn tiếp tục nhận mail, quý khách có thể từ chối
				bằng cách nhấp vào đường link ở dưới cùng trong mọi email quảng cáo.</p>
			<p dir="ltr"
				style="margin-top: 5pt; margin-bottom: 0pt; line-height: 1.38; color: rgb(51, 51, 51);">3.
				Ý kiến của khách hàng</p>
			<p dir="ltr"
				style="margin-top: 5pt; margin-bottom: 0pt; line-height: 1.38; color: rgb(51, 51, 51);">Tất
				cả nội dung trang web và ý kiến phê bình của quý khách đều là tài
				sản của chúng tôi. Nếu chúng tôi phát hiện bất kỳ thông tin giả mạo
				nào, chúng tôi sẽ khóa tài khoản của quý khách ngay lập tức hoặc áp
				dụng các biện pháp khác theo quy định của pháp luật Việt Nam.</p>
			<p dir="ltr"
				style="margin-top: 5pt; margin-bottom: 0pt; line-height: 1.38; color: rgb(51, 51, 51);">4.
				Chấp nhận đơn hàng và giá cả</p>
			<p dir="ltr"
				style="margin-top: 5pt; margin-bottom: 0pt; line-height: 1.38; color: rgb(51, 51, 51);">Chúng
				tôi có quyền từ chối hoặc hủy đơn hàng của quý khách vì bất kỳ lý do
				gì liên quan đến lỗi kỹ thuật, hệ thống một cách khách quan vào bất
				kỳ lúc nào.</p>
			<p dir="ltr"
				style="margin-top: 5pt; margin-bottom: 0pt; line-height: 1.38; color: rgb(51, 51, 51);">Ngoài
				ra, để đảm bảo tính công bằng cho khách hàng là người tiêu dùng cuối
				cùng của nhanvan.vn, chúng tôi cũng sẽ từ chối các đơn hàng không
				nhằm mục đích sử dụng cho cá nhân, mua hàng số lượng nhiều hoặc với
				mục đích mua đi bán lại.</p>
			<p dir="ltr"
				style="margin-top: 5pt; margin-bottom: 0pt; line-height: 1.38; color: rgb(51, 51, 51);">Chúng
				tôi cam kết sẽ cung cấp thông tin giá cả chính xác nhất cho người
				tiêu dùng. Tuy nhiên, đôi lúc vẫn có sai sót xảy ra, ví dụ như
				trường hợp giá sản phẩm không hiển thị chính xác trên trang web hoặc
				sai giá, tùy theo từng trường hợp chúng tôi sẽ liên hệ hướng dẫn
				hoặc thông báo hủy đơn hàng đó cho quý khách. Chúng tôi cũng có
				quyền từ chối hoặc hủy bỏ bất kỳ đơn hàng nào dù đơn hàng đó đã hay
				chưa được xác nhận hoặc đã thanh toán.</p>
			<p dir="ltr"
				style="margin-top: 5pt; margin-bottom: 0pt; line-height: 1.38; color: rgb(51, 51, 51);">5.
				Thay đổi hoặc hủy bỏ giao dịch tại website</p>
			<p dir="ltr"
				style="margin-top: 5pt; margin-bottom: 0pt; line-height: 1.38; color: rgb(51, 51, 51);">Trong
				mọi trường hợp, khách hàng đều có quyền chấm dứt giao dịch nếu đã
				thực hiện các biện pháp sau đây:</p>
			<ul>
				<li dir="ltr">Thông báo cho chúng tôi về việc hủy giao dịch
					qua đường dây nóng (hotline) 0703132898</li>
				<li dir="ltr">Trả lại hàng hoá đã nhận nhưng chưa sử dụng hoặc
					hưởng bất kỳ lợi ích nào từ hàng hóa đó</li>
			</ul>
			<p dir="ltr"
				style="margin-top: 5pt; margin-bottom: 0pt; line-height: 1.38; color: rgb(51, 51, 51);">6.
				Giải quyết hậu quả do lỗi nhập sai thông tin tại website</p>
			<p dir="ltr"
				style="margin-top: 5pt; margin-bottom: 0pt; line-height: 1.38; color: rgb(51, 51, 51);">Khách
				hàng có trách nhiệm cung cấp thông tin đầy đủ và chính xác khi tham
				gia giao dịch tại website. Trong trường hợp khách hàng nhập sai
				thông tin và gửi vào trang TMĐT của chúng tôi, chúng tôi có quyền từ
				chối thực hiện giao dịch. Ngoài ra, trong mọi trường hợp, khách hàng
				đều có quyền đơn phương chấm dứt giao dịch nếu đã thực hiện các biện
				pháp sau đây:</p>
			<ul>
				<li dir="ltr">Thông báo cho chúng tôi qua đường dây nóng
					0703132898</li>
				<li dir="ltr">Trả lại hàng hoá đã nhận nhưng chưa sử dụng hoặc
					hưởng bất kỳ lợi ích nào từ hàng hóa đó.</li>
			</ul>
			<p dir="ltr"
				style="margin-top: 5pt; margin-bottom: 0pt; line-height: 1.38; color: rgb(51, 51, 51);">Trong
				trường hợp sai thông tin phát sinh từ phía chúng tôi mà chúng tôi
				có thể chứng minh đó là lỗi của hệ thống hoặc từ bên thứ ba (sai giá
				sản phẩm, sai xuất xứ, …), chúng tôi sẽ đền bù cho khách hàng một
				mã giảm giá cho các lần mua sắm tiếp theo với mệnh giá tùy từng
				trường hợp cụ thể và có quyền không thực hiện giao dịch bị lỗi.</p>
			<p dir="ltr"
				style="margin-top: 5pt; margin-bottom: 0pt; line-height: 1.38; color: rgb(51, 51, 51);">7.
				Thương hiệu và bản quyền</p>
			<p dir="ltr"
				style="margin-top: 5pt; margin-bottom: 0pt; line-height: 1.38; color: rgb(51, 51, 51);">Mọi
				quyền sở hữu trí tuệ (đã đăng ký hoặc chưa đăng ký), nội dung thông
				tin và tất cả các thiết kế, văn bản, đồ họa, phần mềm, hình ảnh,
				video, âm nhạc, âm thanh, biên dịch phần mềm, mã nguồn và phần mềm
				cơ bản đều là tài sản của chúng tôi. Toàn bộ nội dung của trang web
				được bảo vệ bởi luật bản quyền của Việt Nam và các công ước quốc tế.
				Bản quyền đã được bảo lưu.</p>
			<p dir="ltr"
				style="margin-top: 5pt; margin-bottom: 0pt; line-height: 1.38; color: rgb(51, 51, 51);">8.
				Quyền pháp lý</p>
			<p dir="ltr"
				style="margin-top: 5pt; margin-bottom: 0pt; line-height: 1.38; color: rgb(51, 51, 51);">Các
				điều kiện, điều khoản và nội dung của trang web này được điều chỉnh
				bởi luật pháp Việt Nam và Tòa án có thẩm quyền tại Việt Nam sẽ giải
				quyết bất kỳ tranh chấp nào phát sinh từ việc sử dụng trái phép
				trang web này.</p>
			<p dir="ltr"
				style="margin-top: 5pt; margin-bottom: 0pt; line-height: 1.38; color: rgb(51, 51, 51);">9.
				Quy định về bảo mật</p>
			<p dir="ltr"
				style="margin-top: 5pt; margin-bottom: 0pt; line-height: 1.38; color: rgb(51, 51, 51);">Trang
				web của chúng tôi coi trọng việc bảo mật thông tin và sử dụng các
				biện pháp tốt nhất bảo vệ thông tin và việc thanh toán của quý
				khách. Thông tin của quý khách trong quá trình thanh toán sẽ được mã
				hóa để đảm bảo an toàn. Sau khi quý khách hoàn thành quá trình đặt
				hàng, quý khách sẽ thoát khỏi chế độ an toàn.</p>
			<p dir="ltr"
				style="margin-top: 5pt; margin-bottom: 0pt; line-height: 1.38; color: rgb(51, 51, 51);">Quý
				khách không được sử dụng bất kỳ chương trình, công cụ hay hình thức
				nào khác để can thiệp vào hệ thống hay làm thay đổi cấu trúc dữ
				liệu. Trang web cũng nghiêm cấm việc phát tán, truyền bá hay cổ vũ
				cho bất kỳ hoạt động nào nhằm can thiệp, phá hoại hay xâm nhập vào
				dữ liệu của hệ thống. Cá nhân hay tổ chức vi phạm sẽ bị tước bỏ mọi
				quyền lợi cũng như sẽ bị truy tố trước pháp luật nếu cần thiết.</p>
			<p dir="ltr"
				style="margin-top: 5pt; margin-bottom: 0pt; line-height: 1.38; color: rgb(51, 51, 51);">Mọi
				thông tin giao dịch sẽ được bảo mật ngoại trừ trong trường hợp cơ
				quan pháp luật yêu cầu.</p>
			<p dir="ltr"
				style="margin-top: 5pt; margin-bottom: 0pt; line-height: 1.38; color: rgb(51, 51, 51);">10.
				Thanh toán an toàn và tiện lợi</p>
			<p dir="ltr"
				style="margin-top: 5pt; margin-bottom: 0pt; line-height: 1.38; color: rgb(51, 51, 51);">Người
				mua có thể tham khảo các phương thức thanh toán sau đây và lựa chọn
				áp dụng phương thức phù hợp:</p>
			<p dir="ltr"
				style="margin-top: 5pt; margin-bottom: 0pt; line-height: 1.38; color: rgb(51, 51, 51);">Cách
				1: Thanh toán trực tiếp (người mua nhận hàng tại địa chỉ người bán):</p>
			<p dir="ltr"
				style="margin-top: 5pt; margin-bottom: 0pt; line-height: 1.38; color: rgb(51, 51, 51);">Bước
				1: Người mua tìm hiểu thông tin về sản phẩm, dịch vụ được đăng tin;</p>
			<p dir="ltr"
				style="margin-top: 5pt; margin-bottom: 0pt; line-height: 1.38; color: rgb(51, 51, 51);">Bước
				2: Người mua đến địa chỉ bán hàng</p>
			<p dir="ltr"
				style="margin-top: 5pt; margin-bottom: 0pt; line-height: 1.38; color: rgb(51, 51, 51);">Bước
				3: Người mua thanh toán và nhận hàng;</p>
			<p dir="ltr"
				style="margin-top: 5pt; margin-bottom: 0pt; line-height: 1.38; color: rgb(51, 51, 51);">Cách
				2: Thanh toán sau (COD – giao hàng và thu tiền tận nơi):</p>
			<p dir="ltr"
				style="margin-top: 5pt; margin-bottom: 0pt; line-height: 1.38; color: rgb(51, 51, 51);">Bước
				1: Người mua tìm hiểu thông tin về sản phẩm, dịch vụ được đăng tin;</p>
			<p dir="ltr"
				style="margin-top: 5pt; margin-bottom: 0pt; line-height: 1.38; color: rgb(51, 51, 51);">Bước
				2: Người mua xác thực đơn hàng (điện thoại, tin nhắn, email);</p>
			<p dir="ltr"
				style="margin-top: 5pt; margin-bottom: 0pt; line-height: 1.38; color: rgb(51, 51, 51);">Bước
				3: Người bán xác nhận thông tin Người mua;</p>
			<p dir="ltr"
				style="margin-top: 5pt; margin-bottom: 0pt; line-height: 1.38; color: rgb(51, 51, 51);">Bước
				4: Người bán chuyển hàng;</p>
			<p dir="ltr"
				style="margin-top: 5pt; margin-bottom: 0pt; line-height: 1.38; color: rgb(51, 51, 51);">Bước
				5: Người mua nhận hàng và thanh toán.</p>
			<p dir="ltr"
				style="margin-top: 5pt; margin-bottom: 0pt; line-height: 1.38; color: rgb(51, 51, 51);">Cách
				3: Thanh toán online qua thẻ tín dụng, chuyển khoản</p>
			<p dir="ltr"
				style="margin-top: 5pt; margin-bottom: 0pt; line-height: 1.38; color: rgb(51, 51, 51);">Bước
				1: Người mua tìm hiểu thông tin về sản phẩm, dịch vụ được đăng tin;</p>
			<p dir="ltr"
				style="margin-top: 5pt; margin-bottom: 0pt; line-height: 1.38; color: rgb(51, 51, 51);">Bước
				2: Người mua xác thực đơn hàng (điện thoại, tin nhắn, email);</p>
			<p dir="ltr"
				style="margin-top: 5pt; margin-bottom: 0pt; line-height: 1.38; color: rgb(51, 51, 51);">Bước
				3: Người bán xác nhận thông tin Người mua;</p>
			<p dir="ltr"
				style="margin-top: 5pt; margin-bottom: 0pt; line-height: 1.38; color: rgb(51, 51, 51);">Bước
				4: Ngườii mua thanh toán;</p>
			<p dir="ltr"
				style="margin-top: 5pt; margin-bottom: 0pt; line-height: 1.38; color: rgb(51, 51, 51);">Bước
				5: Người bán chuyển hàng;</p>
			<p dir="ltr"
				style="margin-top: 5pt; margin-bottom: 0pt; line-height: 1.38; color: rgb(51, 51, 51);">Bước
				6: Người mua nhận hàng.</p>
			<p dir="ltr"
				style="margin-top: 5pt; margin-bottom: 0pt; line-height: 1.38; color: rgb(51, 51, 51);">Đối
				với người mua hàng từ nhanvan.vn thì phải tuẩn thu theo chính sách
				thanh toán của công ty.</p>
			<p dir="ltr"
				style="margin-top: 5pt; margin-bottom: 0pt; line-height: 1.38; color: rgb(51, 51, 51);">11.
				Đảm bảo an toàn giao dịch tại nhanvan.vn</p>
			<p dir="ltr"
				style="margin-top: 5pt; margin-bottom: 0pt; line-height: 1.38; color: rgb(51, 51, 51);">Chúng
				tôi sử dụng các dịch vụ để bảo vệ thông tin về nội dung mà người bán
				đăng sản phẩm trên website. Để đảm bảo các giao dịch được tiến
				hành thành công, hạn chế tối đa rủi ro có thể phát sinh.</p>
			<p dir="ltr"
				style="margin-top: 5pt; margin-bottom: 0pt; line-height: 1.38; color: rgb(51, 51, 51);">12.
				Luật pháp và thẩm quyền tại Lãnh thổ Việt Nam</p>
			<p>Tất cả các Điều Khoản và Điều Kiện này và Hợp Đồng (và tất cả
				nghĩa vụ phát sinh ngoài hợp đồng hoặc có liên quan) sẽ bị chi phối
				và được hiểu theo luật pháp của Việt Nam. Nếu có tranh chấp phát
				sinh bởi các Quy định Sử dụng này, quý khách hàng có quyền gửi khiếu
				nại/khiếu kiện lên Tòa án có thẩm quyền tại Việt Nam để giải quyết.</p>
		</div>
	</div>
	
	<jsp:include page="footer.jsp"></jsp:include>
	 <script src="../resources/page/js/jquery.js"></script>
	<script src="../resources/page/js/price-range.js"></script>
    <script src="../resources/page/js/jquery.scrollUp.min.js"></script>
	<script src="../resources/page/js/bootstrap.min.js"></script>
    <script src="../resources/page/js/jquery.prettyPhoto.js"></script>
    <script src="../resources/page/js/main.js"></script>
</body>
</html>