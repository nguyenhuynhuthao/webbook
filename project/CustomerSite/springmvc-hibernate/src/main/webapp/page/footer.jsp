<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Footer</title>
</head>
<body>

	<footer id="footer"><!--Footer-->
		<div class="footer-top">
			<div class="container">
				<div class="row">
					<div class="col-sm-2">
						<div class="companyinfo">
							<h2><span>Nhóm</span>2</h2>
						</div>
					</div>
					<div class="col-sm-7">
						<div class="col-sm-3">
							<div class="video-gallery text-center">
								<p>Trần Đức Lâm</p>
							</div>
						</div>
						
						<div class="col-sm-3">
							<div class="video-gallery text-center">
								<p>Đỗ Tấn Tường</p>
							</div>
						</div>
						
						<div class="col-sm-3">
							<div class="video-gallery text-center">
								<p>Nguyễn Huỳnh Như Thảo</p>
							</div>
						</div>
						
						<div class="col-sm-3">
							<div class="video-gallery text-center">
								<p>Vân Anh</p>
							</div>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="address">
							<img src="../resources/page/images/home/map.png" alt="" />
							<p>Project</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="footer-widget">
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<div class="single-widget">
							<h2>TÀI KHOẢN</h2>
							<ul class="nav nav-pills nav-stacked">
								<li><a href="#"></a></li>
								<c:if test="${mode == null ||mode == 'LOGOUT'}">
								<li><a href="../controller/logins">Đăng nhập</a></li>
								</c:if>
								<li><a href="<c:url value="/page/cart.jsp"/>">Đơn hàng</a></li>
								<li><a href="#">Yêu thích</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="single-widget">
							<h2>Hỗ trợ khách hàng</h2>
							<ul class="nav nav-pills nav-stacked">
								<li><a href="<c:url value="/page/FAQs.jsp"/>">Chính sách đổi trả</a></li>
								<li><a href="<c:url value="/page/privacypolicy.jsp"/>">Chính sách bảo mật</a></li>
								<li><a href="<c:url value="/page/terms.jsp"/>">Điều khoản sử dụng</a></li>
								<li><a href="<c:url value="/page/members.jsp"/>">Chương trình thành viên</a></li>
								<li><a href="<c:url value="/page/customers.jsp"/>">Chinh sách khách sỉ</a></li>
								<li><a href="<c:url value="/page/pay.jsp"/>">Giao hàng và thanh toán</a></li>
							</ul>
						</div>
					</div>
			
				</div>
			</div>
		</div>
		
		<div class="footer-bottom">
			<div class="container">
				<div class="row">
					<p class="pull-left">© 2019 Bản quyền thuộc NHÓM 2.</p>
					<p class="pull-right">Thiết kế bởi <span>Nhóm 2 </span></p>
				</div>
			</div>
		</div>
		
	</footer><!--/Footer-->

</body>
</html>