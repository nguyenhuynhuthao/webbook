<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="<c:url value="/resources/page/images/ico/favicon.ico"/>">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<c:url value="/resources/page/images/ico/apple-touch-icon-144-precomposed.png"/>">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<c:url value="/resources/page/images/ico/apple-touch-icon-114-precomposed.png"/>">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<c:url value="/resources/page/images/ico/apple-touch-icon-72-precomposed.png"/>">
    <link rel="apple-touch-icon-precomposed" href="<c:url value="/resources/page/images/ico/apple-touch-icon-57-precomposed.png"/>">
<link href="<c:url value="../resources/manager/css/style.css"/>" rel='stylesheet' type='text/css'/>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Bonfire Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!--fonts-->
<link href="<c:url value='http://fonts.googleapis.com/css?family=Exo:100,200,300,400,500,600,700,800,900'/>" rel='stylesheet' type='text/css'>
<!--//fonts-->
<link rel="stylesheet"
	href="<c:url value="/resources/page/css/bootstrap.min.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/page/css/font-awesome.min.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/page/css/prettyPhoto.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/page/css/price-range.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/page/css/animate.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/page/css/main.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/page/css/responsive.css" />">
</head>
<body>
	<jsp:include page="header.jsp"></jsp:include>

	<jsp:include page="banner.jsp"></jsp:include>
	

	<jsp:include page="footer.jsp"></jsp:include>
</body>
</html>