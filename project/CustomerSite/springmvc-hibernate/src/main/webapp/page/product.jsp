<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>product</title>
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="<c:url value="/resources/page/images/ico/favicon.ico"/>">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<c:url value="/resources/page/images/ico/apple-touch-icon-144-precomposed.png"/>">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<c:url value="/resources/page/images/ico/apple-touch-icon-114-precomposed.png"/>">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<c:url value="/resources/page/images/ico/apple-touch-icon-72-precomposed.png"/>">
    <link rel="apple-touch-icon-precomposed" href="<c:url value="/resources/page/images/ico/apple-touch-icon-57-precomposed.png"/>">
<link href="<c:url value="../resources/manager/css/style.css"/>" rel='stylesheet' type='text/css'/>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Bonfire Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!--fonts-->
<link href="<c:url value='http://fonts.googleapis.com/css?family=Exo:100,200,300,400,500,600,700,800,900'/>" rel='stylesheet' type='text/css'>
<!--//fonts-->
<link rel="stylesheet"
	href="<c:url value="/resources/page/css/bootstrap.min.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/page/css/font-awesome.min.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/page/css/prettyPhoto.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/page/css/price-range.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/page/css/animate.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/page/css/main.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/page/css/responsive.css" />">
</head>
<body>
	<jsp:include page="header.jsp"></jsp:include>
	
	<section id="advertisement">
		<div class="container">
			<img src="../resources/page/images/shop/advertisement.jpg" />
		</div>
	</section>
	
	<section>
		<div class="container">
			<div class="row">
				<div class="col-sm-3">
					<div class="left-sidebar">
						<h2>Thể loại</h2>
						<div class="panel-group category-products" id="accordian"><!--category-productsr-->
								<div class="panel panel-default">
										<div class="panel-heading">
											<h4 class="panel-title"><a href="../controller/genres">Tất cả sản phẩm</a></h4>
										</div>
								</div>
						</div>
						<c:forEach items="${genre}" var="genre">
							<div class="panel-group category-products" id="accordian"><!--category-productsr-->
								<div class="panel panel-default">
										<div class="panel-heading">
											<h4 class="panel-title"><a href="./getgenres?id=${genre.id }">${genre.description}</a></h4>
										</div>
								</div>
							</div>
						</c:forEach><!--/category-productsr-->
						<div class="price-range"><!--price-range-->
							<h2>Phạm vi giá</h2>
							<div class="well">
								 <input type="text" class="span2" value="" data-slider-min="0" data-slider-max="100000" data-slider-step="500" data-slider-value="[50000,100000]" id="sl2" ><br />
								 <b>0</b> <b class="pull-right">100000</b>
							</div>
						</div><!--/price-range-->
						
						<div class="shipping text-center"><!--shipping-->
							<img src="../resources/page/images/home/shipping.jpg" alt="" />
						</div><!--/shipping-->
						
					</div>
				</div>

				<div class="col-sm-9 padding-right" >
					<div class="features_items"><!--features_items-->
						<h2 class="title text-center">Sản phẩm</h2>
							<c:forEach items="${book}" var="book">
								<div class="col-sm-4">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center" style="width: 210px;height: 350px">
												<a href="../controller/product?id=${book.id }">
												<img src="../resources/page/images/home/${book.imgage}" alt="" style="width: 207px;height: 200px"/>
												<h2>${book.price }</h2>
												<p>${book.name }</p>
												</a>
													<c:if test="${mode == 'LOGIN' }">
														<a type="button" href="../cartcontroller/addcart?id=${i.id}" class="btn btn-default add-to-cart"><i
															class="fa fa-shopping-cart"></i>Add to cart</a>
													</c:if>
													<c:if test="${mode == null}">
														<a type="button" href="../controller/logins" class="btn btn-default add-to-cart"><i
															class="fa fa-shopping-cart"></i>Add to cart</a>
													</c:if>											</div>
											<div class="product-overlay">
												<div class="overlay-content">
													<a href="../controller/product?id=${book.id }">
													<h2>${book.price }</h2>
													<p>${book.name }</p>
													</a>
													<c:if test="${mode == 'LOGIN' }">
														<a type="button" href="../cartcontroller/addcart?id=${i.id}" class="btn btn-default add-to-cart"><i
															class="fa fa-shopping-cart"></i>Add to cart</a>
													</c:if>
													<c:if test="${mode == null}">
														<a type="button" href="../controller/logins" class="btn btn-default add-to-cart"><i
															class="fa fa-shopping-cart"></i>Add to cart</a>
													</c:if>
												</div>
											</div>
										</div>
										
									</div>
								</div>
							</c:forEach>
						</div>
					</div>
						<ul class="pagination">
							<c:if test="${page>0 }">
								<c:if test="${page>1}">
									<li><a href="./genre?page=${page-1}">	&lt;</a></li>
								</c:if>
								<c:if test="${page<slpage}">
								<li><a href="./genre?page=${page+1}">	&gt;</a></li>
								</c:if>
							</c:if>
							<c:if test="${pagegenre>0 }">
								<c:if test="${pagegenre>1}">
									<li><a href="./getgenre?id=${idgenre }&pagegenre=${pagegenre-1}">	&lt;</a></li>
								</c:if>
								<c:if test="${pagegenre<slpagegenre}">
								<li><a href="./getgenre?id=${idgenre }&pagegenre=${pagegenre+1}">	&gt;</a></li>
								</c:if>
							</c:if>
						</ul>
					</div><!--features_items-->
				</div>
			</div>
		</div>
	</section>
	<jsp:include page="footer.jsp"></jsp:include>
	    <script src="../resources/page/js/jquery.js"></script>
	<script src="../resources/page/js/price-range.js"></script>
    <script src="../resources/page/js/jquery.scrollUp.min.js"></script>
	<script src="../resources/page/js/bootstrap.min.js"></script>
    <script src="../resources/page/js/jquery.prettyPhoto.js"></script>
    <script src="../resources/page/js/main.js"></script>
</body>
</html>