<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Thông tin tài khoản</title>
	<link href="<c:url value="/resources/page/css/bootstrap.min.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/page/css/font-awesome.min.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/page/css/prettyPhoto.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/page/css/price-range.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/page/css/animate.css"/>" rel="stylesheet">
	<link href="<c:url value="/resources/page/css/main.css"/>" rel="stylesheet">
	<link href="<c:url value="/resources/page/css/responsive.css"/>" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<c:url value="/resources/page/images/ico/apple-touch-icon-144-precomposed.png"/>">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<c:url value="/resources/page/images/ico/apple-touch-icon-114-precomposed.png"/>">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<c:url value="/resources/page/images/ico/apple-touch-icon-72-precomposed.png"/>">
    <link rel="apple-touch-icon-precomposed" href="<c:url value="/resources/page/images/ico/apple-touch-icon-57-precomposed.png"/>">
    <script type="text/javascript" src="<c:url value="/resources/page/js/jquery.js"/>"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" type="text/javascript"/>"></script>
    <link href="<c:url value="../resources/manager/css/style.css"/>" rel='stylesheet' type='text/css'/>
</head>
<body>
	<jsp:include page="header.jsp"></jsp:include>
	
	<div id="cart_items"><!--form-->
		<div class="container">
				<div class="container">
			<div class="breadcrumbs">
				<ol class="breadcrumb">
				  <li><a href="#">Trang chủ</a></li>
				  <li class="active">Thông tin cá nhân</li>
				</ol>
			</div><!--/breadcrums-->

			<div class="shopper-informations">
				<div class="row">
					<div class="col-sm-8">
						<div class="shopper-info">
							<p>Thông tin khách hàng</p>
							<form>
								<input type="text" placeholder="Họ và tên"  />
								<input type="date" placeholder="Ngày sinh"  />
								<input type="text" placeholder="Số điện thoại" />
								<input type="text" placeholder="Địa chỉ" />
								<button type="submit" class="btn btn-default" >Lưu thông tin</button>
							</form>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="order-message">
							<p>Ghi chú cho shipper</p>
							<textarea name="message"  placeholder="Ghi chú về đơn hàng" rows="16"></textarea>
							<label><input type="checkbox"> Xuất hoá đơn đỏ về địa chỉ email(thuế khách hàng chịu)</label>
						</div>	
					</div>					
				</div>
			</div>
		</div>
	</div><!--/form-->
	
	<jsp:include page="footer.jsp"></jsp:include>
</body>
</html>