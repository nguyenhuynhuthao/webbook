<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>pay</title>
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="<c:url value="/resources/page/images/ico/favicon.ico"/>">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<c:url value="/resources/page/images/ico/apple-touch-icon-144-precomposed.png"/>">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<c:url value="/resources/page/images/ico/apple-touch-icon-114-precomposed.png"/>">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<c:url value="/resources/page/images/ico/apple-touch-icon-72-precomposed.png"/>">
    <link rel="apple-touch-icon-precomposed" href="<c:url value="/resources/page/images/ico/apple-touch-icon-57-precomposed.png"/>">
<link href="<c:url value="../resources/manager/css/style.css"/>" rel='stylesheet' type='text/css'/>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Bonfire Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!--fonts-->
<link href="<c:url value='http://fonts.googleapis.com/css?family=Exo:100,200,300,400,500,600,700,800,900'/>" rel='stylesheet' type='text/css'>
<!--//fonts-->
<link rel="stylesheet"
	href="<c:url value="/resources/page/css/bootstrap.min.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/page/css/font-awesome.min.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/page/css/prettyPhoto.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/page/css/price-range.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/page/css/animate.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/page/css/main.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/page/css/responsive.css" />">
</head>
<body>
	<jsp:include page="header.jsp"></jsp:include>

	<div class="tygh-content clearfix">
		<div class="container ">



			<div class="row">
				<div class="span16 ">
					<div class="ty-wysiwyg-content" data-ca-live-editor-object-id="0"
						data-ca-live-editor-object-type="">
						<h2 dir="ltr"
							rel="line-height:1.56;margin-top:0pt;margin-bottom:22pt;"
							style="margin-top: 0pt; margin-bottom: 22pt; font-family: Arial, Helvetica, Verdana, Tahoma, sans-serif; line-height: 1.56;">PHƯƠNG
							THỨC VẬN CHUYỂN</h2>
						<h3 dir="ltr"
							style="margin-top: 0pt; margin-bottom: 6pt; font-family: Arial, Helvetica, Verdana, Tahoma, sans-serif; line-height: 1.56;">Phí
							vận chuyển</h3>
						<p dir="ltr"
							style="margin-top: 0pt; margin-bottom: 8pt; line-height: 1.92; color: rgb(51, 51, 51); text-align: justify;">Chúng tôi
							luôn cố gắng mang lại trải nghiệm mua hàng thật tốt cho khách
							hàng.</p>
						<p dir="ltr"
							style="margin-top: 0pt; margin-bottom: 8pt; line-height: 1.92; color: rgb(51, 51, 51); text-align: justify;">-
							Với đa phần đơn hàng, Chúng tôi sẽ nhanh chóng chuyển cho đơn vị
							vận chuyển giao hàng trong thời gian tối đa 9 ngày làm việc. Nếu
							đơn hàng có sản phẩm bị hết hàng, Chúng tôi ưu tiên giao phần có
							hàng trước.</p>
						<p dir="ltr"
							style="margin-top: 0pt; margin-bottom: 8pt; line-height: 1.92; color: rgb(51, 51, 51); text-align: justify;">-
							Trong một số trường hợp hàng nằm ở nhà sách xa, thời gian giao
							hàng có thể lâu hơn. Tuy nhiên phí vận chuyển phát sinh
							chúng tôi sẽ hỗ trợ hoàn toàn. Chúng tôi mong quý khách thông
							cảm tình huống này.</p>
						<p style="color: rgb(51, 51, 51); text-align: center;">
							<img
								src="https://nhanvan.vn/images/banner/ship_3456.jpg?1532491377617">
						</p>
						<h3 dir="ltr"
							style="margin-top: 0pt; margin-bottom: 6pt; font-family: Arial, Helvetica, Verdana, Tahoma, sans-serif; line-height: 1.56;">
							<br>
						</h3>
						<h2 dir="ltr"
							style="margin-top: 0pt; margin-bottom: 22pt; font-family: Arial, Helvetica, Verdana, Tahoma, sans-serif; line-height: 1.56;">PHƯƠNG
							THỨC THANH TOÁN</h2>
						<h3 dir="ltr"
							style="margin-top: 0pt; margin-bottom: 6pt; font-family: Arial, Helvetica, Verdana, Tahoma, sans-serif; line-height: 1.56;">
							A. THANH TOÁN BẰNG TIỀN MẶT KHI NHẬN HÀNG (COD)<br>
						</h3>
						<h4 dir="ltr"
							style="font-family: Arial, Helvetica, Verdana, Tahoma, sans-serif;">
							<br>
						</h4>
						<h3 dir="ltr"
							style="margin-top: 0pt; margin-bottom: 6pt; font-family: Arial, Helvetica, Verdana, Tahoma, sans-serif; line-height: 1.56;">B.
							THANH TOÁN BẰNG CHUYỂN KHOẢN NGÂN HÀNG</h3>
					</div>
				</div>
			</div>
		</div>
	</div>

	<jsp:include page="footer.jsp"></jsp:include>
	 <script src="../resources/page/js/jquery.js"></script>
	<script src="../resources/page/js/price-range.js"></script>
    <script src="../resources/page/js/jquery.scrollUp.min.js"></script>
	<script src="../resources/page/js/bootstrap.min.js"></script>
    <script src="../resources/page/js/jquery.prettyPhoto.js"></script>
    <script src="../resources/page/js/main.js"></script>
</body>
</html>