<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" data-ca-mode="full" />
<title>header</title>
</head>
<body>

	<header id="header"><!--header-->
		<div class="header_top"><!--header_top-->
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<div class="contactinfo">
							<ul class="nav nav-pills">
								<li><a href="#"><i class="fa fa-phone"></i> 0703132898 </a></li>
								<li><a href="#"><i class="fa fa-envelope"></i> timmytran202@gmail.com</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="social-icons pull-right">
							<ul class="nav navbar-nav">
								<li><a href="#"><i class="fa fa-facebook"></i></a></li>
								<li><a href="#"><i class="fa fa-instagram"></i></a></li>
								<li><a href="#"><i class="fa fa-skype"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div><!--/header_top-->
		
		<div class="header-middle"><!--header-middle-->
			<div class="container">
				<div class="row">
					<div class="col-sm-4">
						<div class="pull-left">
							<a href="../controller/index"><img src="../resources/page/images/home/logo.png" alt="" /></a>
						</div>
						
					</div>
					<c:if test="${mode == null }">
					<div class="col-sm-8">
						<div class="shop-menu pull-right">
							<ul class="nav navbar-nav">
								<div class="btn-group">
									<li><a href="<c:url value="/page/cart.jsp"/>"><i class="fa fa-shopping-cart"></i> Giỏ hàng</a></li>
									<li><a href="../controller/logins"><i class="fa fa-lock"></i> Đăng nhập</a></li>
								</div>
							</ul>
						</div>
					</div>
					</c:if>
					<c:if test="${mode == 'LOGIN' }">
						<div class="btn-group pull-right">
						<div class="profile_details">		
                    		<ul>
                        		<li class="dropdown profile_details_drop">
                            		<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
										<div class="profile_details">	
											
							                    <ul>
							                        <li class="dropdown profile_details_drop">
							                        	
							                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
							                                <div class="profile_img">	
							                                    <span class="prfil-img"><img src="../resources/page/images/home/avatar.jpg" style="width: 50px;height: 50px;" alt=""> </span> 
							                                    <div class="user-name">
							                                    
							                                        <p>${sessionScope.username.name }</p>
							                                    
							                                    <c:if test="${mode == null || mode == 'YES' }">
							                                        <p>${sessionScope.fullname }</p> 
							                                    </c:if>
							                                        <span>Thành viên</span>
							                                    </div>
							                                    <i class="fa fa-angle-down lnr"></i>
							                                    <i class="fa fa-angle-up lnr"></i>
							                                    <div class="clearfix"></div>	
							                                </div>	
							                            </a>
							                            <ul class="dropdown-menu drp-mnu">
							                                <li> <a href="../controller/userdet?id=${username.id }"><i class="fa fa-user"></i> Hồ sơ<c:if test="${checkuser>0 }"><span class="badge">${checkuser }</span></c:if></a> </li> 
							                                <li><a href="../cartcontroller/orders"><i class="fas fa-shopping-basket	"></i> Đơn Hàng</a>
							                                <li> <a href="../cartcontroller/cart"><i class="fa fa-shopping-cart"></i> Giỏ hàng</a> </li> 
							                                <li> <a href="<c:url value="../controller/logout"/>"><i class="fa fa-sign-out"></i> Đăng xuất</a> </li>
							                            </ul>
							                        </li>
							                    </ul>
			                			</div>
			                		</a>
			                	</li>
			                </ul>
			                </div>
						</div>
					</c:if>
				</div>
			</div>
		</div><!--/header-middle-->
	
		<div class="header-bottom"><!--header-bottom-->
			<div class="container">
				<div class="row">
					<div class="col-sm-9">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
						</div>
						<div class="mainmenu pull-left">
							<ul class="nav navbar-nav collapse navbar-collapse">
								<li><a href="../controller/index" class="active">Trang chủ</a></li>
								<li class="dropdown"><a href="../controller/genres">Sản phẩm</a></li>
								<li><a href="<c:url value="/page/404.jsp"/>">Tuyển Dụng</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="search_box pull-right">
							<form action="../controller/search">
								<input name="search" id="search">
								<button class="btn btn-info">search</button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div><!--/header-bottom-->
	</header><!--/header-->
	<a id="scrollUp" href="#top"
		style="display: none; position: fixed; z-index: 2147483647;"><i class="fa fa-angle-up"></i>
	</a>
</body>
</html>