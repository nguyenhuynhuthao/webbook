<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>customer</title>
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="<c:url value="/resources/page/images/ico/favicon.ico"/>">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<c:url value="/resources/page/images/ico/apple-touch-icon-144-precomposed.png"/>">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<c:url value="/resources/page/images/ico/apple-touch-icon-114-precomposed.png"/>">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<c:url value="/resources/page/images/ico/apple-touch-icon-72-precomposed.png"/>">
    <link rel="apple-touch-icon-precomposed" href="<c:url value="/resources/page/images/ico/apple-touch-icon-57-precomposed.png"/>">
<link href="<c:url value="../resources/manager/css/style.css"/>" rel='stylesheet' type='text/css'/>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Bonfire Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!--fonts-->
<link href="<c:url value='http://fonts.googleapis.com/css?family=Exo:100,200,300,400,500,600,700,800,900'/>" rel='stylesheet' type='text/css'>
<!--//fonts-->
<link rel="stylesheet"
	href="<c:url value="/resources/page/css/bootstrap.min.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/page/css/font-awesome.min.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/page/css/prettyPhoto.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/page/css/price-range.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/page/css/animate.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/page/css/main.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/page/css/responsive.css" />">
</head>
<body>
	<jsp:include page="header.jsp"></jsp:include>

	<div class="tygh-content clearfix">
		<div class="container ">
			<div class="row">
				<div class="span16 ">
					<div class="ty-wysiwyg-content" data-ca-live-editor-object-id="0"
						data-ca-live-editor-object-type="">
						<div style="font-size: 16px; text-align: justify;" ltr"=""""="" "="">Trân
							trọng kính chào các khách hàng của Nhà sách trực tuyến
							Nhanvan.vn.</div>
						<p style="text-align: justify;">
							Hiện nay, do mức chiết khấu trên website Nhanvan.vn đang là rất
							cao, đặc biệt vào các thời điểm có chương trình khuyến mãi. Do
							đó, số lượng sản phẩm giảm KHỦNG có giới hạn nhất định, vì vậy để
							đảm bảo quyền lợi của từng khách hàng của Nhanvan.vn, chúng tôi
							xin thông báo chính sách về “Đơn Hàng Mua Sỉ” và việc xuất hóa
							đơn GTGT như sau:<br>
						</p>
						<p dir="ltr"
							style="line-height: 1.38; font-size: 16px; margin-top: 0pt; margin-bottom: 0pt; text-align: justify;">1.
							Đơn hàng được xem là “đơn hàng mua sỉ” khi thoả 1 trong các tiêu
							chí sau đây:</p>
						<p dir="ltr"
							style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt; margin-left: 36pt; text-align: justify;">
							<br>
						</p>
						<p dir="ltr"
							style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt; margin-left: 36pt; text-align: justify;">-
							Tổng giá trị các đơn hàng trong ngày có giá trị: từ 1.000.000
							đồng (một triệu đồng) trở lên.</p>
						<p dir="ltr"
							style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt; margin-left: 36pt; text-align: justify;">
							<br>
						</p>
						<p dir="ltr"
							style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt; margin-left: 36pt; text-align: justify;">
							- 1 Đơn hàng hoặc tổng các đơn hàng trong ngày: <strong>có
								1 sản phẩm có tổng số lượng từ 10 trở lên</strong> (Cùng 1 mã hàng).<br>
						</p>
						<p dir="ltr"
							style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt; margin-left: 36pt; text-align: justify;">
							<br>
						</p>
						<p dir="ltr"
							style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt; margin-left: 36pt; text-align: justify;">
							- 1 Đơn hàng hoặc tổng các đơn hàng trong ngày: có tổng số lượng
							<strong>sản phẩm (đủ loại) từ 20 sản phẩm trở lên</strong> và Số
							lượng mỗi tựa (Cùng 1 mã hàng) từ 5 trở lên. <strong>Không
								áp dụng cho mặt hàng VPP.</strong>
						</p>
						<p dir="ltr"
							style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt; margin-left: 36pt; text-align: justify;">
							<br>
						</p>
						<p dir="ltr"
							style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt; margin-left: 36pt; text-align: justify;">
							- Các đơn hàng có <strong>cùng thông tin người mua hàng
								(cùng số điện thoại, cùng email hoặc cùng địa chỉ nhận hàng)</strong> thì
							được tính là đơn hàng của 1 khách hàng.
						</p>
						<p dir="ltr"
							style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt; margin-left: 36pt; text-align: justify;">
							<br>
						</p>
						<p dir="ltr"
							style="line-height: 1.38; font-size: 16px; margin-top: 0pt; margin-bottom: 0pt; text-align: justify;">2.
							Chính sách giá (% chiết khấu giảm giá). Đây là chính sách chung
							chỉ mang tính tương đối. Xin quý khách vui lòng liên lạc với Nhân
							Văn để có chính sách giá chính xác nhất:</p>
						<p dir="ltr"
							style="line-height: 1.38; font-size: 16px; margin-top: 0pt; margin-bottom: 0pt; text-align: justify;">
							<br>
						</p>
						<ul>
							<li dir="ltr">Đối với Nhóm hàng <strong>sách độc
									quyền Nhân Văn</strong>: áp dụng mức giảm giá trên web tối đa không vượt
								quá 40%.
							</li>
						</ul>
						<ul>
							<li dir="ltr">Đối với Nhóm hàng <strong>sách của
									các nhà cung cấp trong nước</strong>: áp dụng mức giảm giá trên web tối
								đa không vượt quá 30%.
							</li>
						</ul>
						<ul>
							<li dir="ltr">Đối với Nhóm hàng <strong>sách từ NXB
									Giáo Dục, Oxford và Đại Trường Phát</strong>: áp dụng mức giảm giá trên
								web tối đa không vượt quá 10%.
							</li>
						</ul>
						<ul>
							<li dir="ltr">Đối với Nhóm hàng <strong>văn phòng
									phẩm, đồ chơi, dụng cụ học sinh</strong>: áp dụng mức giảm giá trên web
								tối đa không vượt quá 15%.
							</li>
						</ul>
						<ul>
							<li dir="ltr">Đối với Nhóm hàng <strong>giấy photo,
									sản phẩm điện tử, văn hóa phẩm</strong>: áp dụng mức giảm giá trên web
								tối đa không vượt quá 10%.
							</li>
						</ul>
						<p style="text-align: justify;">
							Quý khách có nhu cầu mua số lượng lớn, vui lòng liên hệ bộ phận
							kinh doanh Nhanvan.vn để có chính sách tốt nhất: <strong>028.36007777</strong>
							hoặc <strong>1900 2806</strong>, hoặc Email: <strong>sale@nhanvan.vn</strong>.
						</p>
						<p p="" dir="ltr"
							style="line-height: 1.38; font-size: 16px; margin-top: 0pt; margin-bottom: 0pt; text-align: justify;">3.
							Đối với việc thanh toán đơn hàng sỉ và xuất hóa đơn GTGT:</p>
						<p p="" dir="ltr"
							style="line-height: 1.38; font-size: 16px; margin-top: 0pt; margin-bottom: 0pt; text-align: justify;">
							<br>
						</p>
						<p style="text-align: justify; margin-left: 20px;">
							<span></span>- Khách hàng mua đơn hàng sỉ hoặc có nhu cầu xuất
							hóa đơn GTGT phải thanh toán trước cho Nhân Văn từ 50 - 100% giá
							trị đơn hàng, chúng tôi sẽ chịu hoàn toàn trách nhiệm về khâu vận
							chuyển và chất lượng hàng hóa đến tay khách hàng.
						</p>
						<p style="text-align: justify;">
							(Chính sách mua sỉ có thể thay đổi tùy theo thời gian thực tế)<br>
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>

	<jsp:include page="footer.jsp"></jsp:include>
	<script src="../resources/page/js/jquery.js"></script>
	<script src="../resources/page/js/price-range.js"></script>
    <script src="../resources/page/js/jquery.scrollUp.min.js"></script>
	<script src="../resources/page/js/bootstrap.min.js"></script>
    <script src="../resources/page/js/jquery.prettyPhoto.js"></script>
    <script src="../resources/page/js/main.js"></script>
</body>
</html>