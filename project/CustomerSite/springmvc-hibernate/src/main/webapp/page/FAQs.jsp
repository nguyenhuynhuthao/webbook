<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>FAQs</title>
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="<c:url value="/resources/page/images/ico/favicon.ico"/>">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<c:url value="/resources/page/images/ico/apple-touch-icon-144-precomposed.png"/>">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<c:url value="/resources/page/images/ico/apple-touch-icon-114-precomposed.png"/>">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<c:url value="/resources/page/images/ico/apple-touch-icon-72-precomposed.png"/>">
    <link rel="apple-touch-icon-precomposed" href="<c:url value="/resources/page/images/ico/apple-touch-icon-57-precomposed.png"/>">
<link href="<c:url value="../resources/manager/css/style.css"/>" rel='stylesheet' type='text/css'/>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Bonfire Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!--fonts-->
<link href="<c:url value='http://fonts.googleapis.com/css?family=Exo:100,200,300,400,500,600,700,800,900'/>" rel='stylesheet' type='text/css'>
<!--//fonts-->
<link rel="stylesheet"
	href="<c:url value="/resources/page/css/bootstrap.min.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/page/css/font-awesome.min.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/page/css/prettyPhoto.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/page/css/price-range.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/page/css/animate.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/page/css/main.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/page/css/responsive.css" />">
</head>
<body>
	<jsp:include page="header.jsp"></jsp:include>

	<div class="container ">
		<div class="ty-wysiwyg-content" data-ca-live-editor-object-id="0"
			data-ca-live-editor-object-type="">
			<h1 align="center"
				style="font-family: Arial, Helvetica, Verdana, Tahoma, sans-serif;">
				CHÍNH SÁCH ĐỔI / TRẢ / HOÀN TIỀN<br>
			</h1>
			<p style="color: rgb(51, 51, 51);">Chúng tôi luôn trân trọng sự
				tin tưởng và ủng hộ của khách hàng khi đặt mua sản phẩm tại
				đây. Do đó chúng tôi luôn cố gắng hoàn thiện dịch vụ tốt nhất
				phục vụ mọi nhu cầu mua sắm của quý khách.</p>
			<p style="color: rgb(51, 51, 51);">chúng tôi luôn luôn
				cam kết tất cả các sản phẩm bán tại đây 100% là những sản
				phẩm chất lượng và xuất xứ nguồn gốc rõ ràng, hợp pháp cũng như an
				toàn cho người tiêu dùng.</p>
			<p style="color: rgb(51, 51, 51);">
				Để việc mua sắm của quý khách tại đây là trải nghiệm dịch vụ
				<span></span>thân thiện, chúng tôi hy vọng quý khách lúc nhận hàng
				kiểm tra kỹ lại:
			</p>
			<p style="color: rgb(51, 51, 51);" rel="color: rgb(51, 51, 51);">-
				Số lượng.</p>
			<p style="color: rgb(51, 51, 51);">
				<span></span>- Tên sản phẩm và chất lượng sản phẩm.
			</p>
			<p style="color: rgb(51, 51, 51);" rel="color: rgb(51, 51, 51);">
				<span></span>- Thông tin sản phẩm, thông tin người nhận <em><strong>(Đối
						chiếu với thông tin trên phiếu giao hàng được bỏ trong hộp</strong></em>) trong
				lúc nhận hàng trước khi ký nhận và thanh toán tiền cho nhân viên
				giao nhận.
			</p>
			<p style="color: rgb(51, 51, 51);">
				Tuy nhiên để quý khách yên tâm hơn trong việc mua sắm và trải nghiệm
				dịch vụ của chúng tôi<a href="#"></a>, chúng
				tôi xây dựng chính sách đổi/ trả/ hoàn tiền trên tinh thần bảo vệ
				quyền lợi người tiêu dùng nhằm cam kết với quý khách về chất lượng
				sản phẩm và dịch vụ của chúng tôi.
			</p>
			<p style="color: rgb(51, 51, 51);">
				Khi quý khách hàng có hàng hóa mua tại đây<a
					href="#"></a> cần đổi/ trả, xin quý khách hàng
				liên hệ với chúng tôi qua hotline <strong><span
					style="color: rgb(192, 80, 77);">0703132898</span></strong> hoặc email: <strong>abcxyz@gmail.com</strong>
			</p>
			<h2 align="center"
				style="font-family: Arial, Helvetica, Verdana, Tahoma, sans-serif;">1.
				Thời gian giải quyết đổi trả</h2>
			<center style="color: rgb(51, 51, 51);">
				<table border="1" cellspacing="0" cellpadding="0"
					style="background-color: rgb(255, 255, 255); width: 727px;">
					<tbody style="margin: 0px; padding: 0px;">
						<tr style="margin: 0px; padding: 0px;">
							<td
								style="padding: 10px; border-width: 1pt; margin: 0px; text-align: center;">
							</td>
							<td width="204"
								style="padding: 10px; border-width: 1pt; margin: 0px;">
								<p align="center"
									style="margin-top: 0px; margin-bottom: 0.11in; padding: 0px;">
									<strong>Đơn hàng của quý khách</strong>
								</p>
								<p align="center"
									style="margin-top: 0px; margin-bottom: 10px; padding: 0px;">
									<strong>được giao thành công</strong>
								</p>
							</td>
							<td width="263"
								style="padding: 10px; border-left-width: 1pt; margin: 0px;">
								<p align="center"
									style="margin-top: 0px; margin-bottom: 0.11in; padding: 0px;">
									<strong>Đơn hàng quý khách</strong>
								</p>
								<p align="center"
									style="margin-top: 0px; margin-bottom: 10px; padding: 0px;">
									<strong>đang trên đường đi</strong>
								</p>
							</td>
						</tr>
						<tr style="margin: 0px; padding: 0px;">
							<td width="211"
								style="padding: 10px; border-top-width: 1pt; margin: 0px;">
								<p style="margin-top: 0px; margin-bottom: 10px; padding: 0px;">
									<strong>Thời gian giải quyết</strong>
								</p>
							</td>
							<td width="204"
								style="padding: 10px; border-top-width: 1pt; margin: 0px;">
								<p style="margin-top: 0px; margin-bottom: 10px; padding: 0px;">Tối
									đa 7 ngày làm việc</p>
							</td>
							<td width="263" style="padding: 10px; margin: 0px;">
								<p style="margin-top: 0px; margin-bottom: 10px; padding: 0px;">Khi
									nhận được yêu cầu từ quý khách</p>
							</td>
						</tr>
						<tr style="margin: 0px; padding: 0px;">
							<td width="211" style="padding: 10px; margin: 0px;">
								<p style="margin-top: 0px; margin-bottom: 10px; padding: 0px;">
									<strong>Nội dung được giải quyết</strong>
								</p>
							</td>
							<td width="204" style="padding: 10px; margin: 0px;">
								<p style="margin-top: 0px; margin-bottom: 10px; padding: 0px;">Theo
									hiện trạng đơn hàng cần đổi/trả. (xem cụ thể tại #2 điều kiện
									đổi trả)</p>
							</td>
							<td width="263" style="padding: 10px; margin: 0px;">
								<p style="margin-top: 0px; margin-bottom: 10px; padding: 0px;">Chúng
									tôi chỉ hỗ trợ quý khách đổi mặt hàng khác để phù hợp hơn với
									nhu cầu của quý khách/hủy đơn hàng(*)</p>
							</td>
						</tr>
						<tr style="margin: 0px; padding: 0px;">
							<td colspan="3" width="706" style="padding: 10px; margin: 0px;">
								<p style="margin-top: 0px; margin-bottom: 10px; padding: 0px;">(*)
									Nếu quý khách hủy đơn hàng cũ, đã chuyển khoản thanh toán thành
									công,mà không có nhu cầu đặt lại đơn hàng khác → chúng tôi sẽ
									hoàn tiền lại cho Quý khách trong vòng 7 ngày làm việc qua hệ
									thống ngân hàng kể từ ngày nhận và giải quyết yêu cầu cho quý
									khách</p>
							</td>
						</tr>
					</tbody>
				</table>
				<p
					style="margin-top: 0px; margin-bottom: 0.11in; font-size: 12px; background-color: rgb(255, 255, 255); padding: 0px; font-family: OpenSansRegular, Arial, Helvetica, sans-serif;">
					<strong>Chính sách này được áp dụng cho toàn bộ đơn hàng
						của Quý khách tại cửa hàng</strong>
				</p>
			</center>
			<h2 align="center"
				style="font-family: Arial, Helvetica, Verdana, Tahoma, sans-serif;">2.
				Điều kiện đổi trả:</h2>
			<p
				style="margin-top: 0px; margin-bottom: 0.11in; margin-left: 0.25in; font-size: 12px; background-color: rgb(255, 255, 255); color: rgb(51, 51, 51); padding: 0px; font-family: OpenSansRegular, Arial, Helvetica, sans-serif;">chúng tôi
				hỗ trợ đổi/ trả sản phẩm cho quý khách nếu:</p>
			<ul>
				<li>Sản phẩm còn nguyên bao bì như hiện trạng ban đầu.</li>
				<li>Sản phẩm còn đầy đủ phụ kiện, quà tặng khuyến mãi kèm theo.</li>
				<li>Phiếu giao hàng / Hóa đơn GTGT ( nếu có)</li>
			</ul>
			<h2 align="center"
				style="font-family: Arial, Helvetica, Verdana, Tahoma, sans-serif;">3.
				Chính sách đổi trả</h2>
			<ul>
				<li>Tất cả các đơn hàng đổi trả cần được chụp hình và gửi email
					về <u>abcxyz@gmail.com</u> với tiêu đề “Đổi Trả Đơn Hàng #100xxx”:
					<ul>
						<li>Chụp hình về tình trạng sản phẩm, nêu rõ lỗi kỹ thuật nếu
							có.</li>
						<li>Chụp hình về tình trạng bao bì, chú ý các điểm như:
							<ul>
								<li>Sách có được bọc màng co hay không?</li>
								<li>Hộp có được chèn giấy vụn để bảo vệ khi vận chuyển hay
									không?</li>
								<li>Hộp bị ướt …?</li>
							</ul>
						</li>
					</ul>
				</li>
			</ul>
			<center style="color: rgb(51, 51, 51);">
				<table border="1" cellspacing="0" cellpadding="0"
					style="background-color: rgb(255, 255, 255); width: 727px;">
					<tbody style="margin: 0px; padding: 0px;">
						<tr valign="top" style="margin: 0px; padding: 0px;">
							<td width="26" style="padding: 10px; margin: 0px;"></td>
							<td width="220" style="padding: 10px; margin: 0px;">
								<p align="center"
									style="margin-top: 0px; margin-bottom: 10px; padding: 0px;">
									<strong>Nội dung đổi/ trả sản phẩm</strong>
								</p>
							</td>
							<td width="471" style="padding: 10px; margin: 0px;">
								<p align="center"
									style="margin-top: 0px; margin-bottom: 10px; padding: 0px;">
									<strong>Cách thức xử lý</strong>
								</p>
							</td>
						</tr>
						<tr style="margin: 0px; padding: 0px;">
							<td width="26" style="padding: 10px; margin: 0px;">
								<p align="center"
									style="margin-top: 0px; margin-bottom: 10px; padding: 0px;">
									<strong>1</strong>
								</p>
							</td>
							<td width="220" style="padding: 10px; margin: 0px;">
								<p style="margin-top: 0px; margin-bottom: 10px; padding: 0px;">Lỗi
									kỹ thuật của sản phẩm (sách thiếu trang, sút gáy, trùng nội
									dung, sản phẩm điện tử không hoạt động..)</p>
							</td>
							<td width="471" style="padding: 10px; margin: 0px;">
								<ul>
									<li>Cửa hàng có sản phẩm→ đổi mới cùng sản phẩm</li>
									<li>Cửa hàng hết hàng→ Hoàn tiền (*)/ Quý khách có thể
										chọn mặt hàng khác tại website</li>
								</ul>
							</td>
						</tr>
						<tr style="margin: 0px; padding: 0px;">
							<td width="26" style="padding: 10px; margin: 0px;">
								<p align="center"
									style="margin-top: 0px; margin-bottom: 10px; padding: 0px;">
									<strong>2</strong>
								</p>
							</td>
							<td width="220" style="padding: 10px; margin: 0px;">
								<p style="margin-top: 0px; margin-bottom: 10px; padding: 0px;">Sản
									phẩm hỏng do quý khách</p>
							</td>
							<td valign="top" width="471" style="padding: 10px; margin: 0px;">
								<ul>
									<li>Không hỗ trợ đổi/ trả</li>
								</ul>
							</td>
						</tr>
						<tr style="margin: 0px; padding: 0px;">
							<td width="26" style="padding: 10px; margin: 0px;">
								<p align="center"
									style="margin-top: 0px; margin-bottom: 10px; padding: 0px;">
									<strong>3</strong>
								</p>
							</td>
							<td width="220" style="padding: 10px; margin: 0px;">
								<p style="margin-top: 0px; margin-bottom: 10px; padding: 0px;">Lý
									do đổi/trả sản phẩm như: khách đặt nhầm, sách có nội dung không
									hay, khách không thích sản phẩm đã đặt hoặc nhu cầu của khách
									thay đổi.</p>
							</td>
							<td valign="top" width="471" style="padding: 10px; margin: 0px;">
								<ul>
									<li>Không hỗ trợ đổi/ trả</li>
								</ul>
							</td>
						</tr>
						<tr style="margin: 0px; padding: 0px;">
							<td width="26" style="padding: 10px; margin: 0px;">
								<p align="center"
									style="margin-top: 0px; margin-bottom: 10px; padding: 0px;">
									<strong>4</strong>
								</p>
							</td>
							<td width="220" style="padding: 10px; margin: 0px;">
								<p style="margin-top: 0px; margin-bottom: 10px; padding: 0px;">Giao
									nhầm/ giao thiếu ( thiếu sản phẩm đã đặt, thiếu phụ kiện, thiếu
									quà tặng kèm theo)</p>
							</td>
							<td width="471" style="padding: 10px; margin: 0px;">
								<ul>
									<li>Giao nhầm → Đổi lại đúng sản phẩm đã đặt.</li>
									<li>Giao thiếu → Giao bù thêm số lượng còn thiếu theo đơn
										hàng</li>
								</ul>
							</td>
						</tr>
						<tr style="margin: 0px; padding: 0px;">
							<td width="26" style="padding: 10px; margin: 0px;">
								<p align="center"
									style="margin-top: 0px; margin-bottom: 10px; padding: 0px;">
									<strong>5</strong>
								</p>
							</td>
							<td width="220" style="padding: 10px; margin: 0px;">
								<p style="margin-top: 0px; margin-bottom: 10px; padding: 0px;">Chất
									lượng hàng hóa kém do vận chuyển</p>
							</td>
							<td valign="top" width="471" style="padding: 10px; margin: 0px;">
								<p style="margin-top: 0px; margin-bottom: 10px; padding: 0px;">Khi
									quý khách hàng nhận gói hàng bị móp méo, ướt, chúng tôi khuyến
									cáo khách hàng nên kiểm tra thực tế hàng hóa bên trong ngay
									thời điểm nhận hàng, vui lòng phản ảnh hiện trang hàng hóa lên
									bill nhận hàng từ phía nhân viên giao nhận và liên lạc với
									chúng tôi về Hotline 0703132898 để được hỗ trợ giải quyết cụ thể.
								</p>
							</td>
						</tr>
						<tr style="margin: 0px; padding: 0px;">
							<td width="26" style="padding: 10px; margin: 0px;">
								<p align="center"
									style="margin-top: 0px; margin-bottom: 10px; padding: 0px;">
									<strong>6</strong>
								</p>
							</td>
							<td width="220" style="padding: 10px; margin: 0px;">
								<p style="margin-top: 0px; margin-bottom: 10px; padding: 0px;">Hình
									thức sản phẩm không giống mô tả ban đầu</p>
							</td>
							<td valign="top" width="471" style="padding: 10px; margin: 0px;">
								<p style="margin-top: 0px; margin-bottom: 10px; padding: 0px;">
									Hãy liên hệ với chúng tôi qua số <strong>Hotline </strong>0703132898,
									chúng tôi sẵn sàng lắng nghe và giải quyết cho bạn ( cụ thể
									theo từng trường hợp)
								</p>
							</td>
						</tr>
						<tr style="margin: 0px; padding: 0px;">
							<td colspan="3" valign="top" width="745" height="36"
								style="padding: 10px; margin: 0px;">
								<p style="margin-top: 0px; margin-bottom: 0.11in; padding: 0px;">(*)
									Phương thức hoàn tiền: Thông qua tài khoản ngân hàng từ 2-7
									ngày làm việc ( sau khi chúng tôi nhận được sản phẩm trả về)</p>
								<p style="margin-top: 0px; margin-bottom: 10px; padding: 0px;">
									<strong>Chúng tôi sẽ kiểm tra các trường hợp trên và
										giải quyết cho quý khách trong 7 ngày làm việc kể từ khi quý
										khách nhận được hàng, quá thời hạn trên rất tiếc chúng tôi
										không giải quyết khiếu nại.</strong>
								</p>
							</td>
						</tr>
					</tbody>
				</table>
				<p>
					<br>
				</p>
				<h2></h2>
				<center></center>
			</center>
			<h2>
				<center style="color: rgb(51, 51, 51);">
					<center>
						4. Cách thức chuyển sản phẩm đổi trả về website<strong></strong>
					</center>
					<h2></h2>
				</center>
			</h2>
			<p style="color: rgb(51, 51, 51);" rel="color: rgb(51, 51, 51);">
				<br>
			</p>
			<ul>
				<li>Khi yêu cầu đổi trả được giải quyết, quý khách vui lòng
					đóng gói sản phẩm như hiện trạng khi nhận hàng ban đầu (bao gồm sản
					phẩm, quà tặng, phụ kiện kèm theo sản phẩm,…nếu có) và gửi về
					website trong thời gian sớm nhất
					trong vòng 3 ngày làm việc kể từ khi thông báo với bộ phận Chăm Sóc
					Khách Hàng và gửi kèm:
					<ul>
						<li>Hóa đơn giá trị gia tăng (nếu có) hoặc phiếu giao hàng
							<a href=""></a>
						</li>
						<li>Phụ kiện đi kèm sản phẩm và tặng khuyến mãi kèm theo (nếu
							có)</li>
					</ul>
				</li>
				<li>Điền họ tên, địa chỉ khách hàng, mã số đơn hàng, mã trả
					hàng do chúng tôi cung cấp để khách hàng không tốn phí trả hàng về
					và gửi hàng về địa chỉ do chúng tôi giải quyết đổi trả gửi qua
					email của quý khách.</li>
			</ul>
			<ul>
				<li><strong>Lưu ý</strong>: Quý khách vui lòng chỉ gởi sản phẩm
					qua đường bưu điện và chịu trách nhiệm về trạng thái nguyên vẹn của
					sản phẩm khi gửi về<a href="#"></a><a
					href="#"></a>. Quý khách có thể ghi nhớ mã bưu
					kiện mình gửi để khi cần tra cứu thông tin kiện hàng gửi trả tại sẽ
					thuận lợi và nhanh chóng hơn.
					<ul>
						<li>Sau khi nhận được sản phẩm quý khách gởi về, website<a
							href="#"></a><a
							href="#"></a> sẽ phản hồi và cập nhật thông
							tin trên từng giai đoạn xử lý đến quý khách qua điện thoại /
							email .
						</li>
						<li>Rất tiếc hiện tại chúng tôi chưa hỗ trợ phí vận chuyển
							đổi trả hàng (Ngoại trừ do lỗi dịch vụ, sản phẩm của chúng tôi).
							Trong trường hợp đổi hàng không phải do lỗi của chúng tôi, quý
							khách vui lòng thanh toán cước phí vận chuyển khi trả hàng về
							</li>
						<h2></h2>
					</ul>
					<h2 align="center">5. Hủy đơn hàng đã thanh toán: Đối với những đơn hàng đã
						thanh toán trực tuyến (Chuyển khoản hoặc Cổng VNPAY)</h2></li>
				<li><strong>Nếu nguyên nhân hủy do Nhân Văn:</strong> Sản phẩm
					hết hàng, đơn hàng xác nhận sai -&gt; Nhân Văn sẽ chuyển hoàn số
					tiền khách hàng đã thanh toán</li>
				<li><strong>Nếu nguyên nhân hủy Khách hàng:</strong> Muốn mua
					sản phẩm khác, không mua đơn hàng nữa -&gt; Nhân Văn sẽ chuyển
					hoàn số tiền khách hàng đã thanh toán (có phí hoàn 10.000đ / đơn
					hàng)</li>
			</ul>
		</div>
	</div>

	<jsp:include page="footer.jsp"></jsp:include>
	<script src="../resources/page/js/jquery.js"></script>
	<script src="../resources/page/js/price-range.js"></script>
    <script src="../resources/page/js/jquery.scrollUp.min.js"></script>
	<script src="../resources/page/js/bootstrap.min.js"></script>
    <script src="../resources/page/js/jquery.prettyPhoto.js"></script>
    <script src="../resources/page/js/main.js"></script>
</body>
</html>