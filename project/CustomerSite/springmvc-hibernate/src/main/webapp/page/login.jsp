<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>login</title>
   <link href="<c:url value="/resources/page/css/bootstrap.min.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/page/css/font-awesome.min.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/page/css/prettyPhoto.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/page/css/price-range.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/page/css/animate.css"/>" rel="stylesheet">
	<link href="<c:url value="/resources/page/css/main.css"/>" rel="stylesheet">
	<link href="<c:url value="/resources/page/css/responsive.css"/>" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<c:url value="/resources/page/images/ico/apple-touch-icon-144-precomposed.png"/>">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<c:url value="/resources/page/images/ico/apple-touch-icon-114-precomposed.png"/>">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<c:url value="/resources/page/images/ico/apple-touch-icon-72-precomposed.png"/>">
    <link rel="apple-touch-icon-precomposed" href="<c:url value="/resources/page/images/ico/apple-touch-icon-57-precomposed.png"/>">
    <script type="text/javascript" src="<c:url value="/resources/page/js/jquery.js"/>"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" type="text/javascript"/>"></script>
    <link href="<c:url value="../resources/manager/css/style.css"/>" rel='stylesheet' type='text/css'/>
	</head>
	<body onload="">
		<jsp:include page="header.jsp"></jsp:include>
	<div id="form"><!--form-->
		<div class="container">
			<div class="row">
				<div class="col-sm-4 col-sm-offset-1">
					<div class="login-form"><!--login form-->
						<h2>Đăng nhập tài khoản</h2>
						<form action="./login" method="post" >
						<p style="color:red">${error}</p>
							<input type="text" placeholder="Tài khoản" id="username" name="name"/>
							<input type="password" placeholder="Mật khẩu" id="password" name="pass"/>
							<div id="x-pass" class="error"></div>
							<span>
								<input type="checkbox" class="checkbox"  path=""/> 
								Giữ đăng nhập
							</span>
							<button type="submit" class="btn btn-default" >Đăng nhập</button>
						</form>
					</div><!--/login form-->
				</div>
				<div class="col-sm-1">
					<h2 class="or">Hoặc</h2>
				</div>
				<div class="col-sm-4">
					<div class="signup-form"><!--sign up form-->
							<h2>Đăng ký tài khoản</h2>
							<form action="./adduser" method="post">
							<p style="color:red">${error1}</p>
								<input type="text" placeholder="Tên tài khoản" id="name" name="name" />
								<input type="password" placeholder="Mật khẩu" id="pass" name="pass" />
								<input type="password" placeholder="Xác nhận mật khẩu" id="sspass" name="sspass"/>
								<button type="submit" class="btn btn-default" >Đăng ký</button>
							</form>
					</div><!--/sign up form-->
				</div>
			</div>
		</div>
	</div><!--/form-->
	<jsp:include page="footer.jsp"></jsp:include>
	<script type="text/javascript" src="<c:url value="/resources/page/js/regis.js"/>"></script>
	
	<script type="text/javascript">
	function getUser(id){
		console.log(id);
		if(id != undefined)
			location.href="../controller/index";
		//link nào vào trang chủ anh index á
		//trang này có nhận biến ào làm điền kiện login ko anh
		// trang index là a join của 4 trang lại như header content foodter với banner
		//chỉ lần gọi iđẽ là vào đc home đúng ko á đủng ồi á
	}
	</script>
</body>
</html>