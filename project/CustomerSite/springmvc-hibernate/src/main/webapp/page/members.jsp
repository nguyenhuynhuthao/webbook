<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>members</title>
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="<c:url value="/resources/page/images/ico/favicon.ico"/>">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<c:url value="/resources/page/images/ico/apple-touch-icon-144-precomposed.png"/>">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<c:url value="/resources/page/images/ico/apple-touch-icon-114-precomposed.png"/>">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<c:url value="/resources/page/images/ico/apple-touch-icon-72-precomposed.png"/>">
    <link rel="apple-touch-icon-precomposed" href="<c:url value="/resources/page/images/ico/apple-touch-icon-57-precomposed.png"/>">
<link href="<c:url value="../resources/manager/css/style.css"/>" rel='stylesheet' type='text/css'/>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Bonfire Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!--fonts-->
<link href="<c:url value='http://fonts.googleapis.com/css?family=Exo:100,200,300,400,500,600,700,800,900'/>" rel='stylesheet' type='text/css'>
<!--//fonts-->
<link rel="stylesheet"
	href="<c:url value="/resources/page/css/bootstrap.min.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/page/css/font-awesome.min.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/page/css/prettyPhoto.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/page/css/price-range.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/page/css/animate.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/page/css/main.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/page/css/responsive.css" />">
</head>
<body>
	<jsp:include page="header.jsp"></jsp:include>

	<div class="container ">
		<div class="row">
			<div class="span16 ">
				<div class="ty-banner__image-wrapper">
					<img class="ty-pict     cm-image" id="det_img_1741625874"
						src="https://nhanvan.vn/images/promo/34/1.png" alt="" title="">

				</div>
				<div class="ty-wysiwyg-content" data-ca-live-editor-object-id="0"
					data-ca-live-editor-object-type="">
					<p>
						Mỗi lần thăng hạng thành viên, khách hàng sẽ được <strong>TẶNG
							1 LẦN VẬN CHUYỂN TIẾT KIỆM MIỄN PHÍ (FREESHIP) </strong>
					</p>
					<p>
						<strong>Từ cấp MEMBER trở lên, khách hàng sẽ nhận được
							Phiếu Mua Hàng 100k - </strong>Áp dụng mua tất cả mặt hàng trên website.
					</p>
				</div>
			</div>
		</div>


		<div class="row">
			<div class="span16 ">
				<div class="ty-banner__image-wrapper">
					<img class="ty-pict     cm-image" id="det_img_1467409643"
						src="https://nhanvan.vn/images/promo/34/2.png" alt="" title="">

				</div>
				<div class="sd-loyalty-program">
					<div class="sd-loyalty-program__card">


						<h2 class="ty-mainbox-simple-title">Khách Hàng Tiềm Năng</h2>

						<div class="ty-price">
							<span class="ty-price-num">Từ <bdi>
								<span>300,000</span>&nbsp;VNĐ</bdi></span>
						</div>

						<div class="sd-loyalty-program-desc">
							<p>
								<span class="fa fa-cog fa-spin"
									style="background-color: initial; font-size: 14px;">﻿</span>
								Hoàn thành thanh toán <strong>300.000đ</strong> tại website
								bạn đã đạt mức thành viên: <strong><span
									style="color: rgb(79, 97, 40);">0. KHÁCH HÀNG TIỀM NĂNG</span></strong>
							</p>
							<p>
								<span class="fa fa-cog fa-spin"
									style="background-color: initial; font-size: 14px;">﻿</span> <strong><span
									style="color: rgb(12, 12, 12);"><strong><span
											style="color: rgb(31, 73, 125);"><strong><span
													style="color: rgb(79, 97, 40);">KHÁCH HÀNG TIỀM NĂNG</span></strong></span></strong></span></strong>
								sẽ nhận 1% điểm thưởng (theo giá trị đơn hàng) cho tất cả đơn
								hàng.<br>
							</p>
							<p>
								(Điểm thưởng có thể dùng để trừ trực tiếp vào đơn hàng)<br>
							</p>
							<p>
								<span class="fa fa-check-square-o"
									style="background-color: initial; font-size: 14px;">﻿</span>
								Tích lũy <strong>1.000.000đ</strong> để thăng cấp <strong><span
									style="color: rgb(127, 127, 127);"><strong><span
											style="color: rgb(31, 73, 125);">1. MEMBER</span></strong></span></strong>
							</p>
						</div>
					</div>
					<div class="sd-loyalty-program__card">
						<h2 class="ty-mainbox-simple-title">MEMBER</h2>

						<div class="ty-price">
							<span class="ty-price-num">Từ <bdi>
								<span>1,000,000</span>&nbsp;VNĐ</bdi></span>
						</div>

						<div class="sd-loyalty-program-desc">
							<p>
								<span class="fa fa-cog fa-spin"
									style="background-color: initial; font-size: 14px;">﻿</span>
								Tích lũy đạt <strong>1.000.000đ</strong> tại website bạn đã
								đạt mức thành viên: <strong><span
									style="color: rgb(31, 73, 125);">1. MEMBER</span></strong>
							</p>
							<p>
								<span class="fa fa-cog fa-spin"
									style="background-color: initial; font-size: 14px;">﻿</span>
								Thành Viên ở cấp <strong><span
									style="color: rgb(12, 12, 12);"><strong><span
											style="color: rgb(31, 73, 125);">MEMBER</span></strong></span></strong> sẽ nhận 3%
								điểm thưởng (theo giá trị đơn hàng) cho tất cả đơn hàng.<br>
							</p>
							<p>
								(Điểm thưởng có thể dùng để trừ trực tiếp vào đơn hàng)<br>
							</p>
							<p>
								<span class="fa fa-check-square-o">﻿</span> Tích lũy <strong>5.000.000đ</strong>
								để thăng cấp <strong><span
									style="color: rgb(127, 127, 127);">2. SILVER</span></strong>
							</p>
						</div>
					</div>
					<div class="sd-loyalty-program__card">
						


						<h2 class="ty-mainbox-simple-title">SILVER</h2>

						<div class="ty-price">
							<span class="ty-price-num">Từ <bdi>
								<span>5,000,000</span>&nbsp;VNĐ</bdi></span>
						</div>

						<div class="sd-loyalty-program-desc">
							<p>
								<span class="fa fa-cog fa-spin"
									style="background-color: initial; font-size: 14px;">﻿</span>
								Tích lũy đạt thêm <strong>5.000.000đ</strong> tại website bạn
								sẽ đạt mức thành viên: <strong><span
									style="color: rgb(127, 127, 127);">2. SILVER</span></strong>
							</p>
							<p>
								<span class="fa fa-cog fa-spin"
									style="background-color: initial; font-size: 14px;">﻿</span>
								Thành Viên ở cấp <strong><span
									style="color: rgb(127, 127, 127);">SILVER</span></strong> sẽ nhận 5%
								điểm thưởng (theo giá trị đơn hàng) cho tất cả đơn hàng. cho tất
								cả các đơn hàng.
							</p>
							<p>
								(Điểm thưởng có thể dùng để trừ trực tiếp vào đơn hàng)<br>
							</p>
							<p>
								<span class="fa fa-check-square-o" style="font-size: 14px;">﻿</span>
								Tích lũy thêm <strong>10.000.000đ</strong> để thăng cấp <strong><span
									style="color: rgb(127, 127, 127);"><strong><span
											style="color: rgb(192, 145, 0);">3. GOLD</span></strong></span></strong><br>
							</p>
						</div>
					</div>
					<div class="sd-loyalty-program__card">
						


						<h2 class="ty-mainbox-simple-title">GOLD</h2>

						<div class="ty-price">
							<span class="ty-price-num">Từ <bdi>
								<span>10,000,000</span>&nbsp;VNĐ</bdi></span>
						</div>

						<div class="sd-loyalty-program-desc">
							<p>
								<span class="fa fa-cog fa-spin"
									style="background-color: initial; font-size: 14px;">﻿</span>
								Tích lũy đạt <strong>10.000.000đ</strong> tại website bạn đã
								đạt mức thành viên: <strong><span
									style="color: rgb(192, 145, 0);">3. GOLD</span></strong>.
							</p>
							<p>
								<span class="fa fa-cog fa-spin"
									style="background-color: initial; font-size: 14px;">﻿</span>
								Thành Viên ở cấp <strong><span
									style="color: rgb(192, 145, 0);">GOLD</span></strong> sẽ nhận 8% điểm
								thưởng (theo giá trị đơn hàng) cho tất cả đơn hàng. cho tất cả
								các đơn hàng.
							</p>
							<p>
								(Điểm thưởng có thể dùng để trừ trực tiếp vào đơn hàng)<br>
							</p>
							<p>
								<span class="fa fa-check-square-o" style="font-size: 14px;">﻿</span>
								Tích lũy <strong>20.000.000đ</strong> để thăng cấp <strong><span
									style="color: rgb(127, 127, 127);"><strong><span
											style="color: rgb(192, 145, 0);"><strong><span
													style="color: rgb(12, 12, 12);">4. PLATINUM</span></strong></span></strong></span></strong><br>
							</p>
						</div>
					</div>
					<div class="sd-loyalty-program__card">


						<h2 class="ty-mainbox-simple-title">PLATINUM</h2>

						<div class="ty-price">
							<span class="ty-price-num">Từ <bdi>
								<span>20,000,000</span>&nbsp;VNĐ</bdi></span>
						</div>

						<div class="sd-loyalty-program-desc">
							<p>
								<span class="fa fa-cog fa-spin"
									style="background-color: initial; font-size: 14px;">﻿</span>
								Tích lũy đạt <strong>20.000.000đ</strong> tại website bạn đã
								đạt mức thành viên cao nhất: <strong><span
									style="color: rgb(12, 12, 12);">4. PLATINUM</span></strong>.
							</p>
							<p>
								<span class="fa fa-cog fa-spin"
									style="background-color: initial; font-size: 14px;">﻿</span>
								Thành Viên ở cấp <strong><span
									style="color: rgb(12, 12, 12);">PLATINUM</span></strong> sẽ nhận 10%
								điểm thưởng (theo giá trị đơn hàng) cho tất cả đơn hàng.
							</p>
							<p>(Điểm thưởng có thể dùng để trừ trực tiếp vào đơn hàng)</p>
						</div>
					</div>
				</div>
			</div>
		</div>




		<div class="row">
			<div class="span16  ">
				<div class="ty-wysiwyg-content" data-ca-live-editor-object-id="0"
					data-ca-live-editor-object-type="">
					<p style="text-align: justify;">
						- Chương trình Thành Viên chỉ áp dụng cho khách hàng đăng nhập vào
						tài khoản và trải nghiệm mua sắm tại website. Điểm và thông
						tin cá nhân được cập nhật chính thức sau khi giao dịch hoàn tất và
						khách hàng nhận được hàng.
					</p>
					<p>
						<strong></strong>
					</p>
					<p style="text-align: justify;">
						<strong>- Chính sách giảm giá cho Thành Viên không áp
							dụng cho <a href="customers"
							target="_blank">ĐƠN HÀNG MUA SỈ</a> (*).
						</strong>
					</p>
					<p style="text-align: justify;">- Chúng tôi sẽ xét duyệt tự
						động hạng thành viên của khách hàng dựa trên tổng số tiền đã mua
						hàng trên website. Thẻ thành viên có giá trị sử dụng 1 năm và sẽ
						được reset vào ngày 1/1 hàng năm.</p>
					<p style="text-align: justify;">- Chúng tôi không chịu trách
						nhiệm giả quyết các quyền lợi của khách hàng nếu thông tin do
						khách hàng cung cấp không đầy đủ và không chính xác, hoặc nếu quá
						thời hạn nhận quyền lợi theo quy định tại thể lệ chương trình.</p>
					<p style="text-align: justify;">- Chúng tôi có quyền quyết
						định bất kỳ lúc nào về việc chỉnh sửa, xét duyệt nâng hạng hoặc
						chấm dứt chương trình khách hàng thành viên website mà không
						cần báo trước.</p>
					<p style="text-align: justify;">- Thời gian xét duyệt hạng
						thành viên có thể thay đổi mỗi năm. Đối với năm 2018 là ngày
						31/12/2018</p>
					<p style="text-align: justify;">+ Đối với khách hàng mới: Tạo
						tài khoản để mua hàng tại Nhanvan.vn và bắt đầu tích lũy thẻ Thành
						Viên.</p>
					<p style="text-align: justify;">+ Đối với khách hàng đã có tài
						khoản: Thẻ Thành Viên sẽ được tự động cập nhật vào tài khoản dựa
						trên tổng tiền khách hàng đã mua sắm.</p>
					<p style="text-align: justify;">Lưu ý: Sau ngày xét duyệt hạng
						thành viên, hệ thống sẽ cập nhật thông tin vào tài khoản của mỗi
						khách hàng trong thời gian từ 7 – 10 ngày làm việc.</p>
					<p style="text-align: center;">
					</p>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="footer.jsp"></jsp:include>
</body>
</html>