<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Info</title>
 <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="<c:url value="/resources/page/images/ico/favicon.ico"/>">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<c:url value="/resources/page/images/ico/apple-touch-icon-144-precomposed.png"/>">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<c:url value="/resources/page/images/ico/apple-touch-icon-114-precomposed.png"/>">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<c:url value="/resources/page/images/ico/apple-touch-icon-72-precomposed.png"/>">
    <link rel="apple-touch-icon-precomposed" href="<c:url value="/resources/page/images/ico/apple-touch-icon-57-precomposed.png"/>">
<link href="<c:url value="../resources/manager/css/style.css"/>" rel='stylesheet' type='text/css'/>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Bonfire Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!--fonts-->
<link href="<c:url value='http://fonts.googleapis.com/css?family=Exo:100,200,300,400,500,600,700,800,900'/>" rel='stylesheet' type='text/css'>
<!--//fonts-->
<link rel="stylesheet"
	href="<c:url value="/resources/page/css/bootstrap.min.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/page/css/font-awesome.min.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/page/css/prettyPhoto.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/page/css/price-range.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/page/css/animate.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/page/css/main.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/page/css/responsive.css" />">
	
	<script type="text/javascript" src="<c:url value="/resources/page/js/checkout.js" />"></script>
</head>
<body>

<jsp:include page="header.jsp"></jsp:include>

<section id="cart_items">
		<div class="container">
			<div class="breadcrumbs">
				<ol class="breadcrumb">
				  <li><a href="#">Trang chủ</a></li>
				  <c:if test="${thanhtoan==1 }">
				  	<li class="active">Thanh toán</li>
				  </c:if>
				  <c:if test="${thanhtoan==0||thanhtoan==null }">
				  	<li class="active">${usernam.name }</li>
				  </c:if>
				</ol>
			</div><!--/breadcrums-->

			<div class="shopper-informations">
				<div class="row">
					<div class="col-sm-8">
						<div class="shopper-info">
							<p>Thông tin khách hàng</p>
							
								<form:form action="../controller/updateuser" method="post" modelAttribute="user" onsubmit="return check();">
									<form:input path="userDetails.id" type="hidden"/>
									<form:input path="id" type="hidden" id="id"/>
									<form:input path="name"  placeholder="UserName" readonly="${thanhtoan == 1||thanhtoan==0||thanhtoan==null}" id="name"/>
									<form:input path="password"  type="hidden"/>
									<form:input path="userDetails.fullname"  placeholder="Tên khách hàng" readonly="${thanhtoan == 1}" id="fullname"/>
									<form:input path="userDetails.phones" placeholder="Số điện thoại" readonly="${thanhtoan == 1}" id="phone"/>
									<form:input path="userDetails.address.id" type="hidden"/>
									<form:input path="userDetails.address.street" placeholder="Tên Đương" readonly="${thanhtoan == 1}" id="street"/>
									<form:input path="userDetails.address.district" placeholder="Quận/Huyện" readonly="${thanhtoan == 1}" id="distreet"/>
									<form:input path="userDetails.address.city" placeholder="Thành Phố/Tỉnh" readonly="${thanhtoan == 1}" id="city"/>
									<div id="txtcheck" style="color: red;"></div>
									<c:if test="${thanhtoan==0||thanhtoan==null }">
									<button type="submit" class="btn btn-default">Update</button>
									</c:if>
								</form:form>
						
						</div>
					</div>
					
				</div>
			</div>
			<c:if test="${thanhtoan==1 }">
				<div class="review-payment">
					<h2>Xem lại hàng và thanh toán</h2>
				</div>
	
				<div class="table-responsive cart_info">
					<table class="table table-condensed">
						<thead>
							<tr class="cart_menu">
								<td class="image">Tên sản phẩm</td>
								<td class="description"></td>
								<td class="price">Giá</td>
								<td class="quantity">Số lượng</td>
								<td class="total">Tổng</td>
								<td></td>
							</tr>
						</thead>
						<tbody>
						<c:forEach items="${cart}" var="c" >
							<tr>
								<td class="cart_product">
									<a href=""><img src="../resources/page/images/home/${c.book.imgage}" alt="" style="width: 110px;height: 110px"></a>
								</td>
								<td class="cart_description">
									<h4><a href="">${c.book.name }</a></h4>
									<p>ID sản phẩm:${c.book.id }</p>
								</td>
								<td class="cart_price">
									<p>${c.book.price }</p>
								</td>
								<td class="cart_quantity">
									<div class="cart_quantity_button">
										<input class="cart_quantity_input" type="text" name="quantity" value="${c.num}" autocomplete="off" size="2" readonly="true">
									</div>
								</td>
								<td class="cart_total">
									<p class="cart_total_price">${c.book.price * c.num }</p>
								</td>
							</tr>
						</c:forEach>
							<tr>
								<td colspan="4">&nbsp;</td>
								<td colspan="2">
									<table class="table table-condensed total-result">
										<tr>
											<td>Tạm tính:</td>
											<td>${price}</td>
										</tr>
										<tr>
											<td>Phí vận chuyển:</td>
											<td>15000</td>										
										</tr>
										<tr class="shipping-cost">
											<td>VAT 10%: </td>
											<td>${price*0.1 }</td>
										</tr>
										<tr>
											<td>Tổng cộng:</td>
											<td><span>${price + 15000 +(price*0.1)}</span></td>
										</tr>
									</table>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="payment-options">
						<span>
							<label><input type="radio" name="only">  Thanh toán khi nhận hàng</label>
						</span>
						<span>
							<label><input type="radio" name="only"> Thanh toán qua thẻ ngân hàng</label>
						</span>
						<a class="btn btn-primary" href="../cartcontroller/order">Thanh toán</a>
						<a class="btn btn-primary" href="">Để sau</a>
				</div>
			</c:if>
		</div>
	</section> <!--/#cart_items-->
	
	<jsp:include page="footer.jsp"></jsp:include>
	<script src="../resources/page/js/jquery.js"></script>
	<script src="../resources/page/js/price-range.js"></script>
    <script src="../resources/page/js/jquery.scrollUp.min.js"></script>
	<script src="../resources/page/js/bootstrap.min.js"></script>
    <script src="../resources/page/js/jquery.prettyPhoto.js"></script>
    <script src="../resources/page/js/main.js"></script>
</body>
</html>