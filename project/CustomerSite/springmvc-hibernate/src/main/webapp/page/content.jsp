<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Content</title>
</head>
<body>
	<section>
	<div class="container">
		<div class="row">
			
			<div class="col-sm-3">
				<div class="left-sidebar">
				
						

<!-- 					<div class="price-range">price-range -->
<!-- 							<h2>Phạm vi giá</h2> -->
<!-- 							<div class="well"> -->
<!-- 								 <input type="text" class="span2" value="" data-slider-min="0" data-slider-max="100000" data-slider-step="500" data-slider-value="[50000,100000]" id="sl2" ><br /> -->
<!-- 								 <b>0</b> <b class="pull-right">100000</b> -->
<!-- 							</div> -->
<!-- 						</div> -->
					<!--/price-range-->
	
					<div class="shipping text-center">
						<!--shipping-->
						<img src="../resources/page/images/home/shipping.jpg" alt="" />
					</div>
					<!--/shipping-->
					<div class="shipping text-center">
						<!--shipping-->
						<img src="https://nld.mediacdn.vn/2018/12/24/13-sach-tranh-1545662011931962968913.jpg" alt="" />
					</div>
					<div class="shipping text-center">
						<!--shipping-->
						<img src="https://www.vinabook.com/images/thumbnails/promo/802x480/336222_desktopkxd3-al.jpg" alt="" />
					</div>
					
				</div>
			</div>

			<div class="col-sm-9 padding-right">
				<div class="features_items">
					<!--features_items-->
					<h2 class="title text-center">Hàng mới</h2>
					<div>
						<c:forEach var="i" items="${book}">
							<div class="col-sm-4">
								
									<div class="product-image-wrapper" >
										<div class="single-products">
											<div class="productinfo text-center" style="width: 210px;height: 350px">
												<a href="../controller/product?id=${i.id }">
													<img src="../resources/page/images/home/${i.imgage}" alt="" style="width: 207px;height: 230px"/>
													<h2>${i.price}</h2>
													<p>${i.name }</p>
												</a>
												<c:if test="${mode == 'LOGIN' }">
													<a type="button" href="../cartcontroller/addcart?id=${i.id}" class="btn btn-default add-to-cart"><i
															class="fa fa-shopping-cart"></i>Add to cart</a>
												</c:if>
												<c:if test="${mode == null || mode == 'LOGOUT' }">
													<a type="button" href="../controller/logins" class="btn btn-default add-to-cart"><i
															class="fa fa-shopping-cart"></i>Add to cart</a>
												</c:if>
											</div>
											<div class="product-overlay">
												<div class="overlay-content">
													<a href="../controller/product?id=${i.id }">
														<img src="../resources/page/images/home/${i.imgage}" alt="" style="width: 207px;height: 230px"/>
														<h2>${i.price}</h2>
														<p>${i.name }</p>
													</a>
													<c:if test="${mode == 'LOGIN' }">
														<a type="button" href="../cartcontroller/addcart?id=${i.id}" class="btn btn-default add-to-cart"><i
															class="fa fa-shopping-cart"></i>Add to cart</a>
													</c:if>
													<c:if test="${mode == null}">
														<a type="button" href="../controller/logins" class="btn btn-default add-to-cart"><i
															class="fa fa-shopping-cart"></i>Add to cart</a>
													</c:if>
					
												</div>
											</div>
										</div>
									</div>
								
							</div>
						</c:forEach>
					</div>

					<div >
						<!--category-tab-->
						<div class="col-sm-12">
							<ul class="nav nav-tabs">
								<li class="active"><a href="#tshirt" data-toggle="tab">BOOK NEW</a></li>
								<li><a href="#blazers" data-toggle="tab">HOT</a></li>
								<li><a href="#poloshirt" data-toggle="tab">OTHER</a></li>
							</ul>
						
							<div class="tab-content">
								<div class="tab-pane fade active in" id="tshirt">
									<c:forEach items="${book}" var="book">
										<c:if test="${book.status==0 }">
											<div class="col-sm-3">
												<div class="product-image-wrapper">
													<div class="single-products">
														<div class="productinfo text-center">
															<img src="../resources/page/images/home/${book.imgage}" alt="" width="200px" height="200px" />
															<h2>${book.price }</h2>
															<p>${book.name }</p>
															<a href="../cartcontroller/addcart?id=${book.id}" class="btn btn-default add-to-cart"><i
																class="fa fa-shopping-cart"></i>Add to cart</a>
														</div>
			
													</div>
												</div>
											</div>
										</c:if>
									</c:forEach>
									
									</div>
	
									<div class="tab-pane fade" id="blazers">
										<c:forEach items="${book}" var="book">
											<c:if test="${book.status==1 }">
												<div class="col-sm-3">
													<div class="product-image-wrapper">
														<div class="single-products">
															<div class="productinfo text-center">
																<img src="../resources/page/images/home/${book.imgage}" alt="" />
																<h2>${book.price }</h2>
																<p>${book.name }</p>
																<a href="../cartcontroller/addcart?id=${book.id}" class="btn btn-default add-to-cart"><i
																	class="fa fa-shopping-cart"></i>Add to cart</a>
															</div>
				
														</div>
													</div>
												</div>
											</c:if>
										</c:forEach>
									</div>
		
									<div class="tab-pane fade" id="poloshirt">
										<c:forEach items="${book}" var="book">
											<c:if test="${book.status==2 }">
												<div class="col-sm-3">
													<div class="product-image-wrapper">
														<div class="single-products">
															<div class="productinfo text-center">
																<img src="../resources/page/images/home/${book.imgage}" alt="" />
																<h2>${book.price }</h2>
																<p>${book.name }</p>
																<a href="../cartcontroller/addcart?id=${book.id}" class="btn btn-default add-to-cart"><i
																	class="fa fa-shopping-cart"></i>Add to cart</a>
															</div>
				
														</div>
													</div>
												</div>
											</c:if>
										</c:forEach>
									</div>
	
								<!--/category-tab-->
								</div>
<!-- 								<div class="features_items"> -->
<%-- 									<c:forEach items="${genres }" var="genre"> --%>
<%-- 										<div class="mainbox-title">${genre.name}</div> --%>
<!-- 										<div id="MyCarousel" class="carousel slide" data-ride="carousel"> -->
<!-- 											<ol class="carousel-indicators"> -->
<!-- 												<li data-target="#MyCarousel" data-slide-to="0" class="active"></li> -->
<!-- 												<li data-target="#MyCarousel" data-slide-to="1"></li> -->
<!-- 												<li data-target="#MyCarousel" data-slide-to="2"></li> -->
<!-- 												<li data-target="#MyCarousel" data-slide-to="3"></li> -->
<!-- 												<li data-target="#MyCarousel" data-slide-to="4"></li> -->
<!-- 												<li data-target="#MyCarousel" data-slide-to="5"></li> -->
<!-- 											</ol> -->
	
<!-- 											<div class="carousel-inner" role="listbox"> -->
<%-- 												<c:forEach var="book" items="${book }"> --%>
<!-- 													<div class="item active"> -->
														
<!-- 													</div> -->
<%-- 												</c:forEach> --%>
<!-- 											</div> -->

<!-- 											<a class="left carousel-control" href="#MyCarousel" role="button" data-slide="prev"> -->
<!-- 												<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> -->
<!-- 												<span class="sr-only">previous</span> -->
<!-- 											</a> -->
<!-- 											<a class="right carousel-control" href="#MyCarousel" role="button" data-slide="next"> -->
<!-- 												<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span> -->
<!-- 												<span class="sr-only">next</span> -->
<!-- 											</a> -->
<!-- 										</div>	 -->
										
<%-- 									</c:forEach> --%>
<!-- 								</div> -->
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</body>
</html>