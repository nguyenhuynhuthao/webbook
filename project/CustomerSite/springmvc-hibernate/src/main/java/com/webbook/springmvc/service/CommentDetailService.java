package com.webbook.springmvc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.webbook.springmvc.dao.Dao;
import com.webbook.springmvc.entities.ConmemntDetail;

@Transactional
@Service
public class CommentDetailService {
	@Autowired
	private Dao<ConmemntDetail> dao;
	
	public List<ConmemntDetail> getAll()
	{
		return dao.getAll();
	}
	public ConmemntDetail get(Long id) 
	{
		return dao.get(id);
	}
	public ConmemntDetail add(ConmemntDetail t) 
	{
		return dao.add(t);
	}
	public Boolean update(ConmemntDetail t) {
		return dao.update(t);
	}
}
