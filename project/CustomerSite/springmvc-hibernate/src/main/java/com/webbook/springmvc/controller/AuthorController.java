package com.webbook.springmvc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.webbook.springmvc.entities.Author;
import com.webbook.springmvc.service.AuthorService;

@Controller
@RequestMapping(value = "/controller")
public class AuthorController {

	@Autowired
	private AuthorService authorService;

	@RequestMapping(value = "/authors", method = RequestMethod.GET)
	public ModelAndView getAllAuthors() {
		ModelAndView model = new ModelAndView();

		model.setViewName("authorList");
		model.addObject("authors", authorService.getAll());

		return model;
	}

	@RequestMapping(value = "/author", method = RequestMethod.GET)
	public ModelAndView getAuthorById(@RequestParam(name = "id") Long id) {
		ModelAndView model = new ModelAndView();

		model.setViewName("authorDetail");
		model.addObject("author", authorService.get(id));

		return model;
	}

}
