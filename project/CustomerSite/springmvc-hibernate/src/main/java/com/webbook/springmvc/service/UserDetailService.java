package com.webbook.springmvc.service;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.webbook.springmvc.dao.Dao;
import com.webbook.springmvc.dao.UserDao;
import com.webbook.springmvc.entities.User;
import com.webbook.springmvc.entities.UserDetails;

@Transactional
@Service
public class UserDetailService {	
		@Autowired
		Dao<UserDetails> UserDetailDAO;
		@Autowired
		private SessionFactory sessionFactory;
		
		public List<UserDetails> getAll(){
			return UserDetailDAO.getAll();
		}
		
		public UserDetails get(Long id){
			return UserDetailDAO.get(id);
		}
		
		public UserDetails add(UserDetails userdetails){
			return UserDetailDAO.add(userdetails);
		}
		
		public Boolean update(UserDetails userdetails){
			return UserDetailDAO.update(userdetails);
		}
		
		public Boolean delete(UserDetails userdetails){
			return UserDetailDAO.delete(userdetails);
		}
		
		public Boolean delete(Long id){
			return UserDetailDAO.delete(id);
		}
		
		public UserDetails getUserDetailByUserName(String fullName){
			UserDetails userdetails = null;
			Session session = sessionFactory.getCurrentSession();
			String hql = "from UserDetails where FULLNAME = :fullName";
			Query query = session.createQuery(hql);
			query.setParameter("fullName", fullName);
			if (query.list().size() >= 1)
				userdetails = (UserDetails) query.list().get(query.list().size()-1);
			return userdetails;
		}



}
