package com.webbook.springmvc.controller;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.spec.KeySpec;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;
import javax.servlet.http.HttpSession;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.webbook.springmvc.entities.Book;
import com.webbook.springmvc.entities.Cart1;
import com.webbook.springmvc.entities.Order;
import com.webbook.springmvc.entities.User;
import com.webbook.springmvc.entities.UserDetails;
import com.webbook.springmvc.service.BookService;
import com.webbook.springmvc.service.GenreService;
import com.webbook.springmvc.service.UserDetailService;
import com.webbook.springmvc.service.UserService;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

@Controller
@RequestMapping(value = "/controller")
public class UserController1 {

	@Autowired
	private UserService userService;
	
	@Autowired
	BookService bookService;
	
	@Autowired
	GenreService genreService;
	
	@RequestMapping(value = "/logins", method = RequestMethod.GET)
	public ModelAndView login(HttpSession session) {
		ModelAndView model = new ModelAndView();
		model.setViewName("login");
		model.addObject("User", new User());
		return model;
	}

	@RequestMapping(value = "/loginadmin", method = RequestMethod.GET)
	public ModelAndView Adminlogin(HttpSession session) {
		ModelAndView model = new ModelAndView();
		model.setViewName("AdminPage");
		model.addObject("User", new User());
		return model;
	}

	@RequestMapping(value = "/index", method = RequestMethod.GET)
	public ModelAndView index(HttpSession session) {
		ModelAndView model = new ModelAndView();
		model.setViewName("index");
		model.addObject("book", phantrang(bookService.getAll(), 8, 0));
		model.addObject("genres", genreService.getAll());
		if (session.getAttribute("username")!=null) {
			User user=(User)session.getAttribute("username");
			System.out.println(user);
			session.setAttribute("checkuser", checkuser(user));
		}
		return model;
	}
	
	private int checkuser(User user) {
		int trong=0;
		System.out.println(user);
		if (user.getUserDetails()!=null) {
			if (user.getUserDetails().getFullname()==null||user.getUserDetails().getFullname()=="") {
				trong++;
			}
			if (user.getUserDetails().getPhones()==null||user.getUserDetails().getPhones()=="") {
				trong++;
			}
			if (user.getUserDetails().getAddress()!=null) {
				if (user.getUserDetails().getAddress().getStreet()==null||user.getUserDetails().getAddress().getStreet()=="") {
					trong++;
				}
				if (user.getUserDetails().getAddress().getDistrict()==null||user.getUserDetails().getAddress().getDistrict()=="") {
					trong++;
				}
				if (user.getUserDetails().getAddress().getCity()==null||user.getUserDetails().getAddress().getCity()=="") {
					trong++;
				}
			}
			else
			{
				trong=trong+3;
			}
		}
		else {
			trong=5;
		}
		return trong;
	}
	private List<Book> phantrang(List<Book> list,int slsp,int vt)
	{
		int pointsl=0;
		int pointvt=0;
		int check=0;
		List<Book> books=new ArrayList<Book>();
		for (Book b : list) {
			if (pointvt==vt) {
				check=1;
			}
			if (check==1) {
				if (pointsl!=slsp) {
					books.add(b);
					pointsl++;
				}
				else {
					break;
				}
			}
		}
		return books;
	}

	@RequestMapping(value = "/adduser", method = RequestMethod.POST)
	public String regis(@RequestParam String name, @RequestParam String pass, @RequestParam String sspass,
			ModelMap mm) {
		ModelAndView model = new ModelAndView();
		User user = new User();
		if (name.equals("") || pass.equals("")) {
			mm.put("error1", "Chưa nhập đủ thông tin");
			return "login";
		} else {
			if (pass.equals(sspass)) {
				if (userService.getUserByUserName(name) == null) {
					String key = "01202342556";
					user.setName(name);
					user.setPassword(Encrypt(pass, key));
					user.setRole((long) 2);
					UserDetails userDetails=new UserDetails();
					user.setUserDetails(userDetails);
					userService.add(user);
					mm.put("error1", "Đăng ký thành công");
					return "login";
				} else {
					mm.put("error1", "Tài khoản đã tồn tại");
					return "login";
				}
			} else {
				mm.put("error1", "Mật khẩu không khớp");
				return "login";
			}
		}
	}
	@RequestMapping(value="/userdet", method=RequestMethod.GET)
	public ModelAndView userdetail(@RequestParam(value="id") Long id) 
	{
		ModelAndView model=new ModelAndView();
		model.setViewName("checkout");
		model.addObject("user", userService.get(id));
		model.addObject("thanhtoan", 0);
		return model;
	}
	@RequestMapping(value="/updateuser", method=RequestMethod.POST)
	public ModelAndView updateuser(@ModelAttribute User user,HttpSession session) 
	{
		userService.update(user);
		session.setAttribute("username", user);
		ModelAndView model=new ModelAndView();
		model.setViewName("checkout");
		return model;
	}
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String login(@RequestParam String name, @RequestParam String pass, HttpSession session, ModelMap mm) {
		ModelAndView model = new ModelAndView();
		User user = new User();
		List<UserDetails> userdetail = new ArrayList<UserDetails>();
		if (name.equals("") || pass.equals("")) {
			session.setAttribute("error", "Vui lòng nhập đủ thông tin");
			return "redirect:/controller/login";
		} else {
			if (userService.getUserByUserName(name) != null) {
				user = userService.getUserByUserName(name);
				String key = "01202342556";
				if (Decrypt(user.getPassword(), key).equals(pass)) {
					if (user.getRole()==(long)2) {
					
					session.setAttribute("mode", "LOGIN");
					session.setAttribute("username", user);
//					for (User u: userService.getAll()){
//						userdetail.add(userDetailService.get(u.getId()));
//					}
					mm.addAttribute("userdetail", userdetail);
					System.out.println(userdetail);
					session.setAttribute("error", "Đăng nhập thành công");
					return "redirect:/controller/index";
					
					}
					else {
						session.setAttribute("error", "Tài Khoản của bạn đã bị khóa");
						return "redirect:/controller/logins";
					}
				} else {
					session.setAttribute("error", "Sai mật khẩu");
					return "redirect:/controller/logins";
				}

				// if(user.getPassword().equals(pass)){
				// System.out.println("Ãƒâ€žÃ¯Â¿Â½Ãƒâ€žÃ†â€™ng nhÃƒÂ¡Ã‚ÂºÃ‚Â­p
				// thÃƒÆ’Ã‚Â nh cÃƒÆ’Ã‚Â´ng");
				// return "index";
				// }else{
				// System.out.println("sai pass");
				// return "login";
				// }
			} else {
				session.setAttribute("error", "Tài khoản không tồn tại");
				return "redirect:/controller/logins";
			}
		}
	}
	
	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logout(HttpSession session) {
		session.removeAttribute("username");
		session.removeAttribute("mode");
		return "redirect:index";
	}

	/// ma hoa triple DES
	public static String Encrypt(String plainText, String key) {
		try {
			byte[] arrayBytes = getValidKey(key);
			KeySpec ks = new DESedeKeySpec(arrayBytes);
			SecretKeyFactory skf = SecretKeyFactory.getInstance("DESede");
			Cipher cipher = Cipher.getInstance("DESede");
			SecretKey seckey = skf.generateSecret(ks);
			// cái đso chắc a bị lỗi biến môi trường thôi à kh sao hết á
			cipher.init(Cipher.ENCRYPT_MODE, seckey);
			byte[] plainByte = plainText.getBytes("UTF8");
			byte[] encryptedByte = cipher.doFinal(plainByte);
			BASE64Encoder encoder = new BASE64Encoder();
			return encoder.encode(encryptedByte);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	// giai ma 3des
	public static String Decrypt(String encryptData, String key) {
		try {
			byte[] arrayBytes = getValidKey(key);
			KeySpec ks = new DESedeKeySpec(arrayBytes);
			SecretKeyFactory skf = SecretKeyFactory.getInstance("DESede");
			Cipher cipher = Cipher.getInstance("DESede");
			SecretKey seckey = skf.generateSecret(ks);
			//
			cipher.init(Cipher.DECRYPT_MODE, seckey);
			BASE64Decoder decode = new BASE64Decoder();
			byte[] decodeBuffer = decode.decodeBuffer(encryptData);
			byte[] encryptByte = decodeBuffer;
			byte[] plainByte = cipher.doFinal(encryptByte);
			return new String(plainByte);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private static byte[] getValidKey(String key) {
		try {
			String sTemp = hash(key).substring(0, 24);
			return sTemp.getBytes();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/// ma hoa md5
	private static String convertToHex(byte[] data) {
		StringBuffer buf = new StringBuffer();
		for (int i = 0; i < data.length; i++) {
			int halfbyte = (data[i] >>> 4) & 0x0F;
			int two_halfs = 0;
			do {
				if ((0 <= halfbyte) && (halfbyte <= 9))
					buf.append((char) ('0' + halfbyte));
				else
					buf.append((char) ('a' + (halfbyte - 10)));
				halfbyte = data[i] & 0x0F;
			} while (two_halfs++ < 1);
		}
		return buf.toString();
	}

	private static String hash(String text) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		MessageDigest md;
		md = MessageDigest.getInstance("MD5");
		byte[] md5hash = new byte[32];
		md.update(text.getBytes("iso-8859-1"), 0, text.length());
		md5hash = md.digest();
		return convertToHex(md5hash);
	}
	/// het ma hoa md5
}
