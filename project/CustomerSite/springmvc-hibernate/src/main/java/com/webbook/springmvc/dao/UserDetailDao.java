package com.webbook.springmvc.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.webbook.springmvc.entities.User;
import com.webbook.springmvc.entities.UserDetails;

@Repository
public class UserDetailDao extends Dao<UserDetails>{
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public List<UserDetails> getAll() {
		Session session = this.sessionFactory.getCurrentSession();
		List<UserDetails> userdetails = session.createQuery("from UserDetails").list();
		return userdetails;
	}
	
	@Override
	public UserDetails get(Long id) {
		Session session = this.sessionFactory.getCurrentSession();
		return (UserDetails) session.get(UserDetails.class, new Long(id));
	}

	@Override
	public UserDetails add(UserDetails userdetails) {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(userdetails);
		return userdetails;
	}

	@Override
	public Boolean update(UserDetails userdetails) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			session.update(userdetails);
			return Boolean.TRUE;
		} catch (Exception e) {
			return Boolean.FALSE;
		}
	}

	@Override
	public Boolean delete(UserDetails userdetails) {
		Session session = this.sessionFactory.getCurrentSession();
		if(null != userdetails) {
			try {
				session.delete(userdetails);
				return Boolean.TRUE;
			} catch (Exception e) {
				return Boolean.FALSE;
			}
		}
		return Boolean.FALSE;
	}

	@Override
	public Boolean delete(Long id) {
		Session session = this.sessionFactory.getCurrentSession();
		UserDetails userdetails = (UserDetails) session.load(UserDetails.class, new Long(id));
		if (null != userdetails) {
			session.delete(userdetails);
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}

}
