package com.webbook.springmvc.entities;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "user", catalog = "webbook")
public class User implements java.io.Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ID_USER", unique = true, nullable = false)
	private Long id;

	@Column(name = "NAME", length = 50)
	private String name;

	@Column(name = "ROLE", length = 50)
	private Long role;

	@Column(name = "PASSWORD", length = 50)
	private String password;

	@OneToMany( fetch=FetchType.EAGER, mappedBy = "user" )
	private Set<Cart1> carts = new HashSet<Cart1>();
	
	@OneToOne(fetch=FetchType.EAGER,cascade=CascadeType.ALL)
	@PrimaryKeyJoinColumn
	private UserDetails userDetails;
	
	public User() {
	}

	public User(String name, Long role, String password, UserDetails userDetails) {
		this.name = name;
		this.role = role;
		this.password = password;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getRole() {
		return role;
	}

	public void setRole(Long role) {
		this.role = role;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	
	
	public Set<Cart1> getCarts() {
		return carts;
	}

	public void setCarts(Set<Cart1> carts) {
		this.carts = carts;
	}

	public UserDetails getUserDetails() {
		return userDetails;
	}

	public void setUserDetails(UserDetails userDetails) {
		this.userDetails = userDetails;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", name=" + name + ", role=" + role + ", password=" + password + ", carts=" + carts
				+ ", userDetails=" + userDetails + "]";
	}
	
}
