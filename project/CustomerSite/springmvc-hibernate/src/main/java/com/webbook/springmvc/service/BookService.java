package com.webbook.springmvc.service;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.webbook.springmvc.dao.BookDao;
import com.webbook.springmvc.dao.Dao;
import com.webbook.springmvc.entities.Book;
import com.webbook.springmvc.entities.Cart1;
import com.webbook.springmvc.entities.User;

@Transactional
@Service
public class BookService {
	
	@Autowired
	BookDao BookDAO;
	@Autowired
	private SessionFactory sessionFactory;
	
	public List<Book> getAll(){
		return BookDAO.getAll();
	}
	
	public Book get(Long id){
		return BookDAO.get(id);
	}
	
	public Book add(Book t){
		return BookDAO.add(t);
	}
	
	public Boolean update(Book t){
		return BookDAO.update(t);
	}
	
	public Boolean delete(Book t){
		return BookDAO.delete(t);
	}
	
	public Boolean delete(Long id){
		return BookDAO.delete(id);
	}
	public Book getProductByIdBook(Long IdBook){
		Book book = null;
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Book where ID_BOOK = :IdBook";
		Query query = session.createQuery(hql);
		query.setParameter("IdBook", IdBook);
		if (query.list().size() >= 1)
			book = (Book) query.list().get(query.list().size()-1);
		return book;
	}
	
	public List<Book> getBookBybookId(Long bookId){
		List<Book> book = null;
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Book where id = :bookId";
		Query query = session.createQuery(hql);
		query.setParameter("bookId", bookId);
		if (query.list().size() >= 1)
			book = (List<Book>) query.list();
		return book;
	}
	public List<Book> getBookByGenre(Long id)
	{
		return BookDAO.getByGenre(id);
	}
	public List<Book> search(String chuoi) 
	{
		return BookDAO.search(chuoi);
	}
}