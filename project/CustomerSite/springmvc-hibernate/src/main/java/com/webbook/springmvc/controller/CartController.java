package com.webbook.springmvc.controller;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.webbook.springmvc.entities.Book;
import com.webbook.springmvc.entities.Cart1;
import com.webbook.springmvc.entities.Order;
import com.webbook.springmvc.entities.OrderDetails;
import com.webbook.springmvc.entities.User;
import com.webbook.springmvc.service.BookService;
import com.webbook.springmvc.service.Cart1Service;
import com.webbook.springmvc.service.OrderDetailsService;
import com.webbook.springmvc.service.OrderService;
import com.webbook.springmvc.service.UserService;

@Controller
@RequestMapping(value = "/cartcontroller")
public class CartController {
	@Autowired
	private Cart1Service cartServices;

	@Autowired
	private BookService bookServices;
	
	@Autowired
	private UserService userServices;
	private Book book;
	
	@Autowired
	private OrderService orderServices;
	
	@Autowired
	private OrderDetailsService orderdetailsservice;

	@RequestMapping(value = "/addcart", method = RequestMethod.GET)
	public String addcart(@RequestParam("id") Long id, HttpSession httpSession, ModelMap mm) {
		ModelAndView model = new ModelAndView();
		model.setViewName("login");
		User user = (User) httpSession.getAttribute("username");
		
		boolean check = false;
		Set<Cart1> cart1s=user.getCarts();
		System.out.println("day la cart: "+cart1s);
		for (Cart1 c : cart1s) {
		//	System.out.println("id="+c.getBook().id+" "+	id);
			Long a=c.getBook().id;
			Long b=id;
//			System.out.println(String.valueOf(a)+" "+String.valueOf(b));
			if (String.valueOf(a).equalsIgnoreCase(String.valueOf(b)) ) {
				Cart1 cart = c;
				cart.setNum(cart.getNum() + 1);
				
				check = true;
				
			
			}
		}
		if (check==false) {
			Cart1 cart = new Cart1();
			cart.setBook(bookServices.get(id));
			cart.setNum(1);
			cart.setUser(user);
			cart1s.add(cart);
		}

		user.setCarts(cart1s);
		userServices.update(user);
		httpSession.setAttribute("username", user);
		return "redirect:/cartcontroller/cart";
	}
	
	@RequestMapping(value = "/cart", method = RequestMethod.GET)
	public ModelAndView getcart(ModelMap mm,HttpSession httpSession) {
		ModelAndView model=new ModelAndView();
		User user = (User) httpSession.getAttribute("username");
		//System.out.println("day la user:"+user);
		Set<Cart1> cart = user.getCarts();
		//System.out.println(cart);
		model.setViewName("cart");
		model.addObject("carts", cart);
		model.addObject("price", getPrice(cart) );
		return model;
	}
	
	@RequestMapping(value = "/removecart", method = RequestMethod.GET)
	public String removedcart(@RequestParam("id") Long id, HttpSession httpSession, ModelMap mm){
		User user = (User) httpSession.getAttribute("username");
		Set<Cart1> list=user.getCarts();
		for (Cart1 c : list) {
			if (String.valueOf(id).equalsIgnoreCase(String.valueOf(c.getId()))) {
				System.out.println("da vao");
				list.remove(c);
				break;
			}
			
		}
		user.setCarts(list);
		userServices.update(user);
		httpSession.setAttribute("username", user);
		return "redirect:/cartcontroller/cart";
	}
	
//	@RequestMapping(value = "/addorder/{id}", method = RequestMethod.GET)
//	public String addoder(@PathVariable("id") Long id, HttpSession httpSession, ModelMap mm){
//		User user = (User) httpSession.getAttribute("username");
//		List<Order> order = new ArrayList<Order>();
//		for (Cart1 c : cartServices.getAll()) {
//			if (c.getBookId().getId() == id && c.getUserId().getId() == user.getId()) {
//				orderServices.add(cart);
//			}
//		}
//		return "redirect:/cartcontroller/cart";
//	}
	
	@RequestMapping(value = "/vieworder", method = RequestMethod.GET)
	public String viewOrder(ModelMap mm,HttpSession httpSession) {
		User user = (User) httpSession.getAttribute("username");
		Set<Cart1> cart = user.getCarts();
		mm.addAttribute("user", user);
		mm.addAttribute("cart", cart);		
		mm.addAttribute("thanhtoan", 1);	
		return "checkout";
	}
	
	@RequestMapping(value = "/deleted/{id}", method = RequestMethod.GET)
	public String deletedcart(@PathVariable("id") Long id, HttpSession httpSession, ModelMap mm) {

		User user = (User) httpSession.getAttribute("username");
		Set<Cart1>cart1s =user.getCarts();
		for (Cart1 c : cart1s) {
			if (String.valueOf(c.getBook().getId()).equalsIgnoreCase(String.valueOf(id))) {
				Cart1 cart = c;
				if(cart.getNum()>1){
					cart.setNum(cart.getNum() - 1);
					break;
				}
				else{
					mm.put("tb", "So luong khong duoc bang 0");
					break;
				}
				
			}
		}
		user.setCarts(cart1s);
		userServices.update(user);
		httpSession.setAttribute("username", user);
		return "redirect:/cartcontroller/cart";
	}
	private int getPrice(Set<Cart1> list) 
	{
		int price=0;
		for (Cart1 c : list) {
			price+=c.getBook().price*c.getNum();
		}
		return price;
	}
	@RequestMapping(value = "/orders", method = RequestMethod.GET)
	public ModelAndView viewOrders(ModelMap mm,HttpSession httpSession) {
		ModelAndView modelAndView=new ModelAndView();
		User user = (User) httpSession.getAttribute("username");
		modelAndView.setViewName("Order");
		modelAndView.addObject("orders", checkod(user.getId()));
		modelAndView.addObject("orderdetail",getord(user.getId()) );
		modelAndView.addObject("mode", "VIEW");
		return modelAndView;
	}
	
	@RequestMapping(value = "/viewdt", method = RequestMethod.GET)
	public ModelAndView viewOrderdetail(@RequestParam(value="id") Long id,HttpSession httpSession) {
		ModelAndView modelAndView=new ModelAndView();
		User user = (User) httpSession.getAttribute("username");
		modelAndView.setViewName("Order");
		modelAndView.addObject("orders", orderServices.get(id));
		modelAndView.addObject("orderdetail",getord(id) );
		modelAndView.addObject("mode", "VIEWDT");
		modelAndView.addObject("price", getpri(id));
		return modelAndView;
	}
	private List<Order> checkod(Long id) 
	{
		List<Order>list	=new ArrayList<>();
		for (Order order : orderServices.getAll()) {
			if (order.getUser().getId()==id) {
				list.add(order);
			}
		}
		return list;
	}
	private List<OrderDetails> getord(Long id) {
		List<OrderDetails>list=new ArrayList<>();
		
		
		for (OrderDetails ord : orderdetailsservice.getAll()) {
			if (ord.getOrder_id().getId_order()==id) {
				list.add(ord);
			}
				
			}
		
		System.out.println(list);
		return list;
	}
	private int getpri(Long id) {
		List<OrderDetails>list=new ArrayList<>();
		
		int pri=0;
		for (OrderDetails ord : orderdetailsservice.getAll()) {
			if (ord.getOrder_id().getId_order()==id) {
				pri+=ord.total_price;
			}
				
			}
		
		System.out.println(list);
		return pri;
	}
	@RequestMapping(value = "/order", method = RequestMethod.GET)
	public String getOrder(ModelMap mm,HttpSession httpSession){
		OrderDetails orderdetails = new OrderDetails();
		Order order = new Order();
		User user = (User) httpSession.getAttribute("username");
		//System.out.println(user.getId());
		Set<Cart1> cart = user.getCarts();
		long millis=System.currentTimeMillis();  
		java.sql.Date date=new java.sql.Date(millis);
		order.setUser(user);
		order.setDate_purchse(date);
		order.setStatus(0);
		orderServices.add(order);
		for (Cart1 c: cart){
			
				orderdetails.setQuantity(c.getNum());
				orderdetails.setBook(c.getBook());
				orderdetails.setTotal_price((int) (c.getNum()*c.getBook().price));
				orderdetails.setOrder_id(order);
				orderdetailsservice.add(orderdetails);
		}
		Set<Cart1> carts=new HashSet<Cart1>();
		user.setCarts(carts);
		userServices.update(user);
		httpSession.setAttribute("username", user);
		return "redirect:/cartcontroller/viewdt?id="+order.getId_order();
	}
	
	@RequestMapping(value="/cancel", method=RequestMethod.GET)
	public String canelorder(@RequestParam(value="id")Long id) 
	{
	 	Order order= orderServices.get(id);
	 	order.setStatus(3);
	 	orderServices.update(order);
	 	return "redirect:/cartcontroller/viewdt?id="+order.getId_order();
	}
}
