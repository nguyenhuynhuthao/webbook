package com.webbook.springmvc.service;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.webbook.springmvc.dao.Dao;
import com.webbook.springmvc.dao.UserDao;
import com.webbook.springmvc.entities.Cart1;
import com.webbook.springmvc.entities.User;
import com.webbook.springmvc.entities.UserDetails;

@Transactional
@Service
public class UserService {
	
	@Autowired
	Dao<User> UserDAO;
	@Autowired
	private SessionFactory sessionFactory;
	
	public List<User> getAll(){
		return UserDAO.getAll();
	}
	
	public User get(Long id){
		return UserDAO.get(id);
	}
	
	public User add(User user){
		return UserDAO.add(user);
	}
	
	public Boolean update(User user){
		return UserDAO.update(user);
	}
	
	public Boolean delete(User user){
		return UserDAO.delete(user);
	}
	
	public Boolean delete(Long id){
		return UserDAO.delete(id);
	}
	public User login(String name_user, String password){
		return UserDAO.login(name_user, password);
		
	}
	public User getUserByUserName(String userName){
		User user = null;
		Session session = sessionFactory.getCurrentSession();
		String hql = "from User where NAME = :userName";
		Query query = session.createQuery(hql);
		query.setParameter("userName", userName);
		if (query.list().size() >= 1)
			user = (User) query.list().get(query.list().size()-1);
		return user;
	}
	
	public List<Cart1> getCartByuserid(Long userId){
		List<Cart1> cart = null;
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Cart1 where userId = :userId";
		Query query = session.createQuery(hql);
		query.setParameter("userId", userId);
		if (query.list().size() >= 1)
			cart = (List<Cart1>) query.list();
		return cart;
	}
	 
	public List<UserDetails> getUserDetailByUserId(Long userId){
		List<UserDetails> userdetail = null;
		Session session = sessionFactory.getCurrentSession();
		String hql = "from User where ID = :userId";
		Query query = session.createQuery(hql);
		query.setParameter("userId", userId);
		if (query.list().size() >= 1)
			userdetail = (List<UserDetails>) query.list();
		return userdetail;
	}
}
