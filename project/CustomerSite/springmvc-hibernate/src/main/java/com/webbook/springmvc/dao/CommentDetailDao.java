package com.webbook.springmvc.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.webbook.springmvc.entities.Comment;
import com.webbook.springmvc.entities.ConmemntDetail;
@Repository
public class CommentDetailDao extends Dao<ConmemntDetail> {

	@Autowired
	private SessionFactory sessionFactory;
	
	
	@Override
	public List<ConmemntDetail> getAll() {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		return session.createQuery("from ConmemntDetail").list();
	}

	@Override
	public ConmemntDetail get(Long id) {
		Session session = this.sessionFactory.getCurrentSession();
		return (ConmemntDetail) session.get(ConmemntDetail.class, id);
	}

	@Override
	public ConmemntDetail add(ConmemntDetail t) {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(t);
		return t;
	}

	@Override
	public Boolean update(ConmemntDetail t) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			session.update(t);
			return Boolean.TRUE;
		} catch (Exception e) {
			return Boolean.FALSE;				
		}
	}

	@Override
	public Boolean delete(ConmemntDetail t) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Boolean delete(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

}
