package com.webbook.springmvc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.webbook.springmvc.dao.Dao;
import com.webbook.springmvc.entities.Comment;

@Transactional
@Service
public class CommentService {
	@Autowired
	private Dao<Comment> dao;
	
	public List<Comment> getAll()
	{
		return dao.getAll();
	}
	public Comment get(Long id) 
	{
		return dao.get(id);
	}
	public Comment add(Comment t) 
	{
		return dao.add(t);
	}
	public Boolean update(Comment t) {
		return dao.update(t);
	}
}
