package com.webbook.springmvc.controller;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.webbook.springmvc.entities.Book;
import com.webbook.springmvc.entities.Comment;
import com.webbook.springmvc.entities.ConmemntDetail;
import com.webbook.springmvc.entities.Genre;
import com.webbook.springmvc.service.BookService;
import com.webbook.springmvc.service.CommentDetailService;
import com.webbook.springmvc.service.CommentService;
import com.webbook.springmvc.service.GenreService;
import com.webbook.springmvc.service.UserService;

@Controller
@RequestMapping(value = "/controller")
public class GenreController {

	@Autowired
	private GenreService genreService;
	@Autowired
	private BookService bookService;
	
	@Autowired 
	private UserService userService;
	
	@Autowired
	private CommentService commentService;

	@Autowired
	private CommentDetailService cDetailService;
	@RequestMapping(value = "/genres", method = RequestMethod.GET)
	public ModelAndView getAllGenre(HttpSession session) {
		ModelAndView model = new ModelAndView();

		model.setViewName("product");
		model.addObject("genre", genreService.getAll());
		model.addObject("book",phantrang(bookService.getAll(),9,0));
		if (bookService.getAll().size()%9==0) {
			session.setAttribute("slpage",bookService.getAll().size()/9);
		}
		else {
			session.setAttribute("slpage",bookService.getAll().size()/9+1);
		}
		session.setAttribute("page", 1);
		session.setAttribute("pagegenre", 0);
		return model;
	}
	@RequestMapping(value = "/genre", method = RequestMethod.GET)
	public ModelAndView getAllGenre(@RequestParam(value="page")Integer page,HttpSession session) {
		ModelAndView model = new ModelAndView();

		model.setViewName("product");
		model.addObject("genre", genreService.getAll());
		model.addObject("book",phantrang(bookService.getAll(),9,(page-1)*9));
		System.out.println(phantrang(bookService.getAll(),9,(page-1)*9));
		System.out.println((page-1)*9);
		session.setAttribute("page", page);
		return model;
	}
	private List<Book> phantrang(List<Book> list,Integer slsp,Integer vt)
	{
		Integer pointsl=0;
		Integer pointvt=0;
		int check=0;
		List<Book> books=new ArrayList<Book>();
		for (Book b : list) {
		//	System.out.println(pointsl+" "+pointvt);
			if (pointvt!=vt) {
				pointvt++;
			}
			else {
				check=1;
			}
			if (check==1) {
				if (pointsl!=slsp) {
				//	System.out.println("da cao day");
					books.add(b);
					pointsl++;
				}
				else {
					break;
				}
			}
		}
		return books;
	}
	@RequestMapping(value="/product",method=RequestMethod.GET)
	public ModelAndView getbookbyid(@RequestParam(value="id") Long id,HttpSession session) {
		ModelAndView model=new ModelAndView();
		model.setViewName("productDetail");
		
		model.addObject("product", bookService.get(id));
		model.addObject("bookli", phantrang(bookService.getBookByGenre(bookService.get(id).genre_id.getId()), 4, 0));
		model.addObject("comments", getCommentByBook( commentService.getAll(), id));
		model.addObject("comdt", getCommentDT(getCommentByBook( commentService.getAll(), id), cDetailService.getAll()));
		model.addObject("tongbl", getCommentByBook(commentService.getAll(), id).size());
		System.out.println(bookService.getBookByGenre(id));
		return model;
	}
	
	private List<Comment> getCommentByBook(List<Comment> set,Long id) {
		List<Comment> set2=new ArrayList<>();
		for (Comment comment : set) {
				if (comment.getBook().getId()==id) {
				set2.add(comment);
			}
		}
		return set2;
	}
	private List<ConmemntDetail> getCommentDT(List<Comment> set,List<ConmemntDetail> cDetails) {
		List<ConmemntDetail> set2=new ArrayList<>();
		for (Comment comment : set) {
			for (ConmemntDetail Detail : cDetails) {
				if (comment.getId_customer()==Detail.getComment().getId_customer()) {
					set2.add(Detail);
				}
			}
		}
		return set2;
	}
	
	@RequestMapping(value="search",method=RequestMethod.GET)
	public ModelAndView Search(@RequestParam(value="search")String search) {
		ModelAndView model=new ModelAndView();
		model.setViewName("product");
		model.addObject("book", bookService.search(search));
		model.addObject("genre", genreService.getAll());
		return model;
		
	}
	@RequestMapping(value = "/getgenres", method = RequestMethod.GET)
	public ModelAndView getBookByGenre(@RequestParam(value="id")Long id, HttpSession session) {
		ModelAndView model = new ModelAndView();
		model.setViewName("product");
		model.addObject("genre", genreService.getAll());
		if (bookService.getBookByGenre(id).size()>9) {
			model.addObject("book",phantrang(bookService.getBookByGenre(id),9,0));
			if (bookService.getAll().size()%9==0) {
				session.setAttribute("slpagegenre",bookService.getBookByGenre(id).size()/9);
				
			}
			else {
				session.setAttribute("slpagegenre",bookService.getBookByGenre(id).size()/9+1);
				session.setAttribute("idgenre", id);
			}
			session.setAttribute("pagegenre", 1);
			
		}
		else
		{
			model.addObject("book", bookService.getBookByGenre(id));
		}
		session.setAttribute("page", 0);
		return model;
	}
	
	@RequestMapping(value = "/getgenre", method = RequestMethod.GET)
	public ModelAndView getBookByGenrepage(@RequestParam(value="id")Long id,@RequestParam(value="pagegenre")Integer pagegenre, HttpSession session) {
		ModelAndView model = new ModelAndView();
		model.setViewName("product");
		model.addObject("genre", genreService.getAll());
		
			model.addObject("book",phantrang(bookService.getBookByGenre(id),9,(pagegenre-1)*9));
			session.setAttribute("pagegenre", pagegenre);
		
		return model;
	}
	@RequestMapping(value="/comment",method=RequestMethod.GET)
	public String GetComment(@RequestParam(value="message")String message,@RequestParam(value="user")Long user,@RequestParam(value="bookid")Long book) {
		Comment comment=new Comment();
		comment.setBook(bookService.get(book));
		comment.setUser(userService.get(user));
		ConmemntDetail cDetail=new ConmemntDetail();
		cDetail.setTinnhan(message);
		cDetail.setFunction(1);
		cDetail.setComment(comment);
		cDetailService.add(cDetail);
		return "redirect:/controller/product?id="+book;
	}
}