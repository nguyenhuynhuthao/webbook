package com.webbook.springmvc.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.webbook.springmvc.entities.Publisher;

@Repository
public class PublisherDao extends Dao<Publisher>{

		@Autowired
		private SessionFactory sessionFactory;
		
		@Override
		public List<Publisher> getAll() {
			Session session = this.sessionFactory.getCurrentSession();
			return session.createQuery("from Publisher").list();
		}
		@Override
		public Publisher get(Long id) {
			Session session = this.sessionFactory.getCurrentSession();
			return (Publisher) session.get(Publisher.class, new Long(id));
		}
		@Override
		public Publisher add(Publisher publisher) {
			Session session = this.sessionFactory.getCurrentSession();
			session.save(publisher);
			return publisher;
		}
		@Override
		public Boolean update(Publisher publisher) {
			Session session = this.sessionFactory.getCurrentSession();
			try {
				session.update(publisher);
				return Boolean.TRUE;
			} catch (Exception e) {
				return Boolean.FALSE;
			}
		}
		
		@Override
		public Boolean delete(Publisher publisher) {
			Session session = this.sessionFactory.getCurrentSession();
			if (null != publisher) {
				try {
					session.delete(publisher);
					return Boolean.TRUE;
				} catch (Exception e) {
					return Boolean.FALSE;
				}
			}
			return Boolean.FALSE;
		}
		@Override
		public Boolean delete(Long id) {
			Session session = this.sessionFactory.getCurrentSession();
			Publisher publisher = (Publisher) session.load(Publisher.class, new Long(id));
			if (null != publisher) {
				session.delete(publisher);
				return Boolean.TRUE;
			}
			return Boolean.FALSE;
		}
}
