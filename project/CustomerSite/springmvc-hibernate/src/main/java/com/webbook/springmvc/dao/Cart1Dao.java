package com.webbook.springmvc.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.webbook.springmvc.entities.Cart1;

@Repository
public class Cart1Dao extends Dao<Cart1>{
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public List<Cart1> getAll() {
		Session session = this.sessionFactory.getCurrentSession();
		return session.createQuery("from Cart1").list();
	}
	
	@Override
	public Cart1 get(Long id) {
		Session session = this.sessionFactory.getCurrentSession();
		return (Cart1) session.get(Cart1.class, new Long(id));
	}

	@Override
	public Cart1 add(Cart1 cart1) {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(cart1);
		return cart1;
	}

	@Override
	public Boolean update(Cart1 cart1) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			session.update(cart1);
			return Boolean.TRUE;
		} catch (Exception e) {
			return Boolean.FALSE;				
		}
	}

	@Override
	public Boolean delete(Cart1 cart1) {
		Session session = this.sessionFactory.getCurrentSession();
		if (null != cart1) {
			try {
				session.delete(cart1);
				return Boolean.TRUE;
			} catch (Exception e) {
				return Boolean.FALSE;
			}
		}
		return Boolean.FALSE;
	}

	@Override
	public Boolean delete(Long id) {
		Session session = this.sessionFactory.getCurrentSession();
		Cart1 cart1 = (Cart1) session.load(Cart1.class, new Long(id));
		if (null != cart1) {
			session.delete(cart1);
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}
	public List<Cart1> getCart(Long id) {
		Session session = this.sessionFactory.getCurrentSession();
		Query query=session.createQuery("from Cart1 WhERE userId = :key");
		query.setParameter("key", id);
		List<Cart1> list=query.list();
		return list;
	}
}
