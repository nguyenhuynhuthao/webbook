package com.webbook.springmvc.entities;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "commentdetail" , catalog = "webbook")
public class ConmemntDetail implements java.io.Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Long id;
	
	@ManyToOne(fetch=FetchType.EAGER,cascade=CascadeType.ALL)
	@JoinColumn(name="comment_id",nullable = false)
	private Comment comment;
	
	@Column(name="function")
	private int function;
	
	@Column(name="tinnhan",length=100)
	private String tinnhan;
	
	public ConmemntDetail(Comment comment, int function, String tinnhan) {
		this.comment = comment;
		this.function = function;
		this.tinnhan = tinnhan;
	}
	
	public ConmemntDetail() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "ConmemntDetail [comment=" + comment + ", function=" + function + ", tinnhan=" + tinnhan + "]";
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Comment getComment() {
		return comment;
	}

	public void setComment(Comment comment) {
		this.comment = comment;
	}

	public int getFunction() {
		return function;
	}

	public void setFunction(int function) {
		this.function = function;
	}

	public String getTinnhan() {
		return tinnhan;
	}

	public void setTinnhan(String tinnhan) {
		this.tinnhan = tinnhan;
	}
	
}
