package com.webbook.springmvc.dao;

import java.util.List;

import com.webbook.springmvc.entities.User;

public abstract class Dao<T> {
	public abstract List<T> getAll();

	public abstract T get(Long id);

	public abstract T add(T t);

	public abstract Boolean update(T t);

	public abstract Boolean delete(T t);

	public abstract Boolean delete(Long id);

	public User login(String name_user, String password) {
		// TODO Auto-generated method stub
		return null;
	}

}