package com.webbook.springmvc.entities;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "publisher" , catalog = "webbook")
public class Publisher implements java.io.Serializable {
	private static final long serialVersionUID = 1L;

	private Long id;
	private String publisher_name;
	private Address address;
	
	public Publisher() {
	}
	
	public Publisher(String publisher_name) {
		this.publisher_name = publisher_name;
	}
	
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(name = "NAME_PUBLISHER" , length = 50)
	public void setPublisher_name(String publisher_name) {
		this.publisher_name = publisher_name;
	}

	public void setAddress(Address address) {
		this.address = address;
	}
	
	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@PrimaryKeyJoinColumn
	public Address getAddress() {
		return address;
	}
	public String getPublisher_name() {
		return publisher_name;
	}

	@Override
	public String toString() {
		return "Publisher [id=" + id + ", publisher_name=" + publisher_name + ", address=" + address + "]";
	}
	
}
