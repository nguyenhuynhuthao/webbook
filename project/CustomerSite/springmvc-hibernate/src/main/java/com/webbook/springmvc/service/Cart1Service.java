package com.webbook.springmvc.service;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.webbook.springmvc.dao.Cart1Dao;
import com.webbook.springmvc.dao.Dao;
import com.webbook.springmvc.entities.Cart1;

@Transactional
@Service
public class Cart1Service {
	@Autowired
	Cart1Dao Cart1DAO;
	@Autowired
	private SessionFactory sessionFactory;
	
	public List<Cart1> getAll(){
		return Cart1DAO.getAll();
	}
	
	public Cart1 get(Long id){
		return Cart1DAO.get(id);
	}
	
	public Cart1 add(Cart1 t){
		return Cart1DAO.add(t);
	}
	
	public Boolean update(Cart1 t){
		return Cart1DAO.update(t);
	}
	
	public Boolean delete(Cart1 t){
		return Cart1DAO.delete(t);
	}
	
	public Boolean delete(Long id){
		return Cart1DAO.delete(id);
	}
	
	public Cart1 isExist(Long bookId, Long userId){
		Cart1 cart = null;
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Cart1 where bookId = :bookId and userId = :userId";
		Query query = session.createQuery(hql);
		query.setParameter("bookId", bookId.longValue());
		query.setParameter("userId", userId.longValue());
		if (query.list().size() >= 1)
			cart = (Cart1) query.list().get(0);
		return cart;
	}
	public List<Cart1> getCart(Long id) {
		return Cart1DAO.getCart(id);
	}
}
