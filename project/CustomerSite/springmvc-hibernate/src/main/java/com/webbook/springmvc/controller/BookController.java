package com.webbook.springmvc.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import com.webbook.springmvc.entities.Book;
import com.webbook.springmvc.entities.Publisher;
import com.webbook.springmvc.service.BookService;
import com.webbook.springmvc.service.PublisherService;


@Controller
@RequestMapping(value = "/controller")
public class BookController {
	@Autowired
	private BookService bookService;
	@Autowired
	private PublisherService publisherService;
	@Autowired
	SessionLocaleResolver localeResolver;

	@RequestMapping(value = "/books", method = RequestMethod.GET)
	public ModelAndView listBook(@RequestParam(value = "lang", defaultValue = "en") String language) {
		switch (language) {
		case "en":
			localeResolver.setDefaultLocale(Locale.ENGLISH);
			break;
		case "vi":
			localeResolver.setDefaultLocale(new Locale("vi", "VN"));
			break;
		default:
			break;
		}
		ModelAndView model = new ModelAndView();
		model.setViewName("bookList");
		model.addObject("book", bookService.getAll());
		return model;
	}

	@RequestMapping(value = "/book", method = RequestMethod.GET)
	public ModelAndView viewBook(@RequestParam Long id) {
		ModelAndView model = new ModelAndView();
		model.setViewName("bookDetail");
		model.addObject("book", bookService.get(id));		
		return model;
	}



}

