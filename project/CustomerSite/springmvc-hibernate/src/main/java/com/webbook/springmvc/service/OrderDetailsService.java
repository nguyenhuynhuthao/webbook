package com.webbook.springmvc.service;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.webbook.springmvc.dao.Dao;
import com.webbook.springmvc.entities.Order;
import com.webbook.springmvc.entities.OrderDetails;

@Transactional
@Service
public class OrderDetailsService {
	@Autowired
	Dao<OrderDetails> orderDetailsDAO;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public List<OrderDetails> getAll(){
		return orderDetailsDAO.getAll();
	}
	
	public OrderDetails get(Long id){
		return orderDetailsDAO.get(id);
	}
	
	public OrderDetails add(OrderDetails t){
		return orderDetailsDAO.add(t);
	}
	
	public Boolean update(OrderDetails t){
		return orderDetailsDAO.update(t);
	}
	
	public Boolean delete(OrderDetails t){
		return orderDetailsDAO.delete(t);
	}
	
	public Boolean delete(Long id){
		return orderDetailsDAO.delete(id);
	}
}
