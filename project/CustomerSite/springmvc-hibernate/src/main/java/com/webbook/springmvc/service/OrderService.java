package com.webbook.springmvc.service;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.webbook.springmvc.dao.Dao;
import com.webbook.springmvc.entities.Book;
import com.webbook.springmvc.entities.Order;

@Transactional
@Service
public class OrderService {
	
	@Autowired
	Dao<Order> orderDAO;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public List<Order> getAll(){
		return orderDAO.getAll();
	}
	
	public Order get(Long id){
		return orderDAO.get(id);
	}
	
	public Order add(Order t){
		return orderDAO.add(t);
	}
	
	public Boolean update(Order t){
		return orderDAO.update(t);
	}
	
	public Boolean delete(Order t){
		return orderDAO.delete(t);
	}
	
	public Boolean delete(Long id){
		return orderDAO.delete(id);
	}
	
	public List<Book> getBookBybookId(Long bookId){
		List<Book> book = null;
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Book where id = :bookId";
		Query query = session.createQuery(hql);
		query.setParameter("bookId", bookId);
		if (query.list().size() >= 1)
			book = (List<Book>) query.list();
		return book;
	}
}