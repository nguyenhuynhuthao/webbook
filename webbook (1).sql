-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th5 17, 2019 lúc 01:41 PM
-- Phiên bản máy phục vụ: 10.1.28-MariaDB
-- Phiên bản PHP: 5.6.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `webbook`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `address`
--

CREATE TABLE `address` (
  `ID` bigint(20) NOT NULL,
  `CITY` varchar(20) DEFAULT NULL,
  `DISTRICT` varchar(20) DEFAULT NULL,
  `STREET` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `address`
--

INSERT INTO `address` (`ID`, `CITY`, `DISTRICT`, `STREET`) VALUES
(1, NULL, NULL, NULL),
(3, 'qwr', 'arf', 'sa'),
(4, 'qwe', 'tphcm', 'ch');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `author`
--

CREATE TABLE `author` (
  `ID` bigint(20) NOT NULL,
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `author`
--

INSERT INTO `author` (`ID`, `name`) VALUES
(1, 'Fujiko F Fujio'),
(2, 'Tsugumi Ohba'),
(3, 'Takeshi Obata'),
(4, 'P.R.O'),
(5, 'Yoshito Usui'),
(6, 'Ruyuha Kyouka'),
(7, 'Gosho Aoyama'),
(8, 'Rikako Akiyoshi'),
(9, 'Yuna Anisaki'),
(10, 'Nhi?u Tác Gi?'),
(11, 'Thanh Nga'),
(12, 'Huy Anh'),
(13, 'Th??ng Thái Vi'),
(14, 'Lâu V? Tình');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `book`
--

CREATE TABLE `book` (
  `ID` bigint(20) NOT NULL,
  `imgage` varchar(255) DEFAULT NULL,
  `Name` varchar(200) DEFAULT NULL,
  `Price` bigint(20) DEFAULT NULL,
  `soluong` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `AUTHOR_ID` bigint(20) DEFAULT NULL,
  `GENRE_ID` bigint(20) DEFAULT NULL,
  `PUBLISHER_ID` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `book`
--

INSERT INTO `book` (`ID`, `imgage`, `Name`, `Price`, `soluong`, `status`, `AUTHOR_ID`, `GENRE_ID`, `PUBLISHER_ID`) VALUES
(0, 'h15.jpg', 'Chàng R&#7875; Hay Ch&#7919;', 84000, 100, 1, 13, 4, 7),
(7, 'h7.jpg', 'H&#7884;C VI&#7878;N SIÊU ANH HÙNG - T&#7852;P 16', 18000, 100, 0, 6, 2, 1),
(8, 'conan.jpg', 'THÁM T&#7916; L&#7914;NG DANH CONAN - T&#7852;P 94', 19400, 100, 0, 7, 2, 1),
(9, 'h9.jpg', 'BOXSET NHÁNH LINH LAN H&#7854;C ÁM', 121500, 100, 2, 11, 2, 3),
(10, 'h10.jpg', '&#272;AO KI&#7870;M LO&#7840;N V&#360;', 31500, 100, 0, 2, 2, 5),
(12, 'h16.jpg', 'Sách Thi&#7871;u Nhi - V&#7847;ng Tr&#259;ng Quên Lãng', 79000, 100, 1, 2, 2, 6),
(13, 'miko.jpg', 'Sách Thi&#7871;u Nhi - Khi M&#7865; V&#7855;ng Nhà', 62400, 100, 0, 2, 2, 1),
(16, 'h16.jpg', 'Mi&#7871;ng Tr&#7847;u Kì Di&#7879;u', 8400, 100, 0, 13, 4, 7),
(17, 'h17.jpg', 'Con Chó Bi&#7871;t Nói', 9000, 100, 0, 8, 4, 8),
(18, 'h18.jpg', 'H&#7891;n Tr&#432;&#417;ng Ba Da Hàng Th&#7883;t', 7700, 100, 0, 2, 4, 6),
(19, 'h19', 'B?n Xe', 75000, 100, 0, 9, 3, 9),
(20, 'h20', 'Th?t T?ch Không M?a', 55300, 100, 0, 10, 3, 10),
(21, 'conan.jpg', 'text', 50000, 0, 0, 1, 2, 1),
(23, 'conan.jpg', 'conan', 20000, NULL, 0, 1, 1, 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `cart1`
--

CREATE TABLE `cart1` (
  `id` bigint(20) NOT NULL,
  `num` int(11) DEFAULT NULL,
  `ID_USER` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `cart_book`
--

CREATE TABLE `cart_book` (
  `ID` bigint(20) NOT NULL,
  `ID_BOOK` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `customer`
--

CREATE TABLE `customer` (
  `ID_CUSTOMER` bigint(20) NOT NULL,
  `MESSAGE` varchar(100) DEFAULT NULL,
  `NAME_CUSTOMER` varchar(50) DEFAULT NULL,
  `PHONE_CUSTOMER` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `genre`
--

CREATE TABLE `genre` (
  `ID` bigint(20) NOT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `genre`
--

INSERT INTO `genre` (`ID`, `DESCRIPTION`) VALUES
(1, 'Vi&#7877;n T&#432;&#7903;ng'),
(2, 'Trinh Thám'),
(3, 'C&#7893; Tích'),
(4, 'Ngôn Tình'),
(5, 'Thi&#7871;u Nhi');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `order`
--

CREATE TABLE `order` (
  `ID` bigint(20) NOT NULL,
  `DATE_PURCHSE` datetime DEFAULT NULL,
  `status` int(11) NOT NULL,
  `ID_USER` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `order`
--

INSERT INTO `order` (`ID`, `DATE_PURCHSE`, `status`, `ID_USER`) VALUES
(6, '2019-05-15 12:42:18', 1, 3),
(7, '2019-05-16 17:44:33', 1, 3),
(8, '2019-05-17 00:43:20', 1, 4),
(9, '2019-05-17 00:48:19', 3, 4),
(10, '2019-05-17 00:50:01', 3, 4),
(11, '2019-05-17 01:09:43', 1, 4),
(12, '2019-05-17 01:12:45', 1, 4);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `order_details`
--

CREATE TABLE `order_details` (
  `ID` bigint(20) NOT NULL,
  `quantity` int(11) NOT NULL,
  `total_price` int(11) NOT NULL,
  `order_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `order_details`
--

INSERT INTO `order_details` (`ID`, `quantity`, `total_price`, `order_id`) VALUES
(7, 1, 121500, 6),
(8, 1, 121500, 7),
(9, 1, 79000, 7),
(10, 1, 121500, 8),
(11, 1, 18000, 9),
(12, 1, 79000, 10),
(13, 1, 18000, 11),
(14, 1, 121500, 12),
(15, 1, 31500, 12);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `publisher`
--

CREATE TABLE `publisher` (
  `ID` bigint(20) NOT NULL,
  `publisher_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `publisher`
--

INSERT INTO `publisher` (`ID`, `publisher_name`) VALUES
(1, 'NXB Kim &#272;&#7891;ng'),
(2, 'NXB V&#259;n Hóa Sài Gòn'),
(3, 'NXB Hà N&#7897;i'),
(4, 'NXB:Báo Sinh viên VN-Hoa H&#7885;c Trò'),
(5, 'NXB Tri Th&#7913;c'),
(6, 'NXB Giáo D&#7909;c Vi&#7879;t Nam'),
(7, 'Nhà Xu&#7845;t b&#7843;n v&#259;n h&#7885;c'),
(8, 'Nhà Xu&#7845;t B&#7843;n Công an nhân dân'),
(9, 'Nhà Xu&#7845;t b&#7843;n h&#7891;ng bàng'),
(10, 'NXB Lao &#272;&#7897;ng');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `revenue`
--

CREATE TABLE `revenue` (
  `ID` bigint(20) NOT NULL,
  `FROM_DATE` date DEFAULT NULL,
  `TO_DATE` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `user`
--

CREATE TABLE `user` (
  `ID_USER` bigint(20) NOT NULL,
  `NAME` varchar(50) DEFAULT NULL,
  `PASSWORD` varchar(50) DEFAULT NULL,
  `ROLE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `user`
--

INSERT INTO `user` (`ID_USER`, `NAME`, `PASSWORD`, `ROLE`) VALUES
(1, 'admin', '1', 0),
(3, 'qwe', 'n0m81hav3sQ=', 2),
(4, 'tuong', 'n0m81hav3sQ=', 2),
(5, 'ttt', 'n0m81hav3sQ=', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `userdetail`
--

CREATE TABLE `userdetail` (
  `ID` bigint(20) NOT NULL,
  `FULLNAME` varchar(50) DEFAULT NULL,
  `PHONES` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `userdetail`
--

INSERT INTO `userdetail` (`ID`, `FULLNAME`, `PHONES`) VALUES
(1, NULL, NULL),
(3, 'as', '0981187082'),
(4, 'tuong', '0981187082'),
(5, NULL, NULL);

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `address`
--
ALTER TABLE `address`
  ADD PRIMARY KEY (`ID`);

--
-- Chỉ mục cho bảng `author`
--
ALTER TABLE `author`
  ADD PRIMARY KEY (`ID`);

--
-- Chỉ mục cho bảng `book`
--
ALTER TABLE `book`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK_gbq0gpu0aaxx03lu50e770qo8` (`AUTHOR_ID`),
  ADD KEY `FK_pp8nhflerescdnce3xxprqqh5` (`GENRE_ID`),
  ADD KEY `FK_qovid3r59gbdh0l7196iviqwt` (`PUBLISHER_ID`);

--
-- Chỉ mục cho bảng `cart1`
--
ALTER TABLE `cart1`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_p32xcbh5uof2wd4i63te8hqvf` (`ID_USER`);

--
-- Chỉ mục cho bảng `cart_book`
--
ALTER TABLE `cart_book`
  ADD KEY `FK_hqr59gqnonupjtddlk5ltww0q` (`ID_BOOK`),
  ADD KEY `FK_8r1apf1bmukb6pw0w2xtait3p` (`ID`);

--
-- Chỉ mục cho bảng `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`ID_CUSTOMER`);

--
-- Chỉ mục cho bảng `genre`
--
ALTER TABLE `genre`
  ADD PRIMARY KEY (`ID`);

--
-- Chỉ mục cho bảng `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK_rx7y7xi6gcwpeb5pjf2goi1n9` (`ID_USER`);

--
-- Chỉ mục cho bảng `order_details`
--
ALTER TABLE `order_details`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK_p387c2pa1m3xxcxcktoawo954` (`order_id`);

--
-- Chỉ mục cho bảng `publisher`
--
ALTER TABLE `publisher`
  ADD PRIMARY KEY (`ID`);

--
-- Chỉ mục cho bảng `revenue`
--
ALTER TABLE `revenue`
  ADD PRIMARY KEY (`ID`);

--
-- Chỉ mục cho bảng `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`ID_USER`);

--
-- Chỉ mục cho bảng `userdetail`
--
ALTER TABLE `userdetail`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `address`
--
ALTER TABLE `address`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT cho bảng `author`
--
ALTER TABLE `author`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT cho bảng `book`
--
ALTER TABLE `book`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT cho bảng `cart1`
--
ALTER TABLE `cart1`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `customer`
--
ALTER TABLE `customer`
  MODIFY `ID_CUSTOMER` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `genre`
--
ALTER TABLE `genre`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT cho bảng `order`
--
ALTER TABLE `order`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT cho bảng `order_details`
--
ALTER TABLE `order_details`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT cho bảng `publisher`
--
ALTER TABLE `publisher`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT cho bảng `revenue`
--
ALTER TABLE `revenue`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `user`
--
ALTER TABLE `user`
  MODIFY `ID_USER` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT cho bảng `userdetail`
--
ALTER TABLE `userdetail`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `book`
--
ALTER TABLE `book`
  ADD CONSTRAINT `FK_gbq0gpu0aaxx03lu50e770qo8` FOREIGN KEY (`AUTHOR_ID`) REFERENCES `author` (`ID`),
  ADD CONSTRAINT `FK_pp8nhflerescdnce3xxprqqh5` FOREIGN KEY (`GENRE_ID`) REFERENCES `genre` (`ID`),
  ADD CONSTRAINT `FK_qovid3r59gbdh0l7196iviqwt` FOREIGN KEY (`PUBLISHER_ID`) REFERENCES `publisher` (`ID`);

--
-- Các ràng buộc cho bảng `cart1`
--
ALTER TABLE `cart1`
  ADD CONSTRAINT `FK_p32xcbh5uof2wd4i63te8hqvf` FOREIGN KEY (`ID_USER`) REFERENCES `user` (`ID_USER`);

--
-- Các ràng buộc cho bảng `order`
--
ALTER TABLE `order`
  ADD CONSTRAINT `FK_rx7y7xi6gcwpeb5pjf2goi1n9` FOREIGN KEY (`ID_USER`) REFERENCES `user` (`ID_USER`);

--
-- Các ràng buộc cho bảng `order_details`
--
ALTER TABLE `order_details`
  ADD CONSTRAINT `FK_p387c2pa1m3xxcxcktoawo954` FOREIGN KEY (`order_id`) REFERENCES `order` (`ID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
